package com.luocanhui.myexpandablelistview;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;

public class MainActivity extends Activity {

	private ExpandableListView mListView;
	private MyAdapter adapter;
	private List<GroupBean> groupBeansList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		initData();
		mListView = (ExpandableListView) findViewById(R.id.my_listview);
		adapter = new MyAdapter(groupBeansList, MainActivity.this);
		
		mListView.setAdapter(adapter);
		mListView.setGroupIndicator(null); // 表示不使用系统提供的展开和收起的图标
		mListView.expandGroup(0); // 表示默认开打第一项
	}

	private void initData() {
		groupBeansList = new ArrayList<GroupBean>();
		{
			List<ChildBean> childBeansList = new ArrayList<ChildBean>();
			ChildBean cb1 = new ChildBean("妈妈", "123");
            ChildBean cb2 = new ChildBean("爸爸", "456");
            ChildBean cb3 = new ChildBean("爷爷", "789");
            ChildBean cb4 = new ChildBean("妹妹", "000");
            childBeansList.add(cb1);
            childBeansList.add(cb2);
            childBeansList.add(cb3);
            childBeansList.add(cb4);
            GroupBean gb1 = new GroupBean("家", childBeansList);
            groupBeansList.add(gb1);
		}
		{
            List<ChildBean> childBeansList = new ArrayList<ChildBean>();
            ChildBean cb1 = new ChildBean("张三", "123");
            ChildBean cb2 = new ChildBean("李四", "456");
            ChildBean cb3 = new ChildBean("王五", "789");
            ChildBean cb4 = new ChildBean("赵六", "000");
            ChildBean cb5 = new ChildBean("风起", "1111");
            ChildBean cb6 = new ChildBean("马坝", "222");
            ChildBean cb7 = new ChildBean("迁就", "3333333");
            childBeansList.add(cb1);
            childBeansList.add(cb2);
            childBeansList.add(cb3);
            childBeansList.add(cb4);
            childBeansList.add(cb5);
            childBeansList.add(cb6);
            childBeansList.add(cb7);
            GroupBean gb1 = new GroupBean("我的朋友", childBeansList);
            groupBeansList.add(gb1);
        }
        {
            List<ChildBean> childBeansList = new ArrayList<ChildBean>();
            ChildBean cb1 = new ChildBean("Tom", "123");
            ChildBean cb2 = new ChildBean("Jerry", "456");
            ChildBean cb4 = new ChildBean("Bush", "000");
            childBeansList.add(cb1);
            childBeansList.add(cb2);
            childBeansList.add(cb4);
            GroupBean gb1 = new GroupBean("国际友人", childBeansList);
            groupBeansList.add(gb1);
        }
        {
            List<ChildBean> childBeansList = new ArrayList<ChildBean>();
            ChildBean cb1 = new ChildBean("赵工", "123");
            ChildBean cb2 = new ChildBean("马工", "456");
            ChildBean cb3 = new ChildBean("王工", "789");
            ChildBean cb4 = new ChildBean("李工", "000");
            ChildBean cb5 = new ChildBean("为工", "000");
            childBeansList.add(cb1);
            childBeansList.add(cb2);
            childBeansList.add(cb3);
            childBeansList.add(cb4);
            childBeansList.add(cb5);
            GroupBean gb1 = new GroupBean("同事", childBeansList);
            groupBeansList.add(gb1);
        }
	}
}
