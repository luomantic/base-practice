package com.luocanhui.myexpandablelistview;

public class ChildBean {
	
	private String name;
	private String sign;
	
	public ChildBean() {
		// TODO Auto-generated constructor stub
	}
	
	public ChildBean(String name, String sign) {
		super();
		this.name = name;
		this.sign = sign;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return "ChildBean [name=" + name + ", sign=" + sign + "]";
	}
	
}
