package com.luocanhui.myexpandablelistview;

import java.util.List;

public class GroupBean {
	
	private String groupName;
	private List<ChildBean> childBeans;
	
	public GroupBean() {
		// TODO Auto-generated constructor stub
	}

	public GroupBean(String groupName, List<ChildBean> childBeans) {
		super();
		this.groupName = groupName;
		this.childBeans = childBeans;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<ChildBean> getChildBeans() {
		return childBeans;
	}

	public void setChildBeans(List<ChildBean> childBeans) {
		this.childBeans = childBeans;
	}

	@Override
	public String toString() {
		return "GroupBean [groupName=" + groupName + ", childBeans=" + childBeans + "]";
	}
	
}
