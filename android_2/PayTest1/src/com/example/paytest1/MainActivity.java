package com.example.paytest1;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import com.unionpay.UPPayAssistEx;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	private  Handler hanlder = new Handler() {
		public void handleMessage(android.os.Message msg) {
			String tn = "";
			if (msg.obj == null || ((String) msg.obj).length() == 0 ) {
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setTitle("错误提示");
				builder.setMessage("网络连接失败，请重试");
				builder.setNegativeButton("确定", new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				builder.create().show();
			} else {
				tn = (String) msg.obj;
				/*************************************************
				 * 步骤2：通过银联工具类启动支付插件
				 ************************************************/
				doStartUnionPayPlugin(MainActivity.this, tn, BaseData.mMode);
			}
		};
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void go(View view) {
		new NetThread().start();
	}
	
	/**
	 * @author Administrator
	 * 获取流水账号的子线程
	 */
	class NetThread extends Thread {
		@Override
		public void run() {
			String tn = null;
			InputStream is;
			try {

				String url = BaseData.TN_URL_01;

				URL myURL = new URL(url);
				URLConnection ucon = myURL.openConnection();
				ucon.setConnectTimeout(120000);
				is = ucon.getInputStream();
				int i = -1;
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				while ((i = is.read()) != -1) {
					baos.write(i);
				}

				tn = baos.toString();
				is.close();
				baos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			Message msg = hanlder.obtainMessage();
			msg.obj = tn;
			hanlder.sendMessage(msg);
		}
	}
	
	/* (non-Javadoc)
	 *  在onActivityResult中通过intent去接收支付的结果
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		String msg = null;
		String str = data.getExtras().getString("pay_result");
		if (str.equalsIgnoreCase("success")) {
			msg = "支付成功";
		}else if (str.equalsIgnoreCase("fail")) {
			msg = "支付失败";
		}else if (str.equalsIgnoreCase("cancel")) {
			msg = "用户取消了支付";
		}
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}
	
	protected void  doStartUnionPayPlugin(MainActivity mainActivity, 
			String tn, String mode) {
		int ret = UPPayAssistEx.startPay(this, null, null, tn, BaseData.mMode);
		if (ret == BaseData.PLUGIN_NEED_UPGRADE 
				|| ret == BaseData.PLUGIN_NOT_INSTALLED) {
			// 需要重新安装控件
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("提示");
			builder.setMessage("完成购买需要安装银联支付控件，是否安装？");
			
			builder.setNegativeButton("确定", new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					UPPayAssistEx.installUPPayPlugin(MainActivity.this);
					dialog.dismiss();
				}
			});
			
			builder.setPositiveButton("取消", new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			builder.show();
		}
	}
	
}
