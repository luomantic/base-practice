package com.example.paytest1;

public class BaseData {

	public static final int PLUGIN_VALID = 0;
    public static final int PLUGIN_NOT_INSTALLED = -1;
    public static final int PLUGIN_NEED_UPGRADE = 2;

    /*****************************************************************
     * mMode参数解释： "00" - 启动银联正式环境 "01" - 连接银联测试环境
     *****************************************************************/
    public static final String mMode = "01";
    //银联内部测试服务器
    public static final String TN_URL_01 = "http://101.231.204.84:8091/sim/getacptn";
	
}
