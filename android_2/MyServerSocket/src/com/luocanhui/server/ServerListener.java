package com.luocanhui.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JOptionPane;

import com.luocanhui.server.constant.MyPort;

public class ServerListener extends Thread {

	@Override
	public void run() {
		try {
			ServerSocket serverSocket = new ServerSocket(MyPort.port);
			while (true) {
				// block
				Socket socket = serverSocket.accept();
				// 建立连接
				JOptionPane.showMessageDialog(null, "有客户端连接了本机的6666端口");
				// 将socket传递给新的线程
				ChatSocket cs = new ChatSocket(socket);
				cs.start();
				ChatManager.getChatManager().add(cs);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
