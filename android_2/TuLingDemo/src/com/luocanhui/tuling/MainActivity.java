package com.luocanhui.tuling;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.luocanhui.tuling.constant.GlobalConstant;
import com.luocanhui.tuling.entity.MyDataEntity;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

	private RequestQueue mQueue;
	private MyDataEntity myDataEntity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mQueue = Volley.newRequestQueue(this);
		startChat();
	}

	public void startChat() {
		StringRequest stringRequest = new StringRequest(GlobalConstant.COMMEN_URL, new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				String tulingMsg = parseTuLingData(response);
			}

		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				System.out.println(error);
			}

		});

		mQueue.add(stringRequest);
	}

	protected String parseTuLingData(String response) {
		Gson gson = new Gson();
		myDataEntity = gson.fromJson(response, MyDataEntity.class);
		return myDataEntity.text;
	}

	// private String getTime() {
	// SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
	// return format.format(new java.util.Date());
	// }
}
