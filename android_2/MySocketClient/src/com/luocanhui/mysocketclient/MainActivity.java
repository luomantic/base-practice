package com.luocanhui.mysocketclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import com.luocanhui.mysocketclient.constant.MyPort;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private EditText et_ip, et_content;
	private Button btn_connect, btn_send;
	private TextView tv_text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		et_ip = (EditText) findViewById(R.id.et_ip);
		et_content = (EditText) findViewById(R.id.et_content);
		btn_connect = (Button) findViewById(R.id.btn_connect);
		btn_send = (Button) findViewById(R.id.btn_send);
		tv_text = (TextView) findViewById(R.id.tv_text);
		
		btn_connect.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				connect();
			}
		});
		btn_send.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				send();
			}
		});
	}
	
	// -------------------------------------------
	
	Socket socket = null;
	BufferedWriter writer = null;
	BufferedReader reader = null;
	protected void connect() {
		try {
			AsyncTask<Void, String, Void> read = new AsyncTask<Void, String, Void>() {

				@Override
				protected Void doInBackground(Void... params) {
					String dstName = et_ip.getText().toString().trim();
					String line;
					try {
						socket = new Socket(dstName, MyPort.port);
						writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
						reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
						publishProgress("@success");
						while ((line = reader.readLine()) != null) {
							publishProgress(line);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;
				}

				@Override
				protected void onProgressUpdate(String... values) {
					if (values[0].equals("@success")) {
						Toast.makeText(MainActivity.this, "连接成功", Toast.LENGTH_SHORT).show();
					}
					tv_text.append(values[0]);
					super.onProgressUpdate(values);
				}
			};
			read.execute();
		} catch (Exception e) {
			Toast.makeText(MainActivity.this, "无法建立连接", Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
	}

	protected void send() {
		try {
			String content = et_content.getText().toString().trim();
			writer.write(content + "\n");
			writer.flush();
			et_content.setText("");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
