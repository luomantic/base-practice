package com.example.chatrobot;

import java.util.ArrayList;
import java.util.Random;

import com.example.chatrobot.VoiceBean.WSBean;
import com.google.gson.Gson;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechUtility;
import com.iflytek.cloud.ui.RecognizerDialog;
import com.iflytek.cloud.ui.RecognizerDialogListener;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {

	private ListView lv_listview;
	private ListViewAdapter adapter;
	private ArrayList<ChatBean> mChatList = new ArrayList<ChatBean>();
	private String[] mMMAnswers = new String[] { "约吗", "讨厌！", "不要再要了！", "这是最后一张了！", "漂亮吧？" };
	private int[] mMMImageIDs = new int[] { R.drawable.p1, R.drawable.p2, R.drawable.p3, R.drawable.p4 };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		lv_listview = (ListView) findViewById(R.id.lv_listview);
		adapter = new ListViewAdapter();
		lv_listview.setAdapter(adapter);

		// 初始化语音引擎
		SpeechUtility.createUtility(this, SpeechConstant.APPID + "=5638926c");
	}

	private class ListViewAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return mChatList.size();
		}

		@Override
		public ChatBean getItem(int position) {
			return mChatList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = View.inflate(MainActivity.this, R.layout.list_item, null);

				holder.tv_asker = (TextView) convertView.findViewById(R.id.tv_asker);
				holder.tv_answer = (TextView) convertView.findViewById(R.id.tv_answer);
				holder.iv_pic = (ImageView) convertView.findViewById(R.id.iv_pic);
				holder.ll_answer = (LinearLayout) convertView.findViewById(R.id.ll_answer);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			ChatBean item = getItem(position);
			if (item.isAsker) { // 是提问者
				holder.tv_asker.setVisibility(View.VISIBLE);
				holder.ll_answer.setVisibility(View.GONE);

				holder.tv_asker.setText(item.text);
			} else {
				holder.tv_asker.setVisibility(View.GONE);
				holder.ll_answer.setVisibility(View.VISIBLE);
				holder.tv_answer.setText(item.text);
				if (item.imageId != -1) { // 有图片
					holder.iv_pic.setImageResource(item.imageId);
					holder.iv_pic.setVisibility(View.VISIBLE);
				} else {
					holder.iv_pic.setVisibility(View.GONE);
				}
			}

			return convertView;
		}

	}

	static class ViewHolder {
		private TextView tv_asker;
		private TextView tv_answer;
		private ImageView iv_pic;
		private LinearLayout ll_answer;
	}

	/**
	 * @param view
	 *            开始语音识别
	 */
	public void voiceButtonClick(View view) {
		// 1.创建RecognizerDialog对象
		RecognizerDialog mDialog = new RecognizerDialog(this, null);
		// 2.设置accent、language等参数
		mDialog.setParameter(SpeechConstant.LANGUAGE, "zh_cn");
		mDialog.setParameter(SpeechConstant.ACCENT, "mandarin");
		// 若要将UI控件用于语义理解， 必须添加以下参数设置， 设置之后onResult回调返回将是语义理解
		// 结果
		// mDialog.setParameter("asr_sch", "1");
		// mDialog.setParameter("nlp_version", "2.0");
		// 3.设置回调接口
		mDialog.setListener(mRecognizerDialogListener);
		// 4.显示dialog，接收语音输入
		mDialog.show();
	}

	StringBuffer mTextBuffer;
	private RecognizerDialogListener mRecognizerDialogListener = new RecognizerDialogListener() {

		@Override
		public void onResult(RecognizerResult results, boolean isLast) {
			mTextBuffer = new StringBuffer();
			String data = parseData(results.getResultString());
			mTextBuffer.append(data);

			if (isLast) { // 会话结束
				String finalText = mTextBuffer.toString();
				mTextBuffer = new StringBuffer(); // 清理buffer
				// System.out.println("最终结果：" + finalText);
				mChatList.add(new ChatBean(finalText, true, -1));

				String answer = "没听清";
				int imageId = -1;
				if (finalText.contains("你好")) {
					answer = "大家好，才是真的好";
				} else if (finalText.contains("你是谁")) {
					answer = "我是你的小助手";
				} else if (finalText.contains("天王盖地虎")) {
					answer = "小鸡炖蘑菇";
					imageId = R.drawable.m;
				} else if (finalText.contains("美女")) {
					Random random = new Random();
					int i = random.nextInt(mMMAnswers.length);
					int j = random.nextInt(mMMImageIDs.length);
					answer = mMMAnswers[i];
					imageId = mMMImageIDs[j];
				}

				mChatList.add(new ChatBean(answer, false, imageId)); // 添加回答数据
				adapter.notifyDataSetChanged(); // 刷新ListView
			}
		}

		@Override
		public void onError(SpeechError arg0) {

		}
	};

	/**
	 * @param resultString
	 *            解析语音数据
	 */
	protected String parseData(String resultString) {
		Gson gson = new Gson();
		VoiceBean bean = gson.fromJson(resultString, VoiceBean.class);
		ArrayList<WSBean> ws = bean.ws;

		StringBuffer sb = new StringBuffer();
		for (WSBean wsBean : ws) {
			String text = wsBean.cw.get(0).w;
			sb.append(text);
		}

		return sb.toString();
	}

	/*
	 * private InitListener mInitListener = new InitListener() {
	 * 
	 * @Override public void onInit(int arg0) {
	 * 
	 * }
	 * 
	 * };
	 */

}
