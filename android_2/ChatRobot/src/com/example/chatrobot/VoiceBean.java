package com.example.chatrobot;

import java.util.ArrayList;

/**
 * @author Administrator
 *	 语音信息的封装
 */
public class VoiceBean {

	public ArrayList<WSBean> ws;
	
	public class WSBean {
		public ArrayList<CWBean> cw;
	}
	
	public class CWBean {
		public String w;
	}
	
}
