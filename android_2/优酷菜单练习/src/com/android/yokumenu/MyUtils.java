package com.android.yokumenu;

import android.view.View;
import android.view.animation.RotateAnimation;

public class MyUtils {

	public static void startAniOut(View view) {
		startAniIn(view, 9);
	}
	
	/**
	 * 让指定view 延迟 执行旋转离开的动画
	 * @param view
	 * @param i 延时的时间
	 */
	public static void startAniOut(View view, int offset) {
		/*
		 * 默认圆心为view的左上角
		 * 水平向右为0
		 * 顺时针旋转度数增加
		 */
		RotateAnimation animation = new RotateAnimation(0, 180, view.getWidth()/2, view.getHeight());
		animation.setDuration(500); //	 设置运行的事件
		animation.setFillAfter(true); // 动画执行完以后保持最后的状态
		animation.setStartOffset(offset); // 设置延时执行的时间
		view.startAnimation(animation);
	}
	
	public static void startAniIn(View view) {
		startAniIn(view, 0);
	}
	
	/**
	 * 让指定的view 延时进入旋转的动画
	 * @param level2
	 * @param i 时间
	 */
	public static void startAniIn(View view, int offset) {
		RotateAnimation animation = new RotateAnimation(180, 360, view.getWidth()/2, view.getHeight());
		animation.setDuration(500); //	 设置运行的事件
		animation.setFillAfter(true); // 动画执行完以后保持最后的状态
		animation.setStartOffset(offset);
		view.startAnimation(animation);
	}

}
