package com.android.yokumenu;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class MainActivity extends Activity implements OnClickListener {

	private ImageView icon_menu;
	private ImageView icon_home;
	private RelativeLayout level1;
	private RelativeLayout level2;
	private RelativeLayout level3;
	/*
	 * 判断 第1、2、3级菜单是否显示
	 * true 为显示
	 */
	private boolean isLevel3Show = true;
	private boolean isLevel2Show = true;
	private boolean isLevel1Show = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		init();
		icon_home.setOnClickListener(this);
		icon_menu.setOnClickListener(this);
	}

	private void init() {
		icon_home = (ImageView) findViewById(R.id.icon_home);
		icon_menu = (ImageView) findViewById(R.id.icon_menu);
		level1 = (RelativeLayout) findViewById(R.id.level1);
		level2 = (RelativeLayout) findViewById(R.id.level2);
		level3 = (RelativeLayout) findViewById(R.id.level3);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.icon_menu: 			//处理menu 图标的点击事件
			// 如果第3级菜单是显示状态，那就把它隐藏
			if (isLevel3Show) {
				//隐藏 第3级菜单
				MyUtils.startAniOut(level3);
			}else {
				// 如果第3级菜单是隐藏状态，那就把它显示
				MyUtils.startAniIn(level3);
			}
			isLevel3Show = !isLevel3Show;
			
			break;
		case R.id.icon_home: 			//处理home 图标的点击事件
			// 如果第2级菜单是显示状态，那就把它隐藏
			// 如果第2级菜单是隐藏状态，那就把它显示
			if (isLevel2Show ) {
				MyUtils.startAniOut(level3);
				
				if (isLevel3Show) { // 如果此时，第3级菜单也显示，那也将其隐藏
					MyUtils.startAniOut(level2, 200);
				}
				
			}else {
				MyUtils.startAniIn(level2);
			}
			isLevel2Show = !isLevel2Show;
			
			break;
		default:
			break;
		}
	}
	
	/**
	 * 改变第1级菜单的状态
	 */
	private void changeLevel1State() {
		//如果 第1级菜单是显示状态，那么久隐藏1,2,3级菜单
		if (isLevel1Show ) {
			MyUtils.startAniOut(level1);
			isLevel1Show = false;
			
			if (isLevel2Show) {
				MyUtils.startAniOut(level2, 100);
				isLevel2Show = false;
				
				if (isLevel3Show) {
					MyUtils.startAniOut(level3, 200);
					isLevel3Show = false;
				}
			}
		}else {
			//如果 第1级菜单是隐藏状态，那么久显示1,2级菜单
			MyUtils.startAniIn(level1);
			isLevel1Show = true;
			
			MyUtils.startAniIn(level2, 200);
			isLevel2Show = true;
			
		}
		
	}
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 *     响应按键的动作
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) { // 监听 menu 按键
			changeLevel1State();
		}
		
		return super.onKeyDown(keyCode, event);
	}

}
