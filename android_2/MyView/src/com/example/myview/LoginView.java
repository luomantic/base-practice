package com.example.myview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class LoginView extends LinearLayout implements View.OnClickListener {

//	private Context mContext;
	private OnLoginViewClickListener _oOnLoginViewClickListener;

	public LoginView(Context context, AttributeSet attrs) {
		super(context, attrs);
//		mContext = context;

		TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.LoginView);
		CharSequence userNameHint = typedArray.getText(R.styleable.LoginView_UserNameHint);
		CharSequence passwordHint = typedArray.getText(R.styleable.LoginView_PasswordHint);
		
		View view = LayoutInflater.from(context).inflate(R.layout.login_view, this, true);
		EditText username = (EditText) view.findViewById(R.id.userName);
		EditText password = (EditText) view.findViewById(R.id.password);
		Button loginButton = (Button) view.findViewById(R.id.loginButton);
		Button signupButton = (Button) view.findViewById(R.id.signupButton);

		username.setHint(userNameHint);
		password.setHint(passwordHint);
		typedArray.recycle(); // 回收
		loginButton.setOnClickListener(this);
		signupButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
//		if (v.getId() == R.id.loginButton) {
//			Toast.makeText(mContext, "Login", Toast.LENGTH_LONG).show();
//		} else if (v.getId() == R.id.signupButton) {
//			Toast.makeText(mContext, "Register", Toast.LENGTH_LONG).show();
//		}
		_oOnLoginViewClickListener.loginViewButtonClicked(v);
	}

	public void setOnLoginViewClickListener(OnLoginViewClickListener loginViewClickListener) {
		// ...
		_oOnLoginViewClickListener = loginViewClickListener;
	}

	public interface OnLoginViewClickListener {
		void loginViewButtonClicked(View v);
	}

}
