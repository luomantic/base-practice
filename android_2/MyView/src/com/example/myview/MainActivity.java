package com.example.myview;

import com.example.myview.LoginView.OnLoginViewClickListener;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	private LoginView loginView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		loginView = (LoginView) findViewById(R.id.loginView);
		loginView.setOnLoginViewClickListener(new OnLoginViewClickListener() {
			
			@Override
			public void loginViewButtonClicked(View v) {
				if (v.getId() == R.id.loginButton) {
					Toast.makeText(MainActivity.this, "Login", Toast.LENGTH_LONG).show();
				} else if (v.getId() == R.id.signupButton) {
					Toast.makeText(MainActivity.this, "Register", Toast.LENGTH_LONG).show();
				}
			}
		});
	}
}
