package com.luocanhui.charrobot.entity;

/**
 * @author Administrator
 *		聊天信息对象
 */
public class ChatBean {

	public String text;// 内容
	public boolean isAsker;// true表示提问者,否则是回答者

	public ChatBean(String text, boolean isAsker) {
		this.text = text;
		this.isAsker = isAsker;
	}

}
