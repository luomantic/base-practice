package com.luocanhui.charrobot;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SpeechUtility;
import com.iflytek.cloud.ui.RecognizerDialog;
import com.iflytek.cloud.ui.RecognizerDialogListener;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.luocanhui.charrobot.constant.GlobleConstant;
import com.luocanhui.charrobot.entity.ChatBean;
import com.luocanhui.charrobot.entity.TuLingBean;
import com.luocanhui.charrobot.entity.VoiceBean;
import com.luocanhui.charrobot.entity.VoiceBean.WSBean;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private ShareActionProvider mShareActionProvider;
	private ArrayList<ChatBean> mChatList = new ArrayList<ChatBean>();
	private ListView lvList;
	private ChatAdapter mChatAdapter;
	private String[] welcome_array;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// 初始化语音引擎
		SpeechUtility.createUtility(this, SpeechConstant.APPID + "=5638926c");
		
		getActionBar().setDisplayHomeAsUpEnabled(true);

		lvList = (ListView) findViewById(R.id.lv_listview);
		mChatAdapter = new ChatAdapter();
		lvList.setAdapter(mChatAdapter);
		
		String welcome_item = getRandomWelcomItems();
		read(welcome_item);
	}

	private String getRandomWelcomItems() {
		String welcom_item = null;
		welcome_array = this.getResources().getStringArray(R.array.welcome_items);
		int index = (int) (Math.random() * (welcome_array.length - 1));
		welcom_item = welcome_array[index];
		return welcom_item;
	}

	class ChatAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return mChatList.size();
		}

		@Override
		public ChatBean getItem(int position) {
			return mChatList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = View.inflate(MainActivity.this, R.layout.chat_content_item, null);

				holder.tvAnswer = (TextView) convertView.findViewById(R.id.tv_answer);
				holder.tvAsk = (TextView) convertView.findViewById(R.id.tv_asker);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			ChatBean item = getItem(position);
			if (item.isAsker) {
				holder.tvAsk.setVisibility(View.VISIBLE);
				holder.tvAnswer.setVisibility(View.GONE);

				holder.tvAsk.setText(item.text);
			} else {
				holder.tvAsk.setVisibility(View.GONE);
				holder.tvAnswer.setVisibility(View.VISIBLE);

				holder.tvAnswer.setText(item.text);
			}
			return convertView;
		}

	}

	static class ViewHolder {
		public TextView tvAsk;
		public TextView tvAnswer;
	}

	/**
	 * @param view
	 *            开始语音识别
	 */
	public void startListen(View view) {
		// 1.创建RecognizerDialog对象
		RecognizerDialog mDialog = new RecognizerDialog(this, null);
		// 2.设置accent、language等参数
		mDialog.setParameter(SpeechConstant.LANGUAGE, "zh_cn");
		mDialog.setParameter(SpeechConstant.ACCENT, "mandarin");
		// 若要将UI控件用于语义理解， 必须添加以下参数设置， 设置之后onResult回调返回将是语义理解
		// 结果
		// mDialog.setParameter("asr_sch", "1");
		// mDialog.setParameter("nlp_version", "2.0");
		// 3.设置回调接口
		mDialog.setListener(mRecognizerDialogListener);
		// 4.显示dialog，接收语音输入
		mDialog.show();
	}

	/**
	 * @param view
	 *            语音朗诵
	 */
	public void read(String text) {
		// 1.创建 SpeechSynthesizer 对象, 第二个参数：本地合成时传 InitListener
		SpeechSynthesizer mTts = SpeechSynthesizer.createSynthesizer(this, null);
		// 2.合成参数设置，详见《科大讯飞MSC API手册(Android)》SpeechSynthesizer 类
		// 设置发音人（更多在线发音人，用户可参见 附录12.2
		mTts.setParameter(SpeechConstant.VOICE_NAME, "nannan"); // 设置发音人
		mTts.setParameter(SpeechConstant.SPEED, "50");// 设置语速
		mTts.setParameter(SpeechConstant.VOLUME, "80");// 设置音量，范围 0~100
		mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD); // 设置云端
		// 设置合成音频保存位置（可自定义保存位置） ，保存在“./sdcard/iflytek.pcm”
		// 保存在 SD 卡需要在 AndroidManifest.xml 添加写 SD 卡权限
		// 仅支持保存为 pcm 和 wav 格式，如果不需要保存合成音频，注释该行代码
		mTts.setParameter(SpeechConstant.TTS_AUDIO_PATH, "./sdcard/iflytek.pcm");
		// 3.开始合成
		mTts.startSpeaking(text, null);
	}

	StringBuffer mTextBuffer = new StringBuffer();
	private RecognizerDialogListener mRecognizerDialogListener = new RecognizerDialogListener() {

		@Override
		public void onResult(RecognizerResult results, boolean isLast) {
			String text = parseData(results.getResultString());
			mTextBuffer.append(text);
			if (isLast) { // 结束说话了
				String finalText = mTextBuffer.toString();
				mTextBuffer = new StringBuffer(); // 清理buff
				
				String url = GlobleConstant.COMMEN_URL + finalText;
				getDataFromTuLing(url);
				mChatList.add(new ChatBean(finalText, true)); // 添加到提问数据
				
				mChatAdapter.notifyDataSetChanged();

				lvList.setSelection(mChatList.size() - 1); // 定位到listview的末尾
			}
		}

		@Override
		public void onError(SpeechError arg0) {

		}
	};

	private void getDataFromTuLing(String url) {
		new HttpUtils().send(HttpMethod.GET, url, new RequestCallBack<String>() {

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				String answer = parseTuLingData(responseInfo.result);
				mChatList.add(new ChatBean(answer, false)); // 添加回答数据
				read(answer);
				mChatAdapter.notifyDataSetChanged();
				lvList.setSelection(mChatList.size() - 1); // 定位到listview的末尾
			}

			@Override
			public void onFailure(HttpException error, String msg) {
				Toast.makeText(MainActivity.this, " 连网失败" + msg, Toast.LENGTH_SHORT).show();
				error.printStackTrace();
			}
		});
	}

	/**
	 * @param response
	 *            解析网络数据
	 */
	protected String parseTuLingData(String response) {
		Gson gson = new Gson();
		TuLingBean mTuLingBean = gson.fromJson(response, TuLingBean.class);
		return mTuLingBean.text;
	}

	/**
	 * @param resultString
	 *            解析语音数据
	 */
	protected String parseData(String resultString) {
		Gson gson = new Gson();
		VoiceBean bean = gson.fromJson(resultString, VoiceBean.class);
		ArrayList<WSBean> ws = bean.ws;

		StringBuffer sb = new StringBuffer();
		for (WSBean wsBean : ws) {
			String text = wsBean.cw.get(0).w;
			sb.append(text);
		}

		return sb.toString();
	}

	private Intent getDefaultIntent() {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("image/*");
		return intent;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		MenuItem shareItem = menu.findItem(R.id.action_share);
		mShareActionProvider = (ShareActionProvider) shareItem.getActionProvider();
		mShareActionProvider.setShareIntent(getDefaultIntent());
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			// 点击设置的逻辑
			Toast.makeText(MainActivity.this, "Action_Settings", Toast.LENGTH_SHORT).show();
			break;
		case R.id.action_search:
			// 点击搜索的逻辑
			Toast.makeText(MainActivity.this, "can not search now", Toast.LENGTH_SHORT).show();
			break;
		case android.R.id.home:
			finish();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
