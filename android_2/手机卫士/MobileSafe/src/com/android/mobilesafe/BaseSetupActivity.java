package com.android.mobilesafe;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.widget.Toast;
import android.view.MotionEvent;
import android.view.View;

public abstract class BaseSetupActivity extends Activity {

	// 1.定义一个手势识别器
	private GestureDetector detector;
	
	protected SharedPreferences sp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		sp = getSharedPreferences("config", MODE_PRIVATE);
		// 2.实例化这个手势识别器
		detector = new GestureDetector(this, new OnGestureListener() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * android.view.GestureDetector.OnGestureListener#onFling(android.
			 * view.MotionEvent, android.view.MotionEvent, float, float)
			 * 当我们的手指在上面滑动的时候回调
			 */
			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
				// 屏蔽在x轴滑动很慢的情形
				if (Math.abs(velocityX)<200) {
					Toast.makeText(getApplicationContext(), "滑动得太慢了", Toast.LENGTH_SHORT).show();
					return true;
				}
				
				// 屏蔽斜滑这种情况
				if(Math.abs(e2.getRawY() - e1.getRawY()) > 100){
					Toast.makeText(getApplicationContext(), "不能这样滑", Toast.LENGTH_SHORT).show();
					return true;
				}
				
				if ((e2.getRawX() - e1.getRawX()) > 200) {
					// 显示上一个页面，从左往右滑动
					System.out.println("显示上一个页面，从左往右滑动");
					showPre();
					return true;
				}

				if ((e1.getRawX() - e2.getRawX()) > 200) {
					// 显示下一个页面，从右往左滑动
					System.out.println("显示下一个页面，从右往左滑动");
					showNext();
					return true; // 不要做其他事情了、、
				}

				return false;
			}

			@Override
			public boolean onDown(MotionEvent e) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void onShowPress(MotionEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public boolean onSingleTapUp(MotionEvent e) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void onLongPress(MotionEvent e) {
				// TODO Auto-generated method stub

			}

		});
	}

	public abstract void  showNext();
	public abstract void showPre();
	/**
	 * 下一步的点击事件
	 * @param view
	 */
	public void next(View view) {
		showNext();
	}

	/**
	 * 上一步
	 * @param view
	 */
	public void pre(View view) {
		showPre();
	}
	
	// 3.使用手势识别器
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		detector.onTouchEvent(event); // 把屏幕滑动事件，传给手势识别器
		return super.onTouchEvent(event);
	}
}
