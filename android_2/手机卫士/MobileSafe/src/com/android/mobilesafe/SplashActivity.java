package com.android.mobilesafe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.mobilesafe.utils.StreamTools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;
import android.widget.Toast;
import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;

public class SplashActivity extends Activity {

	private static final String TAG = "SplashActivity";
	protected static final int ENTER_HOME = 0;
	protected static final int SHOW_UPDATE_DIALOG = 1;
	protected static final int URL_ERROR = 2;
	protected static final int NETWORK_ERROR = 3;
	protected static final int JSON_ERROR = 4;
	private TextView tv_splash_version;
	private TextView tv_update_info;

	private String version;
	private String description;
	private String apkurl;
	
	private SharedPreferences sp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		sp = getSharedPreferences("config", MODE_PRIVATE);
		tv_splash_version = (TextView) findViewById(R.id.tv_splash_version);
		tv_splash_version.setText("手机卫士 " + getVersionName());
		tv_update_info = (TextView) findViewById(R.id.tv_update_info);
		boolean update = sp.getBoolean("update", false);
		
		installShorCut();
		
		//拷贝数据库
		copyDB("address.db");
		copyDB("antivirus.db");
		if (update) {
			// 检查升级
			checkUpdate();
		}else {
			// 自动更新已经关闭
			handler.postDelayed(new Runnable() {
				public void run() {
					// 进入主页面
					enterHome();
				}
			}, 2000);
		}
		
		//设置跳转时的渐出效果
		AlphaAnimation aa = new AlphaAnimation(0.2f, 1.0f);
		aa.setDuration(500);
		findViewById(R.id.rl_root_splash).startAnimation(aa);
	}

	/**
	 *  创建快捷图标
	 */
	private void installShorCut() {
		// 判断shortcut，使快捷图标只创建一次
		boolean shortcunt = sp.getBoolean("shortcut", false);
		if (shortcunt) {
			return;
		}
		Editor editor = sp.edit();
		// 发送广播的意图，告诉桌面应用，要创建快捷图标
		Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
		// 快捷方式  要包含3个重要信息 1.名称 2.图标 3.干什么事情
		intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "手机小卫士");
		intent.putExtra(Intent.EXTRA_SHORTCUT_ICON, BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher));
		// 桌面点击图标对应的意图
		Intent shortcutIntent = new Intent();
		// 指定的是main跟launcher(入口activity)  卸载的时候也会被从桌面清楚掉，如果是一个其他的activity就不会了
		shortcutIntent.setAction("android.intent.action.MAIN");
		shortcutIntent.addCategory("android.intent.category.LAUNCHER");
		shortcutIntent.setClassName(getPackageName(), "com.android.mobilesafe.SplashActivity");
		intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
		sendBroadcast(intent);
		editor.putBoolean("shortcut", true);
		editor.commit();
	}

	/**
	 *  path 把address.db这个数据库拷贝到data/data/包名/files/address
	 */
	private void copyDB(String filename) {
		//只要拷贝了一次，我就不要你再拷贝了
		try {
			File file= new File(getFilesDir(), filename);
			if (file.exists() && file.length()>0) {
				// 正常了不需要拷贝了
				Log.i(TAG, "正常了不需要拷贝了");
			}else {
				InputStream is = getAssets().open(filename);
				FileOutputStream fos = new FileOutputStream(file);
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = is.read(buffer)) != -1) {
					fos.write(buffer, 0, len);
				}
				is.close();
				fos.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			switch (msg.what) {
			case SHOW_UPDATE_DIALOG: //显示升级对话框
				Log.i(TAG, "显示升级对话框");
				showUpdateDialog();
				break;
			case ENTER_HOME://进入主页面
				enterHome();
				break;
			case URL_ERROR://URL错误
				enterHome();
				Toast.makeText(getApplicationContext(), "URL错误", Toast.LENGTH_SHORT).show();
				break;
			case NETWORK_ERROR://网络异常
				enterHome();
				Toast.makeText(getApplicationContext(), "网络异常，无法连接到服务器", Toast.LENGTH_SHORT).show();
				break;
			case JSON_ERROR://JSON解析出错
				enterHome();
				Toast.makeText(SplashActivity.this, "JSON解析出错", Toast.LENGTH_SHORT).show();
				break;

			default:
				break;
			}
		}

	};

	/*
	 * 检查是否有新版本，如果有就升级
	 */
	private void checkUpdate() {

		new Thread(new Runnable() {

			Message msg = Message.obtain();
			long startTime = System.currentTimeMillis();
			public void run() {
				try {
					// URL http://192.168.56.1:8080/updateinfo.html
					URL url = new URL(getString(R.string.serverurl));
					// 联网
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setConnectTimeout(4 * 1000);
					int code = conn.getResponseCode();
					if (code / 100 == 2) {
						// 联网成功
						InputStream inputStream = conn.getInputStream();
						// 把输入流转成String
						String result = StreamTools.readFromStream(inputStream);
						Log.i(TAG, "联网成功" + result);
						// json解析
						JSONObject obj = new JSONObject(result);
						version = (String) obj.get("version");
						description = obj.getString("description");
						apkurl = (String) obj.get("apkurl");

						// 校验是否有新版本
						if (getVersionName().equals(version)) {
							// 版本一致，没有新版本，进入主页面
							msg = Message.obtain();
							msg.what = ENTER_HOME;
						} else {
							// 有新版本，弹出一个升级对话框
							msg.what = SHOW_UPDATE_DIALOG;
						}
					}
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					msg.what = URL_ERROR;
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					msg.what = NETWORK_ERROR;
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					msg.what = JSON_ERROR;
					e.printStackTrace();
				} finally {
					
					long endTime = System.currentTimeMillis();
					//	花了多长时间
					long dTime = endTime - startTime;
					//2000
					if (dTime < 2000) {
						try {
							Thread.sleep(2000 - dTime);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					handler.sendMessage(msg);
				}
			}
		}).start();

	}

	/**
	 * 弹出升级对话框
	 */
	protected void showUpdateDialog() {
		AlertDialog.Builder builder = new Builder(this);
		builder.setTitle("升级提示");
//		builder.setCancelable(false);//强制升级
		builder.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				//	进入主页面
				enterHome();
				dialog.dismiss();
			}
		});
		builder.setMessage(description);
		builder.setPositiveButton("立刻升级", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//	下载APK，并且替换安装
				if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
					//sdcard存在
					//afinal
					FinalHttp finalHttp = new FinalHttp();
					finalHttp.download(apkurl, Environment.getExternalStorageDirectory().getAbsolutePath() + "/mobilesafe2.0.apk", 
							new AjaxCallBack<File>() {

								@Override
								public void onFailure(Throwable t, int errorNo, String strMsg) {
									t.printStackTrace();
									Toast.makeText(getApplicationContext(), "下载失败，请重新下载", Toast.LENGTH_LONG).show();
									super.onFailure(t, errorNo, strMsg);
								}

								@Override
								public void onLoading(long count, long current) {
									// TODO Auto-generated method stub
									super.onLoading(count, current);
									tv_update_info.setVisibility(View.VISIBLE);
									// 当前下载百分比
									int progress = (int) (current * 100 / count);
									
									tv_update_info.setText("下载进度："+ progress +"%");
								}

								@Override
								public void onSuccess(File t) {
									// TODO Auto-generated method stub
									super.onSuccess(t);
									installAPK(t);
								}

								/**
								 * 安装APK
								 * @param t
								 */
								private void installAPK(File t) {
									// TODO Auto-generated method stub
									Intent intent = new Intent();
									intent.setAction("android.intent.action.VIEW");
									intent.addCategory("android.intent.category.DEFAULT");
									intent.setDataAndType(Uri.fromFile(t), "application/vnd.android.package-archive");
								
									startActivity(intent);
								}
								
							});
				}else {
					Toast.makeText(getApplicationContext(), "没有sdcard，请安装后再试", Toast.LENGTH_SHORT).show();
					return;
				}
			}
		});
		builder.setNegativeButton("下次再说", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				enterHome(); // 进入主界面
			}
		});
		builder.show();
		
	}

	protected void enterHome() {
		Intent intent = new Intent(this, HomeActivity.class);
		startActivity(intent);
		//关闭当前页面
		finish();
	}

	/*
	 * 得到应用程序的版本名称
	 */
	private String getVersionName() {
		// 用来管理手机的APK
		PackageManager pm = getPackageManager();
		try {
			// 得到指定APK的功能清单文件
			PackageInfo info = pm.getPackageInfo(getPackageName(), 0);
			return info.versionName;
		} catch (NameNotFoundException e) {
			return "";
		}
	}

}
