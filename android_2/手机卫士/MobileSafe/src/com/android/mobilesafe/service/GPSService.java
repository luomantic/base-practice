package com.android.mobilesafe.service;

import java.io.IOException;
import java.io.InputStream;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;

public class GPSService extends Service {

	// 用到位置服务
	private LocationManager lm;
	private MyLocationListener listener;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		lm = (LocationManager) getSystemService(LOCATION_SERVICE);
		listener = new MyLocationListener();
		// 注册监听位置服务
		// 给位置提供者设置条件
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		// 还可以有很多参数设置的细节、、criteria.setxxx
		String provider = lm.getBestProvider(criteria, true);
		lm.requestLocationUpdates(provider, 60000, 50, listener);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		lm.removeUpdates(listener);
		listener = null; // 设置null，让垃圾回收机制更好的回收
	}

	class MyLocationListener implements LocationListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.location.LocationListener#onLocationChanged(android.location.
		 * Location) 位置发生改变的时候，回调
		 */
		@Override
		public void onLocationChanged(Location location) {
			String longitude = "j:" + location.getLongitude() + "\n"; //经度
			String latitude = "w:" + location.getLatitude() + "\n";//纬度
			String accuracy = "a:" + location.getAccuracy() + "\n";//精确度
			//发短信给安全号码
			
			//把标准的GPS坐标转换成火星坐标
			InputStream is;
			try {
				is = getAssets().open("axisoffset.dat");
				ModifyOffset offset = ModifyOffset.getInstance(is);
				PointDouble double1 = offset.s2c(new PointDouble(location.getLongitude(), location.getLatitude()));
				// 相当于转换过后重新接收一下
				longitude = "j:" + offset.X + "\n";
				latitude = "w:" + offset.Y+ "\n";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			SharedPreferences sp = getSharedPreferences("config", MODE_PRIVATE);
			Editor editor = sp.edit();
			editor.putString("lastlocation", longitude+latitude+accuracy);
			editor.commit();
			
		}

		/*
		 * (non-Javadoc)
		 * 当状态发生改变的时候回调 GSP——开启到关闭，关闭到开启
		 * @see
		 * android.location.LocationListener#onStatusChanged(java.lang.String,
		 * int, android.os.Bundle) 
		 */
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 某一个位置提供者可以使用了
		 * @see
		 * android.location.LocationListener#onProviderEnabled(java.lang.String)
		 */
		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		/*
		 * (non-Javadoc)
		 * 某一个位置提供者不可以使用了
		 * @see android.location.LocationListener#onProviderDisabled(java.lang.String) 
		 */
		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub

		}

	}

}
