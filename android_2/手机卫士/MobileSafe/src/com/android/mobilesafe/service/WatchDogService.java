package com.android.mobilesafe.service;

import java.util.List;

import com.android.mobilesafe.EnterPwdActivity;
import com.android.mobilesafe.db.dao.ApplockDao;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

/**
 * @author Administrator
 *	 看门狗服务代码，不停的监视当前手机里面运行的应用程序信息
 *	 只要发现应用程序需要保护，弹出来密码输入的界面
 */
public class WatchDogService extends Service {
	
	private ActivityManager am;
	private boolean flag;
	private ApplockDao dao;
	private InnerReceiver innerReceiver;
	private String tempStopProtectPackname;
	private DataChangeReciver dataChangeReciver;
	
	private ScreenOffReceiver offreceiver;
	
	private List<String> protectPacknames;
	private Intent intent;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	private class ScreenOffReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			tempStopProtectPackname = null;
		}
	}
	
	private class DataChangeReciver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			protectPacknames = dao.findAll();
		}
		
	}
	
	private class InnerReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			System.out.println("接受到了临时停止保护的广播事件");
			tempStopProtectPackname = intent.getStringExtra("packname");
		}
		
	}
	
	@Override
	public void onCreate() {
		offreceiver = new ScreenOffReceiver();
		registerReceiver(offreceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));
		
		innerReceiver = new InnerReceiver();
		registerReceiver(innerReceiver, new IntentFilter("com.android.mobilesafe.tempstop"));
		
		dataChangeReciver = new DataChangeReciver();
		registerReceiver(dataChangeReciver, new IntentFilter("com.android.mobilesafe.applockchange"));
		
		am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		flag = true;
		dao = new ApplockDao(this);
		protectPacknames = dao.findAll();
		intent = new Intent(getApplicationContext(), EnterPwdActivity.class);
		// 服务是没有任务栈信息的，在服务开启Activity，要指定这个activity运行的任务栈
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		new Thread(new Runnable() {
			public void run() {
				while (flag) {
					List<RunningTaskInfo> infos = am.getRunningTasks(1);
					String packname = infos.get(0).topActivity.getPackageName();
					//System.out.println("当前用户操作的应用程序：:"+packname);  // 狗狗培训好了
					//if (dao.find(packname)) { // 查询数据库太慢了，消耗资源，改成查询内存
					if (protectPacknames.contains(packname)) { // 查询内存效率高很多
						// 判断应用程序是否需要临时的停止保护
						if (packname.equals(tempStopProtectPackname)) {
							
						}else {
							// 当前应用需要保护  ，弹出一个密码输入界面
							intent.putExtra("packname", packname);
							startActivity(intent);
						}
					}
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();
		super.onCreate();
	}
	
	@Override
	public void onDestroy() {
		flag = false;
		unregisterReceiver(innerReceiver);
		innerReceiver = null;
		unregisterReceiver(offreceiver);
		offreceiver = null;
		unregisterReceiver(dataChangeReciver);
		dataChangeReciver = null;
		super.onDestroy();
	}
	
}
