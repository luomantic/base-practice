package com.android.mobilesafe;

import com.android.mobilesafe.utils.SmsUtils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class AtoolsActivity extends Activity {

	//窗体管理者
	private WindowManager wm;
	private ProgressDialog pd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_atools);
		//实例化窗体
		wm = (WindowManager) getSystemService(WINDOW_SERVICE);
	}
	
	/**
	 *  点击事件，进入号码归属地查询的页面
	 * @param view
	 */
	public void numberQuery(View view) {
		Intent intent = new Intent(this, NumberAddressQueryActivity.class);
		startActivity(intent);
	}
	
	/**
	 * 来电去电号码归属地的显示、、      （暂未开通此功能）
	 * @param view
	 */
	public void showAddress(View view) {
		// 自定义吐司
//		Toast.makeText(this, "暂未开启此功能，偷个小懒，嘿嘿、、、", Toast.LENGTH_LONG).show();
		String text = "暂未开启此功能，偷个小懒，嘿嘿、、、\n(我是自定义吐司喔~ duang~) ";
		myToast(text);
	}	

	/**
	 * 自定义土司
	 * @param address
	 */
	private void myToast(String text) {
		TextView view = new TextView(this);
		view.setBackgroundResource(R.drawable.shape_selector);
		view.setText(text);
		view.setTextSize(24);
		view.setTextColor(Color.LTGRAY);

		// 窗体的参数就设置好了
		WindowManager.LayoutParams params = new WindowManager.LayoutParams();
		params.height = WindowManager.LayoutParams.WRAP_CONTENT;
		params.width = WindowManager.LayoutParams.WRAP_CONTENT;
		/*
		//指定窗体默认为左上角对齐
		params.gravity = Gravity.TOP + Gravity.LEFT;
		//指定窗体距离左边100 上边100个像素
		params.x = 80;
		params.y = 400;
		*/
		params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
				| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
		params.format = PixelFormat.TRANSLUCENT;
		// android系统里面具有电话优先级的一种窗体类型，记得添加权限。
		params.type = WindowManager.LayoutParams.TYPE_PRIORITY_PHONE;
		wm.addView(view, params);
		
		/*
		 // 给view对象设置一个触摸监听器
		view.setOnTouchListener(new OnTouchListener() {
			int startX, startY;	//定义手指的初始化位置
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					startX = (int) event.getRawX();
					startY = (int) event.getRawY();
					break;
				case MotionEvent.ACTION_MOVE:
					int newX = (int) event.getRawX();
					int newY= (int) event.getRawY();
					int dx = newX - startX;
					int dy = newY - startY;
					params.x += dx;
					params.y += dy;
					// 重新初始化手机开始结束的位置
					startX = (int) event.getRawX();
					startY = (int) event.getRawY();
					wm.updateViewLayout(view, params);
					break;
				case MotionEvent.ACTION_UP:
					
					break;
				default:
					break;
				}
				return true; //	事件处理完毕了，不要让父控件响应触摸事件了
			}
		});
		*/
	}
	
	/**
	 * 点击事件，短信的备份
	 * @param view
	 */
	public void smsBackup1(View view) {
		try {
			SmsUtils.backupSms(this, pd);
			Toast.makeText(this, "备份成功", Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(this, "备份失败", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void smsBackup(View view) {
		pd = new ProgressDialog(this);
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pd.setMessage("正在备份短信");
		pd.show();
		
		new Thread(new Runnable() {
			public void run() {
				try {
					SmsUtils.backupSms(AtoolsActivity.this, pd);
					runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(AtoolsActivity.this, "备份成功", Toast.LENGTH_SHORT).show();
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
					runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(AtoolsActivity.this, "备份失败", Toast.LENGTH_SHORT).show();
						}
					});
				}finally {
					pd.dismiss();
				}
			}
		}).start();
		
	}
	
	/**
	 * 点击事件，短信的还原
	 * @param view
	 */
	public void smsRestore(View view) {
		// 避免还原很多次， 给一个界面提醒，是否清除掉旧的短信
//		SmsUtils.restoreSms(this);
		Toast.makeText(this, "还原成功++++++++  (未完成)", 0).show();
	}
}

