package com.android.mobilesafe.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Telephony.Sms;
import android.telephony.TelephonyManager;
import android.telephony.gsm.SmsManager;
import android.widget.Toast;

public class BootCompleteReceiver extends BroadcastReceiver {

	private SharedPreferences sp;
	private TelephonyManager tm;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		sp = context.getSharedPreferences("config", Context.MODE_PRIVATE);
		
		boolean protecting = sp.getBoolean("protecting", false);
		if (protecting) {
			// 开启防盗保护，才执行这个地方
			tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			
			// 读取之前保存的SiM卡信息
			String saveSim = sp.getString("sim", null);
			
			// 读取当前的SiM信息
			String realSim = tm.getSimSerialNumber();
			
			//比较是否一样
			if (saveSim.equals(realSim)) {
				//SiM卡没有变更，还是同一个人在使用手机
			}else {
				//SiM卡已经变更  发一个短信给安全号码
				System.out.println("SiM卡已经变更  发一个短信给安全号码");
//				Toast.makeText(context, "SiM卡已经变更  发一个短信给安全号码", Toast.LENGTH_LONG).show();
				
				SmsManager.getDefault().sendTextMessage(sp.getString("safenumber", null), null, "SiM卡已经变更...", null, null);
			}
		}
		
	}

}
