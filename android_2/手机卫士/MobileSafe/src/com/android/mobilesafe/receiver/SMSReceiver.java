package com.android.mobilesafe.receiver;

import com.android.mobilesafe.R;
import com.android.mobilesafe.service.GPSService;

import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

public class SMSReceiver extends BroadcastReceiver {

	private static final String TAG = "SMSReceiver";
	private SharedPreferences sp;
	/**
	 *  设备策略服务
	 */
	private DevicePolicyManager dpm;

	@Override
	public void onReceive(Context context, Intent intent) {
		// 写接受短信的代码
		Object[] objs = (Object[]) intent.getExtras().get("pdus");
		sp = context.getSharedPreferences("config", Context.MODE_PRIVATE);
		String safenumber = sp.getString("safe", null);
		dpm = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE );
		
		for (Object object : objs) {
			// 具体的某一条短信
			SmsMessage sms = SmsMessage.createFromPdu((byte[]) object);
			// 发送者
			String sender = sms.getOriginatingAddress();
			String body = sms.getMessageBody();
			if (sender.contains(safenumber)) {
				
				if ("#*location*#".equals(body)) {
					//得到手机的GPS
					Log.i(TAG, "得到手机的GPS");
					//启动服务
					Intent i = new Intent(context, GPSService.class);
					context.startService(i);
					SharedPreferences sp = context.getSharedPreferences("config",Context.MODE_PRIVATE);
					String lastlocation = sp.getString("lastlocation", null);
					if (TextUtils.isEmpty(lastlocation)) {
						//位置没有得到
						SmsManager.getDefault().sendTextMessage(sender, null, "正在获取位置信息...", null, null);
					}else {
						SmsManager.getDefault().sendTextMessage(sender, null, lastlocation, null, null);
					}
					
					//把这个广播终止掉
					abortBroadcast();
				}else if ("#*alarm*#".equals(body)) {
					//播放报警音乐
					Log.i(TAG, "播放报警音乐");
					MediaPlayer player = MediaPlayer.create(context,R.raw.ylzs);
					player.setLooping(true);//循环播放
					player.setVolume(1.0f, 1.0f);// 声音设到最大
					player.start();
					
					abortBroadcast();
				}else if ("#*wipedata*#".equals(body)) {
					//远程清除数据
					Log.i(TAG, "远程清除数据");
					// 创建一个Intent 
			        Intent intent_wipedata = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
			        // 我要激活谁
			        ComponentName mDeviceAdminSample = new ComponentName(context, MyDeviceAdminReceiver.class);
			        // 
			        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mDeviceAdminSample);
			        // 劝说用户开启管理员权限
			        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
			                "开启管理员权限，还送萌萌哒妹纸哟~ *(๑• ₃ •๑)*");
			        context.startActivity(intent_wipedata);
			        if (dpm.isAdminActive(mDeviceAdminSample)) {
						//清除Sdcard上的数据
						dpm.wipeData(DevicePolicyManager.WIPE_EXTERNAL_STORAGE);
						//恢复出厂设置
						dpm.wipeData(0);
					}else {
						Toast.makeText(context, "还没有打开管理员权限、、", Toast.LENGTH_LONG).show();
						return;
					}
					abortBroadcast();
				}else if ("#*lockscreen*#".equals(body)) {
					//远程锁屏
					Log.i(TAG, "远程锁屏");
					// 创建一个Intent 
			        Intent intent_wipedata = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
			        // 我要激活谁
			        ComponentName mDeviceAdminSample = new ComponentName(context, MyDeviceAdminReceiver.class);
			        // 
			        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mDeviceAdminSample);
			        // 劝说用户开启管理员权限
			        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
			                "开启管理员权限，还送萌萌哒妹纸哟~ *(๑• ₃ •๑)*");
			        context.startActivity(intent_wipedata);
			        if (dpm.isAdminActive(mDeviceAdminSample)) {
						dpm.lockNow();//锁屏
						dpm.resetPassword("", 0);//设置锁屏密码
					}else {
						Toast.makeText(context, "还没有打开管理员权限、、", Toast.LENGTH_LONG).show();
						return;
					}
					abortBroadcast();
				}
				
			}
		}
	}
	
}
