package com.android.mobilesafe;

import com.android.mobilesafe.service.CallSmsService;
import com.android.mobilesafe.service.WatchDogService;
import com.android.mobilesafe.ui.SettingItemView;
import com.android.mobilesafe.utils.ServiceUtils;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class SettingActivity extends Activity {

	// 设置是否开启自动更新
	private SettingItemView siv_update;
	private SharedPreferences sp;

	// 设置是否开启显示归属地
	private SettingItemView siv_show_address;
	private Intent showAddress; // 偷个小懒++

	// 黑名单拦截设置
	private SettingItemView siv_callsms_safe;
	private Intent callSmsSafeIntent;

	// 程序看门狗设置
	private SettingItemView siv_watchdog;
	private Intent watchDoaIntent;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		boolean isCallSmsServiceRunning = ServiceUtils.isServiceRunning(SettingActivity.this,
				"com.android.mobilesafe.service.CallSmsService");
		siv_callsms_safe.setChecked(isCallSmsServiceRunning);
		
		boolean isWatchDogServiceRunning = ServiceUtils.isServiceRunning(SettingActivity.this,
				"com.android.mobilesafe.service.WatchDogService");
		siv_watchdog.setChecked(isWatchDogServiceRunning);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		// 初始化设置是否开启自动更新
		sp = getSharedPreferences("config", MODE_PRIVATE);
		siv_update = (SettingItemView) findViewById(R.id.siv_update);

		boolean update = sp.getBoolean("update", false);
		if (update) {
			// 自动更新已开启
			siv_update.setChecked(true);
		} else {
			// 自动更新已关闭
			siv_update.setChecked(false);
		}
		siv_update.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Editor editor = sp.edit();
				// 判断是否选中
				if (siv_update.isChecked()) {
					// 已经关闭自动升级
					siv_update.setChecked(false);
					editor.putBoolean("update", false);
				} else {
					// 已经打开自动升级
					siv_update.setChecked(true);
					editor.putBoolean("update", true);
				}
				editor.commit();
			}
		});

		// 设置号码归属地显示控件
		siv_show_address = (SettingItemView) findViewById(R.id.siv_show_address);
		// showAddress = new Intent(this, addressser);
		siv_show_address.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (siv_show_address.isChecked()) {
					// 变为非选中状态
					siv_show_address.setChecked(false);
				} else {
					// 变为选中状态
					siv_show_address.setChecked(true);

				}
			}
		});

		// 黑名单拦截的设置
		siv_callsms_safe = (SettingItemView) findViewById(R.id.siv_callsms_safe);
		// showAddress = new Intent(this, addressser);
		callSmsSafeIntent = new Intent(this, CallSmsService.class);
		siv_callsms_safe.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (siv_callsms_safe.isChecked()) {
					// 变为非选中状态
					siv_callsms_safe.setChecked(false);
					stopService(callSmsSafeIntent);
				} else {
					// 变为选中状态
					siv_callsms_safe.setChecked(true);
					startService(callSmsSafeIntent);
				}
			}
		});

		// 程序锁的设置
		siv_watchdog = (SettingItemView) findViewById(R.id.siv_watchdog);
		// showAddress = new Intent(this, addressser);
		watchDoaIntent = new Intent(this, WatchDogService.class);
		siv_watchdog.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (siv_watchdog.isChecked()) {
					// 变为非选中状态
					siv_watchdog.setChecked(false);
					stopService(watchDoaIntent);
				} else {
					// 变为选中状态
					siv_watchdog.setChecked(true);
					startService(watchDoaIntent);
				}
			}
		});
	}

}
