package com.android.mobilesafe.domain;

/**
 * @author Administrator
 *		黑名单号码的业务bean
 */
public class BlackNumberInfo {
	private String Number;
	private String mode;
	
	public BlackNumberInfo() {
		super();
	}

	public BlackNumberInfo(String number, String mode) {
		super();
		Number = number;
		this.mode = mode;
	}
	
	public String getNumber() {
		return Number;
	}
	public void setNumber(String number) {
		Number = number;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}

	@Override
	public String toString() {
		return "BlackNumberInfo [Number=" + Number + ", mode=" + mode + "]";
	}
	
}
