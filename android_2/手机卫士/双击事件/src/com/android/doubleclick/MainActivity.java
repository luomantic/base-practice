package com.android.doubleclick;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	long firstClickTime;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void click(View view) {
		if (firstClickTime > 0) {
			long secondClickTime = SystemClock.uptimeMillis();
			long dtime = secondClickTime - firstClickTime;
			if (dtime<500) {
				Toast.makeText(this, "双击了", Toast.LENGTH_SHORT).show();
			}else {
				firstClickTime = 0;
			}
			return;
		}
		firstClickTime = SystemClock.uptimeMillis();  // CPU开机运行的开始时间
		new Thread(new Runnable() {
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				firstClickTime = 0;
			}
		}).start();
	}
	
}
