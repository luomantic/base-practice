

import java.security.MessageDigest;

public class Md5Encryption {
	
	public static void main(String[] args) throws Exception {
		//得到一个信息摘要器
		MessageDigest digest = MessageDigest.getInstance("md5");
		String password = "123";
		byte[] result = digest.digest(password.getBytes());
		StringBuffer buffer = new StringBuffer();
		//对每一个byte做一个与运算0xff; 
		for (byte b : result) {
			//与运算
			int number = b & 0xff;  // b & 0xfff   密码加盐
			String str = Integer.toHexString(number);
			System.out.println(str);
			if (str.length()==1) {
				buffer.append("0");
			}
			buffer.append(str);
		}
		
		//标准的MD5加密后的结果
		System.out.println(buffer.toString());
	}
	
}
