package com.example.gpsdemo;

import android.app.Activity;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity {

	//用到位置服务
	private LocationManager lm;
	private MyLocationListener listener;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		lm = (LocationManager) getSystemService(LOCATION_SERVICE);
		/*
		 * 三种定位:网络 基站 GPS	
			List<String> provider = lm.getAllProviders();
			for (String string : provider) {
				System.out.println(string);
			}
		*/
		listener = new MyLocationListener();
		//	注册监听位置服务
		//	给位置提供者设置条件
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		// 还可以有很多参数设置的细节、、criteria.setxxx
		String provider = lm.getBestProvider(criteria, true);
		lm.requestLocationUpdates(provider, 60000, 50, listener);
	}
	
	/**
	 * 取消监听位置服务
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		lm.removeUpdates(listener);
		listener = null; // 设置null，让垃圾回收机制更好的回收
	}
	
	class MyLocationListener implements LocationListener{

		/* (non-Javadoc)
		 * @see android.location.LocationListener#onLocationChanged(android.location.Location)
		 * 位置发生改变回调
		 */
		@Override
		public void onLocationChanged(Location location) {
			String longitude = "经度:" + location.getLongitude();
			String latitude = "纬度:" + location.getLatitude();
			String accuracy = "精确度:" + location.getAccuracy();
			TextView textView = new TextView(MainActivity.this);
			textView.setText(longitude + "\n" + latitude + "\n" + accuracy);
			setContentView(textView);
		}

		/* (non-Javadoc)
		 * @see android.location.LocationListener#onStatusChanged(java.lang.String, int, android.os.Bundle)
		 * 当状态发生改变的时候回调  GSP——开启到关闭，关闭到开启
		 */
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see android.location.LocationListener#onProviderEnabled(java.lang.String)
		 * 某一个位置提供者可以使用了
		 */
		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see android.location.LocationListener#onProviderDisabled(java.lang.String)
		 * 某一个位置提供者不可以使用了
		 */
		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}
		
	}
}
