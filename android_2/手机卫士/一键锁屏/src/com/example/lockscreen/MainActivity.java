package com.example.lockscreen;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	/**
	 *  设备策略服务
	 */
	private DevicePolicyManager dpm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		dpm = (DevicePolicyManager) getSystemService(DEVICE_POLICY_SERVICE);
	}
	
	/*
	 * 用代码去开启管理员
	 */
	public void openAdmin(View view) {
		// 创建一个Intent 
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        // 我要激活谁
        ComponentName mDeviceAdminSample = new ComponentName(this, MyAdmin.class);
        // 
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mDeviceAdminSample);
        // 劝说用户开启管理员权限
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                "开启管理员权限，还送萌萌哒妹纸哟~ *(๑• ₃ •๑)*");
        startActivity(intent);
	} 
	
	/**
	 * 一键锁屏
	 * @param view
	 */
	public void lockscreen(View view) {
		ComponentName mDeviceAdminSample = new ComponentName(this, MyAdmin.class);
		if (dpm.isAdminActive(mDeviceAdminSample)) {
			dpm.lockNow();//锁屏
			dpm.resetPassword("", 0);//设置锁屏密码
			
			//清除Sdcard上的数据
//			dpm.wipeData(DevicePolicyManager.WIPE_EXTERNAL_STORAGE);
			//恢复出厂设置
//			dpm.wipeData(0);
		}else {
			Toast.makeText(this, "还没有打开管理员权限、、", Toast.LENGTH_LONG).show();
			return;
		}

	}
	
	/**
	 * 卸载当前软件
	 * @param view
	 */
	public void uninstall(View view) {
		ComponentName mDeviceAdminSample = new ComponentName(this, MyAdmin.class);
		//1.先清除管理员权限
		dpm.removeActiveAdmin(mDeviceAdminSample);
		//2.普通应用的卸载
		Intent intent = new Intent();
		intent.setAction("android.intent.action.VIEW");
		intent.addCategory("android.intent.category.DEFAULT");
		intent.setData(Uri.parse("package:" + getPackageName()));
		startActivity(intent);
	}
}
