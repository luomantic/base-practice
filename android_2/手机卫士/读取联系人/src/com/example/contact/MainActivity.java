package com.example.contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class MainActivity extends Activity {

	private ListView select_contact;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		select_contact = (ListView) findViewById(R.id.select_contact);
		List<Map<String, Object>> data = getContactInfo();
		select_contact.setAdapter(new SimpleAdapter(MainActivity.this, data, R.layout.contact_item_view,
				new String[] { "name", "phone" }, new int[] { R.id.tv_name, R.id.tv_phone }));
	}

	/**
	 * 读取手机里面的联系人
	 * @return
	 */
	private List<Map<String, Object>> getContactInfo() {
		//把所有的联系人
		List<Map<String, Object>> lists = new ArrayList<Map<String,Object>>();
		
		// 得到一个内容解析器
		ContentResolver resolver = getContentResolver();
		// raw_contacts  uri
		Uri uri = Uri.parse("content://com.android.contacts/raw_contacts");
		Uri uriData = Uri.parse("content://com.android.contacts/data");
		
		Cursor cursor = resolver.query(uri, new String[]{"contact_id"}, null, null, null);
		
		while (cursor.moveToNext()) {
			String contact_id = cursor.getString(0);
			if (contact_id != null) {
				//	具体的某一个联系人
				Map<String, Object> map = new HashMap<String, Object>();
				
				Cursor datacursor = resolver.query(uriData, new String[] { "data1", "mimetype" }, "contact_id=?",
						new String[] { contact_id }, null);
				while (datacursor.moveToNext()) {
					String data1 = datacursor.getString(0);
					String mimetype = datacursor.getString(1);
					//Toast.makeText(this, "data1 = " + data1 + "mimetype = " + mimetype, Toast.LENGTH_LONG).show();
				
					if ("vnd.android.cursor.item/name".equals(mimetype)) {
						// 联系人姓名
						map.put("name", data1);
					}else if ("vnd.android.cursor.item/phone_v2".equals(mimetype)) {
						// 联系人的电话号码
						map.put("phone", data1);
					}
				}
				lists.add(map);
//				Toast.makeText(MainActivity.this, lists.toString(), Toast.LENGTH_LONG).show();
				datacursor.close();
			}
		}
		
		cursor.close();
		return lists;
	}
	
}
