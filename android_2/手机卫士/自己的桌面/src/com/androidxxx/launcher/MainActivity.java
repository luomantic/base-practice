package com.androidxxx.launcher;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private PackageManager pm;
	private List<String> packnames;
	private GridView lv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		lv = (GridView) findViewById(R.id.lv);
		pm = getPackageManager();
		Intent intent = new Intent();
		intent.setAction("android.intent.action.MAIN");
		intent.addCategory("android.intent.category.LAUNCHER");

		List<ResolveInfo> infos = pm.queryIntentActivities(intent, PackageManager.GET_INTENT_FILTERS);
		packnames = new ArrayList<String>();
		for (ResolveInfo resolveInfo : infos) {
			String packname = resolveInfo.activityInfo.packageName;
			packnames.add(packname);
		}

		lv.setAdapter(new Myadapter());
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				String packageName = packnames.get(position);
				Intent intent = pm.getLaunchIntentForPackage(packageName);
				startActivity(intent);
			}
		});
	}

	class Myadapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return packnames.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = View.inflate(getApplicationContext(), R.layout.item, null);
			TextView tv = (TextView) view.findViewById(R.id.tv);
		    ImageView iv = (ImageView) view.findViewById(R.id.iv);
			String packname = packnames.get(position);
			try {
				tv.setText(pm.getPackageInfo(packname, 0).applicationInfo.loadLabel(pm));
				iv.setImageDrawable(pm.getPackageInfo(packname, 0).applicationInfo.loadIcon(pm));
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			tv.setTextColor(Color.BLACK);
			return view;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

	}

	@Override
	public void onBackPressed() {
		Toast.makeText(getApplicationContext(), "�㻹���ȥ��  = =��", Toast.LENGTH_SHORT).show();
	}
	
}
