package com.example.bitmapdemo;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends Activity {

	private ImageView iv_github;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		iv_github = (ImageView) findViewById(R.id.iv_github);

//		final String url = "http://userimage2.360doc.com/11/0509/21/699443_201105092106330650.jpg";
		final String url = "http://www.farmer.com.cn/ywzt/jkfz/sp/201509/W020150929378093228904.bmp";

		new Thread(new Runnable() {
			public void run() {
				final Bitmap downloadBitmap = downloadBitmap(url);
				if (downloadBitmap!=null) {
					System.out.println("++++++++++++++++++Bitmap:" + downloadBitmap.getByteCount()/1024 + "k");
				}
				runOnUiThread(new Runnable() {
					public void run() {
						iv_github.setImageBitmap(downloadBitmap);
					}
				});
				
			}
		}).start();

	}

	private Bitmap downloadBitmap(String url) {
		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			connection.setRequestMethod("GET");
			connection.connect();

			int responseCode = connection.getResponseCode();

			if (responseCode == 200) {
				InputStream inputStream = connection.getInputStream();

				// 图片压缩处理
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 2;  // 宽高都变为原来二分之一
//				options.inPreferredConfig = Bitmap.Config.ALPHA_8;

				Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, options);
				return bitmap;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
		return null;
	}
}
