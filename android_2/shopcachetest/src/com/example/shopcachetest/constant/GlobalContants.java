package com.example.shopcachetest.constant;

/**
 * @author Administrator
 *	定义全局参数
 */
public class GlobalContants {

	public static final String SERVER_URL = "http://101.200.183.103:8888/shop";
	public static final String DETAIL_URL = "/topic?page=1&pageNum=5"; // 详情接口
	public static final String MAIN_URL = SERVER_URL + DETAIL_URL;
}
