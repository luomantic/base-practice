package com.example.shopcachetest.utils.bitmap;

import com.example.shopcachetest.R;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * @author Administrator
 * 	自定义图片加载工具
 */
public class MyBitmapUtils {

	NetCacheUtils mNetCacheUtils;
	LocalCacheUtils mLocalCacheUtils;
	MemoryCacheUtils mMemoryCacheUtils;
	
	public MyBitmapUtils() {
		mMemoryCacheUtils = new MemoryCacheUtils();
		mLocalCacheUtils = new LocalCacheUtils();
		mNetCacheUtils = new NetCacheUtils(mLocalCacheUtils, mMemoryCacheUtils);
	}
	
	public void display(ImageView iv_shop, String url) {
		iv_shop.setImageResource(R.drawable.pic_item_list_default); // 设置默认加载图片
		
		Bitmap bitmap = null;
		// 从内存读
		bitmap = mMemoryCacheUtils.getBitmapFromMemory(url);
		if (bitmap!=null) {
			iv_shop.setImageBitmap(bitmap);
//			System.out.println("从内存读取图片啦...");
			return;
		}
		
		// 从本地读
		bitmap = mLocalCacheUtils.getBitmapFromLocal(url);
		if (bitmap!=null) {
			iv_shop.setImageBitmap(bitmap);
//			System.out.println("从本地读取图片啦...");
			mMemoryCacheUtils.setBitmapToMemory(url, bitmap); // 将图片保存到内存
			return;
		}
		// 从网络读
		mNetCacheUtils.getBitmapFromNet(iv_shop, url);
	}

}
