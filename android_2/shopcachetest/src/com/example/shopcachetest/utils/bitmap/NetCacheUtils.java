package com.example.shopcachetest.utils.bitmap;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

/**
 * @author Administrator 网络缓存
 */
public class NetCacheUtils {

	private LocalCacheUtils mLocalCacheUtils;
	private MemoryCacheUtils memoryCacheUtils;
	public NetCacheUtils(LocalCacheUtils mLocalCacheUtils, MemoryCacheUtils memoryCacheUtils) {
		this.mLocalCacheUtils = mLocalCacheUtils;
		this.memoryCacheUtils = memoryCacheUtils;
	}

	/**
	 * 从网络下载图片
	 * 
	 * @param iv_shop
	 * @param url
	 */
	public void getBitmapFromNet(ImageView iv_shop, String url) {
		new BitmapTask().execute(iv_shop, url); // 启动AsyncTask，参数会在doInBackground中获取
	}

	/**
	 * @author Administrator handler和线程池的封装 第一个泛型：参数类型 第二个泛型：更新进度的泛型
	 *         第三个泛型：onPostExecute的返回结果
	 */
	class BitmapTask extends AsyncTask<Object, Void, Bitmap> {

		private ImageView iv_shop;
		private String url;

		/*
		 * 后台耗时方法在此执行，子线程
		 */
		@Override
		protected Bitmap doInBackground(Object... params) {
			iv_shop = (ImageView) params[0];
			url = (String) params[1];

			iv_shop.setTag(url); // 将url和imageview绑定
			
			return downloadBitmap(url);
		}

		/*
		 * 更新进度，主线程
		 */
		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}

		/*
		 * 耗时方法结束后，执行该方法，主线程
		 */
		@Override
		protected void onPostExecute(Bitmap result) {
			if (result!=null) {
				String bindUrl = (String) iv_shop.getTag();
				if (url.equals(bindUrl)) { // 确保图片设定给了正确的imageview
					iv_shop.setImageBitmap(result);
					mLocalCacheUtils.setBitmapToLocal(url, result);  // 将图片保存在本地
					memoryCacheUtils.setBitmapToMemory(url, result); // 将图片保存在内存
//					System.out.println("从网络缓存读取图片...");
				}
			}
		}
	}

	/**
	 * 下载图片
	 * @param url
	 * @return
	 */
	private Bitmap downloadBitmap(String url) {
		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			connection.setRequestMethod("GET");
			connection.connect();
			
			int responseCode = connection.getResponseCode();
			if (responseCode == 200) {
				InputStream inputStream = connection.getInputStream();
				
				// 图片压缩处理
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 2; // 宽高都压缩为原来的二分之一  (这个值还需要根据图片的大小来确定)
				options.inPreferredConfig = Config.ALPHA_8; //设置图片格式
				
				Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, options);
				return bitmap;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			connection.disconnect();
		}
		return null;
	}
	
}
