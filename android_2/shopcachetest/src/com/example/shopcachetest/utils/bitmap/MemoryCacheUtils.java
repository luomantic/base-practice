package com.example.shopcachetest.utils.bitmap;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

public class MemoryCacheUtils {

//	private HashMap<String, SoftReference<Bitmap>> mMemoryCache = new HashMap<String, SoftReference<Bitmap>>();
	private LruCache<String, Bitmap> mMemoryCache;
	
	public MemoryCacheUtils() {
		// 将内存控制在一定大小的范围内，这个最大值，自己定义
		long maxMemory = Runtime.getRuntime().maxMemory()/8; // app最大内存/8
		mMemoryCache = new LruCache<String, Bitmap>((int) maxMemory){
			@Override
			protected int sizeOf(String key, Bitmap value) {
				int byteCount = value.getRowBytes() * value.getHeight();
				return byteCount;
			}
		};
	}
	
	/**
	 * 从内存读
	 * 
	 * @param url
	 */
	public Bitmap getBitmapFromMemory(String url) {
//		SoftReference<Bitmap> softReference = mMemoryCache.get(url);
//		if (softReference != null) {
//			Bitmap bitmap = softReference.get();
//			return bitmap;
//		}
		return mMemoryCache.get(url);
	}

	/**
	 * 写内存
	 * 
	 * @param url
	 * @param bitmap
	 */
	public void setBitmapToMemory(String url, Bitmap bitmap) {
//		SoftReference<Bitmap> softReference = new SoftReference<Bitmap>(bitmap);
//		mMemoryCache.put(url, softReference);
		mMemoryCache.put(url, bitmap);
	}

}
