package com.example.shopcachetest.utils.bitmap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.example.shopcachetest.utils.MD5Utils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Environment;

/**
 * @author Administrator 本地缓存
 */
public class LocalCacheUtils {

	public static final String CACHE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath()
			+ "/shopdemo_cache";

	/**
	 * 向sdcard写图片
	 * 
	 * @param url
	 * @param bitmap
	 */
	public void setBitmapToLocal(String url, Bitmap bitmap) {
		try {
			String filename = MD5Utils.encode(url);
			
			File file = new File(CACHE_PATH, filename);
			
			File parentFile = file.getParentFile();
			if (!parentFile.exists()) { // 如果文件夹不存在，创建文件夹
				parentFile.mkdirs();
			}
			
			// 将图片保存在本地
			bitmap.compress(CompressFormat.JPEG, 100, new FileOutputStream(file));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 从本地sdcard读图片
	 */
	public Bitmap getBitmapFromLocal(String url) {
		try {
			String filename = MD5Utils.encode(url);
			File file = new File(CACHE_PATH, filename);
			if (file.exists()) {
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
				return bitmap;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
