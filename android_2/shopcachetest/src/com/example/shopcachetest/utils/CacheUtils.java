package com.example.shopcachetest.utils;

import android.content.Context;

/**
 * @author Administrator
 *  缓存工具类  缓存json
 */
public class CacheUtils {
	
	/**
	 * 设置缓存
	 * @param key    url
	 * @param value  json对象
	 * @param context 上下文对象
	 */
	public static void setCache(String key, String value, Context context) {
		PreUtils.setString(context, key, value);
	}
	
	/**
	 * 读缓存
	 * @param key url
	 * @param context 上下文对象
	 */
	public static String getCache(String key, Context context) {
		return PreUtils.getString(context, key, null);
	}
	
}
