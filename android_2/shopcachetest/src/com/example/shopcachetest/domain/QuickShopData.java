package com.example.shopcachetest.domain;

import java.util.ArrayList;

/**
 * @author Administrator
 * 商品促销页的数据
 */
public class QuickShopData {
	
	public ArrayList<TopicData> topic;
	
	public class TopicData {
		public int id;
		public String name;
		public String pic;
		
		@Override
		public String toString() {
			return "TopicData [id=" + id + ", name=" + name + ", pic=" + pic + "]";
		}
		
	}

	@Override
	public String toString() {
		return "QuickShopData [topic=" + topic + "]";
	}
	
}
