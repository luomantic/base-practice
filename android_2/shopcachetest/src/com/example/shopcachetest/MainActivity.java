package com.example.shopcachetest;

import java.util.ArrayList;

import com.example.shopcachetest.constant.GlobalContants;
import com.example.shopcachetest.domain.QuickShopData;
import com.example.shopcachetest.domain.QuickShopData.TopicData;
import com.example.shopcachetest.utils.CacheUtils;
import com.example.shopcachetest.utils.bitmap.MyBitmapUtils;
import com.google.gson.Gson;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private ListView lv_quick_shop;
	private ShopListAdapter adapter;
	private QuickShopData mQuickShopData;
	/**
	 * 商品列表数据的集合
	 */
	private ArrayList<TopicData> mTopicList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		lv_quick_shop = (ListView) findViewById(R.id.lv_quick_shop);
	}

	/**
	 * 点击从网络获取数据
	 */
	public void getData(View view) {
		getDataFromServer();
	}
	
	/**
	 * 从本地缓存加载数据
	 * @param view
	 */
	public void getDataFromLocalCache(View view) {
		String cache = CacheUtils.getCache(GlobalContants.MAIN_URL, MainActivity.this);
		if (!TextUtils.isEmpty(cache)) {
			parseData(cache);
		}else {
			Toast.makeText(MainActivity.this, "现在还没有本地缓存...", Toast.LENGTH_SHORT ).show();
		}
		getDataFromServer(); // 不管有没有缓存，都获取一下服务器数据，保证数据最新
	}
	
	/**
	 * 从网络获取数据
	 */
	private void getDataFromServer(){
		new HttpUtils().send(HttpMethod.GET, GlobalContants.MAIN_URL, 
				new RequestCallBack<String>() {

					// 主线程
					@Override
					public void onFailure(HttpException arg0, String arg1) {
						Toast.makeText(MainActivity.this, " 获取网络资源失败:"+arg1, Toast.LENGTH_SHORT).show();
						arg0.printStackTrace();
					}

					// 主线程
					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						parseData(arg0.result);
						CacheUtils.setCache(GlobalContants.MAIN_URL, arg0.result, MainActivity.this);
					}
				});
	}
	
	/**
	 * @param result
	 *  解析网络数据
	 */
	private void parseData(String result) {
		Gson gson = new Gson();
		mQuickShopData = gson.fromJson(result, QuickShopData.class);
		mTopicList = mQuickShopData.topic;
		if (mTopicList!=null) {
			adapter = new ShopListAdapter();
			lv_quick_shop.setAdapter(adapter);
		}
	}
	
	/**
	 * @author Administrator
	 * 商品列表适配器
	 */
	class ShopListAdapter extends BaseAdapter {

//		private BitmapUtils utils;
		private MyBitmapUtils utils;

		public ShopListAdapter() {
//			utils = new BitmapUtils(MainActivity.this);
//			utils.configDefaultLoadingImage(R.drawable.pic_item_list_default);
			utils = new MyBitmapUtils();
		}
		
		@Override
		public int getCount() {
			return mTopicList.size();
		}

		@Override
		public TopicData getItem(int position) {
			return mTopicList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView==null) {
				convertView = View.inflate(MainActivity.this, R.layout.list_view_item, null);
				holder = new ViewHolder();
				holder.tv_shop = (TextView) convertView.findViewById(R.id.tv_shop);
				holder.iv_shop = (ImageView) convertView.findViewById(R.id.iv_shop);
				
				convertView.setTag(holder);
			}else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			TopicData item = getItem(position);
			holder.tv_shop.setText(item.name);
			utils.display(holder.iv_shop, item.pic);
			
			return convertView;
		}
		
	}
	
	class ViewHolder{
		public TextView tv_shop;
		public ImageView iv_shop;
	}
	
}
