package com.example.push_test;

import android.app.Application;
import cn.jpush.android.api.JPushInterface;

public class MyApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		System.out.println("消息发送啦...");
		JPushInterface.setDebugMode(true);
		JPushInterface.init(this);
	}
	
	public void doSomething(){
		System.out.println("do something...");
	}
	
}
