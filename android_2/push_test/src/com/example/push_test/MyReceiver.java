package com.example.push_test;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import cn.jpush.android.api.JPushInterface;

/**
 * @author Administrator 
 * 	自定义接收器 如果不定义这个 Receiver，则： 
 * 	1) 默认用户会打开主界面 
 * 	2) 接收不到自定义消息
 */
public class MyReceiver extends BroadcastReceiver {

	private static final String TAG = "MyReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();
		Log.d(TAG, "onReceive - " + intent.getAction());
		
		if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {

		} else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
			System.out.println("接收到推送下来的自定义消息");
		} else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
			System.out.println("用户收到了通知");
		} else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
			System.out.println("用户点击了通知");
			String extra = bundle.getString(JPushInterface.EXTRA_EXTRA);
			Log.d(TAG, "extra:" + extra);
			
			try {
				JSONObject jsonObject = new JSONObject(extra);
				String url = jsonObject.getString("url");
				System.out.println(url);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
//			Intent i = new Intent(context, TestActivity.class);
//			context.startActivity(i);
		}
	}

}
