package com.luocanhui.googleplay;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;

public class MainActivity extends Activity implements OnQueryTextListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
	    // Add 3 tabs, specifying the tab's text and TabListener
	    for (int i = 0; i < 4; i++) {
	        actionBar.addTab(
	                actionBar.newTab()
	                        .setText("tab" + (i + 1))
	                        .setTabListener(new MyTabListener()));
	    }
	}
	
	class MyTabListener implements TabListener {

		@Override
		public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
			
		}

		@Override
		public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {
			
		}

		@Override
		public void onTabReselected(Tab tab, android.app.FragmentTransaction ft) {
			
		}
		
	} 
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		// copy from api Demo
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(this); // 搜索的监听
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
//		Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
//		Toast.makeText(this, newText, Toast.LENGTH_SHORT).show();
		return false;
	}
	
	/* 
	 * 处理actionBar菜单条目的点击事件
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_search) {
			Toast.makeText(MainActivity.this, "search clicked", Toast.LENGTH_SHORT).show();
		}else if (item.getItemId() == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}
