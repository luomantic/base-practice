package com.example.mywebview;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sharesdk.onekeyshare.OnekeyShareTheme;

public class MainActivity extends Activity {

	private WebView wv_web;
	private ProgressBar pb_progress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		wv_web = (WebView) findViewById(R.id.wv_web);
		pb_progress = (ProgressBar) findViewById(R.id.pb_progress);

		String url = new String("http://www.cnblogs.com/");
		WebSettings settings = wv_web.getSettings();
		// settings.setJavaScriptEnabled(true); // 表示支持js
		// settings.setBuiltInZoomControls(true); // 表示显示放大缩小按钮
		settings.setUseWideViewPort(true); // 支持双击缩放

		wv_web.setWebViewClient(new WebViewClient() {
			/*
			 * 开始加载网页
			 */
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				pb_progress.setVisibility(View.VISIBLE);
			}

			/*
			 * 网页加载结束
			 */
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				pb_progress.setVisibility(View.GONE);
			}

			/*
			 * 所有跳转的连接都会在此方法中回调
			 */
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				wv_web.loadUrl(url);
				return true;
			}
		});
		// wv_web.goBack();

		wv_web.setWebChromeClient(new WebChromeClient() {
			/*
			 * 进度发生变化
			 */
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				// TODO Auto-generated method stub
				System.out.println(newProgress);
				super.onProgressChanged(view, newProgress);
			}

			/*
			 * 获取网页标题
			 */
			@Override
			public void onReceivedTitle(WebView view, String title) {
				System.out.println(title);
				super.onReceivedTitle(view, title);
			}
		});

		wv_web.loadUrl(url);
	}

	/**
	 * 设置字体大小
	 * 
	 * @param view
	 */
	public void changeTextSize(View view) {
		showChooseDialog();
	}

	private int mCurrentChooseItem; // 记录当前选中的item，默认的字体
	private int mCurrentItem = 2; // 记录当前选中的字体
	/**
	 * 显示选择对话框
	 */
	private void showChooseDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		String[] items = new String[] { "超大号字体", "大号字体", "正常字体", "小号字体", "超小号字体" };
		builder.setTitle("字体设置");
		builder.setSingleChoiceItems(items, mCurrentItem, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				mCurrentChooseItem = which;
			}
		});
		builder.setNegativeButton("取消", null);
		builder.setPositiveButton("确定", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				WebSettings settings = wv_web.getSettings();
				switch (mCurrentChooseItem) {
				case 0:
					settings.setTextZoom(200);
//					settings.setTextSize(t);
					break;
				case 1:
					settings.setTextZoom(150);
					break;
				case 2:
					settings.setTextZoom(100);
					break;
				case 3:
					settings.setTextZoom(75);
					break;
				case 4:
					settings.setTextZoom(50);
					break;
				default:
					break;
				}
				mCurrentItem = mCurrentChooseItem;
				dialog.dismiss();
			}
		});
		builder.show();
	}

	/*
	 * 分享
	 */
	public void shareApp(View view) {
		showShare();
	}
	
	/**
	 * 分享，注意在sdcard根目录放test.jpg
	 */
	private void showShare() {
		 ShareSDK.initSDK(this);
		 OnekeyShare oks = new OnekeyShare();
		 
		 oks.setTheme(OnekeyShareTheme.SKYBLUE);//设置天蓝色的主题
		 
		 //关闭sso授权
		 oks.disableSSOWhenAuthorize(); 

		// 分享时Notification的图标和文字  2.5.9以后的版本不调用此方法
		 //oks.setNotification(R.drawable.ic_launcher, getString(R.string.app_name));
		 // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
		 oks.setTitle(getString(R.string.share));
		 // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
		 oks.setTitleUrl("http://sharesdk.cn");
		 // text是分享文本，所有平台都需要这个字段
		 oks.setText("我是分享文本_美女约吗？");
		 // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
		 oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
		 // url仅在微信（包括好友和朋友圈）中使用
		 oks.setUrl("http://sharesdk.cn");
		 // comment是我对这条分享的评论，仅在人人网和QQ空间使用
		 oks.setComment("我是测试评论文本");
		 // site是分享此内容的网站名称，仅在QQ空间使用
		 oks.setSite(getString(R.string.app_name));
		 // siteUrl是分享此内容的网站地址，仅在QQ空间使用
		 oks.setSiteUrl("http://sharesdk.cn");

		// 启动分享GUI
		 oks.show(this);
		 }
}
