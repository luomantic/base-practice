package com.luocanhui.binderdemo;

import android.os.Parcel;
import android.os.Parcelable;

public class Book implements Parcelable {
	public int bookId;
	public String bookName;
	
	public Book(int bookId, String bookName) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
	}
	
	private Book(Parcel in) {
		bookId = in.readInt();
		bookName = in.readString();
	}
	
	 /* (non-Javadoc)
	 * @see android.os.Parcelable#describeContents()
	 * android parcelable序列化对象的描述信息  一般情况下为0，有描述信息为1
	 */
	public int describeContents(){
		return 0;
	 }
	
	public void writeToParcel(Parcel out, int flags){
		out.writeInt(bookId);
		out.writeString(bookName);
	}
	
	public static final Parcelable.Creator<Book> CREATOR = new Creator<Book>() {
		
		@Override
		public Book[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Book[size];
		}
		
		@Override
		public Book createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Book(source);
		}
	};
}
