package com.luocanhui.binderdemo;

import java.util.List;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public class BookManagerImpl extends Binder implements IBookManager {

	/* Construct the stub at attach it to the interface*/
	public BookManagerImpl() {
		this.attachInterface(this, DESCRIPTOR);
	}
	
	/*
	 * Cast an IBinder object into an IbookManager Interface, generating a proxy
	 * if needed.
	 */
	public static IBookManager asInterface (IBinder obj) {
		if (obj == null) {
			return null;
		}
		IInterface iInterface = obj.queryLocalInterface(DESCRIPTOR);
		if ((iInterface != null) && (iInterface instanceof IBookManager)) {
			return (IBookManager) iInterface;
		}
		return new Proxy(obj);
	}
	
	@Override
	public IBinder asBinder() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	protected boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
		switch (code) {
		case INTERFACE_TRANSACTION:
			reply.writeString(DESCRIPTOR);
			return true;
		case TRANSCATION_getBookList:	
			data.enforceInterface(DESCRIPTOR);
			List<Book> result = this.getBookList();
			reply.writeNoException();
			reply.writeTypedList(result);
			return true;
		case TRANSCATION_addBook:
			data.enforceInterface(DESCRIPTOR);
			Book arg0;
			if ( data.readInt() != 0) {
				arg0 = Book.CREATOR.createFromParcel(data);
			}else {
				arg0 = null;
			}
			this.addBook(arg0);
			reply.writeNoException();
			return true;
		default:
			break;
		}
		return super.onTransact(code, data, reply, flags);
	}
	
	@Override
	public List<Book> getBookList() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addBook(Book book) throws RemoteException {
		// TODO Auto-generated method stub
	}

	private static class Proxy implements IBookManager {
		private IBinder mRemote;
		
		public Proxy(IBinder remote) {
			mRemote = remote;
		}
		
		@Override
		public IBinder asBinder() {
			return mRemote;
		}

		public String getInterfaceDescriptor() {
			return DESCRIPTOR;
		}
		
		// TODO Auto-generated method stub
		@Override
		public List<Book> getBookList() throws RemoteException {
			Parcel data =  Parcel.obtain();
			Parcel reply = Parcel.obtain();
			List<Book> result;
			try {
				data.writeInterfaceToken(DESCRIPTOR);
				mRemote.transact(TRANSCATION_getBookList, data, reply, 0);
				reply.readException();
				result = reply.createTypedArrayList(Book.CREATOR);
			} finally {
				reply.recycle();
				data.recycle();
			}
			return result;
		}

		@Override
		public void addBook(Book book) throws RemoteException {
			Parcel data = Parcel.obtain();
			Parcel reply = Parcel.obtain();
			try {
				data.writeInterfaceToken(DESCRIPTOR);
				if (book != null) {
					data.writeInt(1);
					book.writeToParcel(data, 0);
				} else {
					data.writeInt(0);
				}
				mRemote.transact(TRANSCATION_addBook, data, reply, 0);
				reply.readException();
			} finally {
				reply.recycle();
				data.recycle();
			}
		}
		
	}
	
}
