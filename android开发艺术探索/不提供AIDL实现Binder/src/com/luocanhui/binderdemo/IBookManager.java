package com.luocanhui.binderdemo;

import java.util.List;

import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;

/**
 * @author Administrator
 *		手写的Binder  只要在服务器端定义一个IBookManger接口，然后其实跟系统自己生产的代码是一样的，
 *只是为了加深对Binder的理解。实际过程中基本也是用系统自动生成的gen目录下的代码。
 */
public interface IBookManager extends IInterface {

	static final String DESCRIPTOR = "com.luocanhui.binderdemo.IBookManager";
	
	static final int TRANSCATION_getBookList = IBinder.FIRST_CALL_TRANSACTION + 0;
	
	static final int TRANSCATION_addBook = IBinder.FIRST_CALL_TRANSACTION + 1;
	
	public List<Book> getBookList() throws RemoteException;
	
	public void addBook(Book book) throws RemoteException;
}
