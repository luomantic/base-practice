package com.android.rpcalc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class CalcActivity extends Activity {

	private TextView tv_result;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calc);
		
		tv_result = (TextView) findViewById(R.id.tv_result);
		
		Intent intent = getIntent();
		String name = intent.getStringExtra("name");
		byte[] result = name.getBytes();
		int number = 0;
		for (byte b : result) {
			number += b&0xff;  // 每一个byte转换成十进制，然后相加
		}
		int score = Math.abs(number)%100;

		tv_result.setText(name + "的人品：" + score);
	}
}
