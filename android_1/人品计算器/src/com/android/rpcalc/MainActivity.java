package com.android.rpcalc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

	private EditText et_name;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		et_name = (EditText) findViewById(R.id.et_name);
	}
	
	public void enter(View view) {
		String name = et_name.getText().toString().trim();
		if (TextUtils.isEmpty(name)) {
			Toast.makeText(this, "请输入姓名", Toast.LENGTH_SHORT).show();;
			return;
		}
		
		//	显示意图		
		Intent intent = new Intent(this, CalcActivity.class);
//		intent.setClassName(getPackageName(), "com.android.rpcalc.CalcActivity");
		//	隐式意图
//		intent.setAction(action);
//		intent.setData(data);
		
/*	
 * 	Bundle extras = new Bundle();
		extras.putString(name, "name");
		intent.putExtras(extras);
*/		
	
		intent.putExtra("name", name);
		startActivity(intent);
	}
}
