package com.android.imageview2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity implements OnClickListener{
	private Button selectImageButton;
	private Button cutImageButton;
	private ImageView imageView;
	//声明两个静态的整型变量，主要是用于意图的返回的标志
	private static final int IMAGE_SELECT = 1;//选择图片
	private static final int IMAGE_CUT = 2;//裁剪图片
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		selectImageButton = (Button) findViewById(R.id.selectImageButton);
		cutImageButton = (Button) findViewById(R.id.cutImageButton);
		imageView = (android.widget.ImageView) findViewById(R.id.imageview);
		selectImageButton.setOnClickListener(this);
		cutImageButton.setOnClickListener(this);
//		imageView.setImageBitmap(bm);
	}
 
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode==RESULT_OK) {
			//处理图片按照手机的屏幕大小显示
			if (requestCode==IMAGE_SELECT) {
				Uri uri = data.getData();//获得图片的路径
				int dw = getWindowManager().getDefaultDisplay().getWidth();//获得屏幕的宽度
				int dh = getWindowManager().getDefaultDisplay().getHeight()/2;
				try {
					//实现对图片的裁剪的类，是一个匿名内部类
					BitmapFactory.Options factory = new BitmapFactory.Options();
					factory.inJustDecodeBounds = true;//如果设置为true，允许查询图片不是按照像素分配内存
					Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri),
							null,factory);
					//对图片的宽度和高度对应手机的屏幕进行匹配
					int hRatio = (int) Math.ceil(factory.outHeight/(float)dh);
					//如果大于1表示图片的高度大于手机屏幕的高度
					int wRatio =  (int) Math.ceil(factory.outWidth/(float)dw);
					//如果大于1表示图片的高度大于手机屏幕的高度
					//缩放到 1/radio 的尺寸和 1/radio^2像素
					if (hRatio>1 || wRatio>1) {
						if (hRatio>wRatio) {
							factory.inSampleSize = hRatio;
						}else{
							factory.inSampleSize = wRatio;
						}
					}
					//
					factory.inJustDecodeBounds = false;
					//使用BitmapFactory对图片进行适屏的操作
					bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri),null,factory);
					imageView.setImageBitmap(bitmap);
				} catch (Exception e) {
					// TODO: handle exception
				}
				//	表示裁剪图片
			}else if (requestCode==IMAGE_CUT) {
				Bitmap bitmap = data.getParcelableExtra("data");
				imageView.setImageBitmap(bitmap);
			}
		}
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.selectImageButton:
			//如何提取手机的图片，并且进行选择图片的功能
			Intent intent = new Intent(
					Intent.ACTION_PICK,
					android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);//打开手机的图片库
			startActivityForResult(intent, IMAGE_SELECT);
			break;
			
		case R.id.cutImageButton:
			Intent intent2 = getImageClipIntent();
			startActivityForResult(intent2, IMAGE_CUT);
			break;

		default:
			break;
		}
	}

	private Intent getImageClipIntent() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT,null);
		//实现对图片的裁剪，必须要设置图片的属性和大小
		intent.setType("image/*");//获取任意的图片类型
		intent.putExtra("crop", "true");//滑动选中图片区域
		intent.putExtra("aspectX", 1);//表示剪切框的比例1：1的效果
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", 80);//指定输出图片的大小
		intent.putExtra("outputY", 80);
		intent.putExtra("return-data", true);

		return intent;
	}
}
