package com.android.senddata;

import com.android.fragment_01.R;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {

	private Fragment03 fragment03;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//打开的时候默认...
		fragment03 = new Fragment03();
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.f1, fragment03);
		fragmentTransaction.commit();
	}

	//把fragment01的界面显示到帧布局中
	public void click1(View view) {
		//创建fragment对象
		Fragment01 fragment01 = new Fragment01();
		//获取fragment管理器
		FragmentManager fragmentManager = getFragmentManager();
		//打开事务
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		//把内容显示至帧布局
		fragmentTransaction.replace(R.id.f1, fragment01);
		//提交
		fragmentTransaction.commit();
	}

	public void click2(View view) {
		Fragment02 fragment02 = new Fragment02();
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.f1, fragment02);
		fragmentTransaction.commit();
	}

	public void click3(View view) {
		fragment03 = new Fragment03();
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.f1, fragment03);
		fragmentTransaction.commit();
	}
	
	public void click4(View view) {
		EditText et_main = (EditText) findViewById(R.id.et_main);
		String text = et_main.getText().toString();
		
		// 传递数据
		fragment03.setText(text);
	}
	
	public void setText(String text) {
		//这个findViewById是优先在fragment里面找的，在fragment里面找不到才来这里
		//	所以fragment里面的资源id跟main布局里面的资源id不能冲突
		EditText et_main = (EditText) findViewById(R.id.et_main);
		et_main.setText(text);
	}
}
