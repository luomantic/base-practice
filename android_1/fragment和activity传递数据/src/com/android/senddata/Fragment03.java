package com.android.senddata;

import com.android.fragment_01.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Fragment03 extends Fragment {
	
	private TextView tv;

	/* (non-Javadoc)
	 * @see android.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 * 返回的view对象会作为fragment03的内容显示在屏幕上
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, 
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment03, null);
		tv = (TextView) view.findViewById(R.id.tv);
		return view;
	}
	
	public void setText(String text){
		tv.setText(text);
	}
	
}
