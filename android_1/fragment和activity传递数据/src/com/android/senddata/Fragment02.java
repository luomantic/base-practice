package com.android.senddata;

import com.android.fragment_01.R;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class Fragment02 extends Fragment {
	
	/* (non-Javadoc)
	 * @see android.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 * 返回的view对象会作为fragment02的内容显示在屏幕上
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, 
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment02, null);
		
		final EditText et = (EditText) view.findViewById(R.id.et_frag);
		Button bt = (Button) view.findViewById(R.id.bt);
		bt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String text = et.getText().toString();
				//把数据传递给activity
				((MainActivity)getActivity()).setText(text);
			}
		});
		
		return view;
	}
	
}
