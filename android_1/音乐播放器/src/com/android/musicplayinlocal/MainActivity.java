package com.android.musicplayinlocal;

import java.io.File;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * @author Administrator
 *	没用到服务，没必要看...  看服务请移步本地音乐播放器
 */
public class MainActivity extends Activity implements OnClickListener{

	private EditText et_path;
	private Button bt_play, bt_pause, bt_stop, bt_replay;
	private MediaPlayer mediaPlayer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		et_path = (EditText) findViewById(R.id.et_path);
		bt_play = (Button) findViewById(R.id.bt_play);
		bt_pause = (Button) findViewById(R.id.bt_pause);
		bt_stop = (Button) findViewById(R.id.bt_stop);
		bt_replay = (Button) findViewById(R.id.bt_replay);
		
		mediaPlayer = new MediaPlayer();
		
		bt_play.setOnClickListener(this);
		bt_pause.setOnClickListener(this);
		bt_stop.setOnClickListener(this);
		bt_replay.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_play:
			play();
			break;
		case R.id.bt_pause:
			pause();
			break;
		case R.id.bt_stop:
			stop();
			break;
		case R.id.bt_replay:
			replay();
			break;
		default:
			break;
		}
	}
	
	public void play() {
		String filepath = et_path.getText().toString().trim();
		File file = new File(filepath);
		if (file.exists()) {
			try {
				mediaPlayer = new MediaPlayer();
				mediaPlayer.setDataSource(filepath);  // 设置播放的数据源
				mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
				mediaPlayer.prepare();//准备开始播放 播放的逻辑是c代码在新的线程里面执行的
				mediaPlayer.start();
				bt_play.setEnabled(false);
				mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						bt_play.setEnabled(true);
					}
				});
			} catch ( Exception e) {
				e.printStackTrace();
				Toast.makeText(this, "播放失败", Toast.LENGTH_SHORT).show();
			}
		}else {
			Toast.makeText(this, "文件不存在，请检查文件路径", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void pause() {
		if ("继续".equals(bt_pause.getText().toString())) {
			mediaPlayer.start();
			bt_pause.setText("暂停");
			return;
		}
		if (mediaPlayer!=null && mediaPlayer.isPlaying()) {
			mediaPlayer.pause();
			bt_pause.setText("继续");
		}
	}
	
	public void stop() {
		if (mediaPlayer!=null && mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
		}
		bt_pause.setText("暂停");
		bt_play.setEnabled(true);
	}
	
	public void replay() {
		if (mediaPlayer!=null && mediaPlayer.isPlaying()) {
			mediaPlayer.seekTo(0);
		}else {
			play();
		}
		bt_pause.setText("暂停");
	}
	
}
