package com.generic.test;

public class GenericTest {
	public static void main(String[] args) {
		Box<String> name = new Box<String>("corn");
		Box<Integer> age = new Box<Integer>(123);
		
		System.out.println("name class:"+name.getClass());
		System.out.println("age class:"+age.getClass());
		System.out.println(name.getClass()==age.getClass());
		
		System.out.println("name data:"+name.getData());
		System.out.println("age data:"+age.getData());
	}
}

class Box<T> {
	private T data;
	
	public Box() {
		// TODO Auto-generated constructor stub
	}
	
	public Box(T data) {
		this.data = data;
	}
	
	public T getData() {
		return data;
	}
}

//getData(name);

//The method getData(Box<Number>) in the type GenericTest is 
//not applicable for the arguments (Box<Integer>)

//  getData(age); 
