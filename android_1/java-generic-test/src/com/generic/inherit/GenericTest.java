package com.generic.inherit;

public class GenericTest {
	
	public static void main(String[] args) {
		Box<Number> name = new Box<Number>(123);
		Box<Integer> age = new Box<Integer>(456);
		
		getData(name);
		
		//The method getData(Box<Number>) in the type GenericTest is 
		//not applicable for the arguments (Box<Integer>)
	
//		getData(age); 
		System.out.println("age-data:"+age.getData());
	}
	
	public static void getData(Box<Number> data){
		System.out.println("data:"+data.getData());
	}	
	
}

class Box<T> {
	private T data;
	
	public Box() {
		// TODO Auto-generated constructor stub
	}
	
	public Box(T data) {
		this.data = data;
	}
	
	public T getData() {
		return data;
	}
}
