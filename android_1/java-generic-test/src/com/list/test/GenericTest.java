package com.list.test;

import java.util.ArrayList;
import java.util.List;

public class GenericTest {
	/*
	 * public static void main1(String[] args) {
			List list = new ArrayList();

			// list.add("hellodubby");
			list.add(111);
			list.add("helloworld");
			list.add("hellokitty");

			for (int i = 0; i < list.size(); i++) {
				// String msg = (String) list.get(i);
				// System.out.println("msg:"+msg);

				int num = (int) list.get(i);
				System.out.println("num:" + num);
			}
		}
	 */

	public static void main1(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("111");
		list.add("helloworld");
		list.add("hellokitty");

		for (int i = 0; i < list.size(); i++) {
			String msg = (String) list.get(i);
			System.out.println("msg:" + msg);
		}
	}
}
