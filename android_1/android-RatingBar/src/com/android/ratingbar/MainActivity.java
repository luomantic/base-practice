package com.android.ratingbar;

import android.app.Activity;
import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.Toast;

public class MainActivity extends Activity implements OnRatingBarChangeListener{
	private RatingBar ratingBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ratingBar = (RatingBar) findViewById(R.id.ratrinBar);
		ratingBar.setNumStars(5);
		ratingBar.setMax(100);//设置最大刻度
		ratingBar.setProgress(20);//设置当前的刻度
		
		ratingBar.setOnRatingBarChangeListener(this);
	}

	@Override
	public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
		// TODO Auto-generated method stub
		int progress = ratingBar.getProgress();
		Toast.makeText(MainActivity.this, "progresss:"+progress+"	rating:"+rating, 
				Toast.LENGTH_SHORT).show();
	}
}
