package com.example.broadcasttest;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends Activity {

	private NetworkChangeReceiver networkChangeReceiver;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//	定义一个意图过滤器
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
		
		networkChangeReceiver = new NetworkChangeReceiver();
		
		//	动态注册
		registerReceiver(networkChangeReceiver, intentFilter);
	}
	
	@Override
	protected void onDestroy() {
		unregisterReceiver(networkChangeReceiver);
	}
	
	/**
	 * @author Administrator
	 *定义一个广播接收器
	 */
	class NetworkChangeReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
//			Toast.makeText(MainActivity.this, "Network Changes", Toast.LENGTH_SHORT)
//				.show();
			ConnectivityManager connectivityManager =  (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isAvailable()) {
				Toast.makeText(MainActivity.this, "network is avaliable", Toast.LENGTH_SHORT).show();
			}else {
				Toast.makeText(MainActivity.this, "network is unavaliable", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
}
