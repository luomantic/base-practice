package com.example.android_alertdialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	private Button button;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 先要创建一个Builder对象,Builder完成对dialog基本信息的构建
				AlertDialog.Builder builder = new AlertDialog.Builder(
						MainActivity.this);
				builder.setTitle("提示");
				builder.setMessage("你确定要删除吗？"); 
				builder.setIcon(R.drawable.ic_launcher);
				
				builder.setPositiveButton("确定", new android.content.DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						//完成用户的操作，比如删除数据，提交请求
						dialog.dismiss();//让对话框消失
					}
				});
				builder.setNegativeButton("取消", new android.content.DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						//取消用户的操作，比如停止下载
					}
				});
				builder.setNeutralButton("忽略", new android.content.DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				});
				AlertDialog alertDialog = builder.create();//创建一个dialog
				alertDialog.show();//让对话框显示
			}
		});
	}
}
