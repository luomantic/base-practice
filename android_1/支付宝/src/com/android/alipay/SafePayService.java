package com.android.alipay;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

public class SafePayService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return new Mybinder();
	}

	/**
	 * e.g 模拟安全支付的方法，开发的时候，参数特别多
	 * @param time
	 * @param pwd
	 * @param money
	 */
	private boolean pay(long time, String password, double money) {
		if ("123".equals(password)) {
			return true;
		} else {
			return false;
		}
	}
	
	private class Mybinder extends ISafePay.Stub {
		
		/* (non-Javadoc)
		 * @see com.android.alipay.ISafePay#callPay(long, java.lang.String, double)
		 * 调用安全支付的逻辑
		 */
		@Override
		public boolean callPay(long time, String password, double money) throws RemoteException {
			
			boolean flag = pay(time, password, money);
			
			return flag;
		}
		
	}
	
	@Override
	public void onCreate() {
		System.out.println("支付宝服务被创建，需要一直运行，检测用户的手机的支付环境");
		super.onCreate();
	}
	
	@Override
	public void onDestroy() {
		System.out.println("支付宝服务被销毁");
		super.onDestroy();
	}
}
