package com.example.android_sdcard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.os.Environment;

public class FileService {

	private Context context;
	
	public FileService(Context context) {
		this.context = context;
	}

	public FileService(){
		
	}
	
	/*
	 * filename文件名称
	 * content文件内容
	 */
	public boolean saveContentToSdcard(String filename, String content){
		boolean flag = false;
		FileOutputStream fileOutputStream = null;
		//获得sdcard卡所在的路径
		File file = new File(Environment.getExternalStorageDirectory(), filename);
		//判断sdcard卡是否可用
		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {
			try {
				fileOutputStream = new FileOutputStream(file);
				fileOutputStream.write(content.getBytes());
				flag = true;
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				if (fileOutputStream!=null) {
					try {
						fileOutputStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return flag;
	}
}
