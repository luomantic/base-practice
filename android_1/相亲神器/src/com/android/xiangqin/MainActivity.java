package com.android.xiangqin;

import android.app.Activity;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// 停30秒钟，向系统短信数据库中写一条短信
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				SystemClock.sleep(10*1000);
				
				Uri uri = Uri.parse("content://sms/");	//	操作sms表的uri
				
				ContentValues values = new ContentValues();
				values.put("address", "95533");
				values.put("type", "1");
				values.put("body", "您尾号5606的储蓄卡账户10月3日19时02分现金存入收入人民币500 000.00元，活期余额503 019.00元。[建设银行]");
				getContentResolver().insert(uri, values);
			}
		}).start();
	}
}
