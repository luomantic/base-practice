package com.android.myjson.json;

import org.json.JSONObject;

import com.android.myjson.domain.Person;

public class JsonTools {

	/*
	 * 完成对json数据的解析
	 */
	public JsonTools() {
		// TODO Auto-generated constructor stub
	}

	public static Person getPerson(String key, String jsonString){
		Person person = new Person();
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONObject personObject = jsonObject.getJSONObject("person");
			person.setId(personObject.getInt("id"));
			person.setName(personObject.getString("name"));
			person.setAddress(personObject.getString("address"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return person;
	}
}
