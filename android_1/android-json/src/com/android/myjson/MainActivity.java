package com.android.myjson;

import com.android.myjson.domain.Person;
import com.android.myjson.http.HttpUtils;
import com.android.myjson.json.JsonTools;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {
	private Button person, persons, liststring, listmap;
	private static final String TAG = "MainActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		person = (Button) findViewById(R.id.person);
		persons = (Button) findViewById(R.id.persons);
		liststring = (Button) findViewById(R.id.liststring);
		listmap = (Button) findViewById(R.id.listmap);
		person.setOnClickListener(this);
		persons.setOnClickListener(this);
		liststring.setOnClickListener(this);
		listmap.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.person:
			String path = "http://localhost:8080/jsonProject/servlet/JsonAction?action_flag=person";
			String jsonString = HttpUtils.getJsonContent(path);
			Person person = JsonTools.getPerson("person", jsonString);
			Log.i(TAG, person.toString());
			break;
		case R.id.persons:

			break;
		case R.id.liststring:

			break;
		case R.id.listmap:

			break;

		default:
			break;
		}
	}
}
