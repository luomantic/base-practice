package com.android.newseasedemo;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;

import com.android.newseasedemo.entities.NewsInfo;
import com.loopj.android.image.SmartImageView;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static final String TAG = "MainActivity";
	private final int SUCCESS = 0;
	private final int FAILED = 1;
	private ListView lvNews;
	private List<NewsInfo> newsInfoList;
	
	private Handler handler	= new Handler() {
		/*
		 * 	接收消息
		 */
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case SUCCESS:	//	访问成功，有数据
				//	给ListView列表绑定数据
				
				newsInfoList = (List<NewsInfo>) msg.obj;
				
				MyAdapter adapter = new MyAdapter();
				lvNews.setAdapter(adapter);
				break;
			case FAILED:	//	访问失败，无数据
				Toast.makeText(MainActivity.this, "获取新闻失败，请检查您的网络连接", Toast.LENGTH_SHORT).show();
				break;
			default:
				break;
			}
		}
		
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		init();
	}

	private void init() {
		lvNews = (ListView) findViewById(R.id.lv_news);
		
		//	抓取新闻数据
		new Thread(new Runnable() {
			public void run() {
				//	获得新闻实体集合
				List<NewsInfo> newsInfoList = getNewsFromInternet();
				Message msg = new Message();
				if (newsInfoList != null) {
					msg.what = SUCCESS;
					msg.obj = newsInfoList;
				} else {
					msg.what = FAILED;
				}
				handler.sendMessage(msg);
			}
		}).start();
		
	}

	/*
	 *  返回新闻信息
	 */
	private List<NewsInfo> getNewsFromInternet() {
		HttpClient client = null;
		try {
			//	定义一个客户端
			client = new DefaultHttpClient();
			
			//	定义get方法
			HttpGet get = new HttpGet("http://192.168.56.1:8080/NetEaseServer/new.xml");
			
			//	执行请求
			HttpResponse response = client.execute(get);
			
			int statusCode = response.getStatusLine().getStatusCode();
			
			if (statusCode == 200) {
				InputStream inputStream = response.getEntity().getContent();
				List<NewsInfo> newsInfoList = getNewListFromInputStream(inputStream);
				
				Log.i(TAG, "访问成功:	" + statusCode);
				
				return newsInfoList;
			}else {
				Log.i(TAG, "访问失败:	" + statusCode);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (client != null) {
				client.getConnectionManager().shutdown();
			}
		}
		
		return null;
	}
	
	/**
	 *   从流中解析新闻集合
	 * @param inputStream
	 * @return
	 * @throws Exception 
	 */
	private List<NewsInfo> getNewListFromInputStream(InputStream inputStream) throws Exception {
		XmlPullParser parser = Xml.newPullParser();	//	创建一个pull解析器
		parser.setInput(inputStream, "utf-8"); // 指定解析流和编码
		
		int eventType = parser.getEventType();
		
		List<NewsInfo> newsInfoList = null;
		NewsInfo newsInfo = null;
		
		while (eventType != XmlPullParser.END_DOCUMENT) {	  //  如果没有到结尾处，继续循环
			
			String tagName = parser.getName();	//	节点名称
			switch (eventType) {
			case XmlPullParser.START_TAG:
				if ("news".equals(tagName)) {
					newsInfoList = new ArrayList<NewsInfo>();
				} else if ("new".equals(tagName)) {
					newsInfo = new NewsInfo();
				} else if ("title".equals(tagName)) {
					newsInfo.setTitle(parser.nextText());
				} else if ("detail".equals(tagName)) {
					newsInfo.setDetail(parser.nextText());
				} else if ("comment".equals(tagName)) {
					newsInfo.setComment(Integer.valueOf(parser.nextText()));
				} else if ("image".equals(tagName)) {
					newsInfo.setImageUrl(parser.nextText());
				}
				break;
			case XmlPullParser.END_TAG:	 // 	</new>
				if ("new".equals(tagName)) {
					newsInfoList.add(newsInfo);
				}
				break;
			default:
				break;
			}
			eventType = parser.next();	//  取下一个事件类型
		}
		
		return newsInfoList;
	}
	
	class MyAdapter extends BaseAdapter {

		/*
		 *  返回列表的总长度
		 */
		@Override
		public int getCount() {
			return newsInfoList.size();
		}

		/*
		 *   返回一个列表的字条目的布局
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = null;
			
			if (convertView == null) {
				LayoutInflater inflater = getLayoutInflater();
				view = inflater.inflate(R.layout.listview_item, null);
			} else {
				view = convertView;
			}
			
			//	重新赋值，不会产生缓存对象中原有数据保留的现象    ——>（++idear 下拉刷新）
			// 	注意已经不是activity_main里面的xml布局，是view里面的。细节，空指针异常
			SmartImageView sivIcon = (SmartImageView) view.findViewById(R.id.siv_listview_item_icon);
			TextView tvTitle = (TextView) view.findViewById(R.id.tv_listvew_item_title);	   
			TextView tvDetail = (TextView) view.findViewById(R.id.tv_listvew_item_detail);
			TextView tvComment = (TextView) view.findViewById(R.id.tv_listvew_item_comment);
			
			NewsInfo newsInfo = newsInfoList.get(position);
			
			String url = "192.168.56.1" + newsInfo.getImageUrl().substring(15, newsInfo.getImageUrl().length());
			Log.i(TAG, "url：" + url);
			sivIcon.setImageUrl(url); // 设置图片
			tvTitle.setText(newsInfo.getTitle());
			tvDetail.setText(newsInfo.getDetail());
			tvComment.setText(newsInfo.getComment() + "跟帖");
			
			return view;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
	}
}
