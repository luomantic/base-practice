package com.sax.handle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MyHandler extends DefaultHandler {
	//	存储单个的解析对象
	private HashMap<String, String> map = null;
	//	存储所有的解析对象
	private List<HashMap<String, String>> list = null;
	//	正在解析的元素标签
	private String currentTag = null;
	//	解析当前元素的值
	private String currentValue = null;
	//	解析当前的节点名称
	private String nodeName = null;
	
	public List<HashMap<String, String>> getList() {
		return list;
	}

	public MyHandler(String nodeName) {
		this.nodeName = nodeName;
	}
	
	@Override
	public void startDocument() throws SAXException {
		//	当读到第一个开始标签的时候，会处罚这个方法
		list = new ArrayList<HashMap<String, String>>();
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		//	当遇到文档的开头的时候，调用这个方法
		if (qName.equals(nodeName)) {
			map = new HashMap<String, String>();
		}
		if (attributes!=null && map!=null) {
			for (int i=0; i<attributes.getLength(); i++) {
				map.put(attributes.getQName(i), attributes.getValue(i));
			}
		}
		currentTag = qName;
	}
	
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		//	这个方法是用来处理xml文件所读取到的内容
		if (currentTag!=null && map!=null) {
			currentValue = new String(ch, start, length);
			if (currentValue!=null && currentValue.trim().equals("") && currentValue.trim().equals("\n")) {
				map.put(currentTag, currentValue);
			}
		}
		//	把当前节点的对应的值和标签设置为空
		currentTag = null;
		currentValue = null;
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		// 遇到结束标记的时候，会调用这个方法
		if (qName.equals(nodeName)) {
			list.add(map);
			map = null;
		}
	}
	
}
