package com.sax.service;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.sax.handle.MyHandler;

public class SaxService {
	public SaxService() {
		// TODO Auto-generated constructor stub
	}
	
	public static List<HashMap<String, String>> readXML(
			InputStream inputStreamm, String nodeName){
		try {
			//	创建一个解析XML的工厂对象
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser parser = spf.newSAXParser();
			MyHandler handler = new MyHandler(nodeName);
			parser.parse(inputStreamm, handler);
			inputStreamm.close();
			return handler.getList();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
}
