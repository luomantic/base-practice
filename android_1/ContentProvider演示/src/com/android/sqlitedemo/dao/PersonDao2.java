package com.android.sqlitedemo.dao;

import java.util.ArrayList;
import java.util.List;

import com.android.sqlitedemo.db.PersonSQLiteOpenHelper;
import com.android.sqlitedemo.entities.Person;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class PersonDao2 {
	
	private static final String TAG = "PersonDao2";
	private PersonSQLiteOpenHelper mOpenHelper;	//	数据库的帮助类对象

	public PersonDao2(Context context) {
		mOpenHelper = new PersonSQLiteOpenHelper(context);
	}
	
	/**
	 * 添加到person表一条数据
	 * @param person
	 */
	public void insert(Person person) {
		SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		if(db.isOpen()) {	//	如果数据库打开，执行添加的操作
			
			ContentValues values = 	new ContentValues();
			values.put("name", person.getName());
			values.put("age", person.getAge());
			long id = db.insert("person", null, values);
			Log.i(TAG, "id:" +	id);
			
			db.close(); //	数据库关闭
		}
	}
	
	public void delete(int id) {
		SQLiteDatabase db = mOpenHelper.getWritableDatabase();	//	获得可写的数据库对象
		if(db.isOpen()) {	//	如果数据库打开，执行添加的操作
			
			db.close(); //	数据库关闭
		}
	}
	
	public void update(int id, String name) {
		SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		if(db.isOpen()) {	//	如果数据库打开，执行添加的操作
			
			db.close(); //	数据库关闭
		}
	}
	
	public List<Person> queryAll() {
		SQLiteDatabase db = mOpenHelper.getReadableDatabase();	// 获得一个只读的数据库对象
		if(db.isOpen()) {	//	如果数据库打开，执行添加的操作
			
			db.close(); //	数据库关闭
		}
		
		return null;
	}
	
	public Person queryItem(int id) {
		SQLiteDatabase db = mOpenHelper.getReadableDatabase();
		
		if (db.isOpen()) {
			
		}
		
		db.close();
		
		return null;
	}
}
