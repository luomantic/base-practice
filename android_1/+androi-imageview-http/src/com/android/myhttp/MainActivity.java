package com.android.myhttp;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity {
	private Button button;
	private ImageView imageview;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		button = (Button) findViewById(R.id.button);
		imageview = (ImageView) findViewById(R.id.imageview);
		button.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				try {
//					InputStream inputStream = HttpUtils.getImageViewInputStream();
//					Bitmap bitmap = BitmapFactory.decodeStream(inputStream); 
//					imageview.setImageBitmap(bitmap);
//				} catch (Exception e) {
//					// TODO: handle exception
//				}		
				byte[] data = HttpUtils.getImageViewArray();
				Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
				imageview.setImageBitmap(bitmap);
			}
		});
	}
}
