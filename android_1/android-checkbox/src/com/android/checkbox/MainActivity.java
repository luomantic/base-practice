package com.android.checkbox;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

public class MainActivity extends Activity {
	private List<CheckBox> checkBoxs  = new ArrayList<CheckBox>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_main);
		String[] checkboxText = new String[]{"您是学生吗？","是否喜欢android？",
				"你喜欢的原因旅游么？","打算出国吗？"};
		//动态加载布局
		LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(
				R.layout.activity_main, null);
		//给指定的checkbox赋值
		for (int i=0;i<checkboxText.length;i++){
			//先获得checkbox.xml的对象
			CheckBox checkBox = (android.widget.CheckBox) getLayoutInflater().inflate(R.layout.checkbox, null);
			checkBoxs.add(checkBox);
			checkBoxs.get(i).setText(checkboxText[i]);
			//	
			linearLayout.addView(checkBox, i);
		}
		setContentView(linearLayout);
		Button button = (Button) findViewById(R.id.button);
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String string = "";
				for (CheckBox checkBox:checkBoxs) {
					if (checkBox.isChecked()) {
						string += checkBox.getText()+"\n";
					}
				}
				if("".equals(string)) {
					string = "您没有选中选项";
				}
				//使用一个提示框来提示用户的信息
				new AlertDialog.Builder(MainActivity.this).setMessage(string).setPositiveButton("关闭", 
						null).show();
			}
		});
	}
}
