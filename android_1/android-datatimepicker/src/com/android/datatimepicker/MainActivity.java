package com.android.datatimepicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import android.app.Activity;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.TimePicker.OnTimeChangedListener;

public class MainActivity extends Activity implements OnDateChangedListener, OnTimeChangedListener {
	private TextView textview;
	private DatePicker datepicker;
	private TimePicker timepicker;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		datepicker = (DatePicker) findViewById(R.id.datePicker);
		timepicker = (TimePicker) findViewById(R.id.timePicker);
		textview = (TextView) findViewById(R.id.textview);
		datepicker.init(2001, 1, 25, this);// 初始化日期
		timepicker.setIs24HourView(true);// 显示时间是否是按照24小时制
		timepicker.setOnTimeChangedListener(this);// 注册事件
	}

	@Override
	public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
		// TODO Auto-generated method stub
		Toast.makeText(MainActivity.this, "hourofDay:" + hourOfDay + "	minute:" + minute, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		// TODO Auto-generated method stub
		Calendar calendar = Calendar.getInstance();
		calendar.set(datepicker.getYear(), datepicker.getMonth(), datepicker.getDayOfMonth(), timepicker.getCurrentHour(),
				timepicker.getCurrentMinute());
		String format = "yyyy-MM-dd HH:mm";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);

		textview.setText(simpleDateFormat.format(calendar.getTime()));
	}
}
