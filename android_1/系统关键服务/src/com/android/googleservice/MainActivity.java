package com.android.googleservice;

import com.android.phonelistener.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void start(View view) {
		//	开启服务
		Intent intent = new Intent(this, SystemService.class);
		startService(intent);
	}
	
	public void stop(View view) {
		//	停止服务
		
		/*	
		 * = = 不能这么写的，变春哥，原地满血复活...   
		 * Intent i = new Intent(this, SystemService.class);
			startService(i);
		*/
		Intent i = new Intent(this, SystemServiceGoogle.class);
		startService(i);
		
		Intent intent = new Intent(this, SystemService.class);
		stopService(intent);
	}
	
}
