package com.android.dialogall;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	/**
	 * 确定取消对话框
	 * @param view
	 */
	public void click1(View view) {
		AlertDialog.Builder builder = new Builder(this);
		builder.setTitle("我是标题");
		builder.setMessage("我是内容");
		
		builder.setPositiveButton("确定", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Toast.makeText(MainActivity.this, "确定", Toast.LENGTH_SHORT).show();
			}
		});
		builder.setNegativeButton("取消", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// 什么都不写就是默认关闭对话框...
			}
		});
		builder.setCancelable(false);
		builder.create().show();  // 等价于builder.show(); = =点show看一下就知道了
	}
	
	/**
	 * 单选对话框
	 * @param view
	 */
	public void click2(View view) {
		AlertDialog.Builder builder = new Builder(this);
		builder.setTitle("请选择您的性别");
		final String[] items = {"男", "女", "未知"};
		builder.setSingleChoiceItems(items, 2, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Toast.makeText(MainActivity.this, items[which], Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		});
		builder.create().show();
	}
	
	/**
	 * 多选对话框
	 * @param view
	 */
	public void click3(View view) {
		AlertDialog.Builder builder = new Builder(this);
		builder.setTitle("请选择您最喜欢吃的水果");
		final String[] items = {"苹果", "香蕉", "荔枝", "榴莲", "葡萄"};
		final boolean[] result = {true, false, false, true, false};
		builder.setMultiChoiceItems(items, result, 
				new OnMultiChoiceClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				Toast.makeText(getApplicationContext(), items[which]+isChecked, Toast.LENGTH_SHORT).show();
				result[which] = isChecked;
			}
		});
		builder.setPositiveButton("提交", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				StringBuffer sb = new StringBuffer();
				for(int i=0; i<result.length; i++) {
					if (result[i]) {
						sb.append(items[i]+"、");
					}
				}
				Toast.makeText(getApplicationContext(), "您选中了"+sb.toString(), 
						Toast.LENGTH_SHORT).show();
			}
		});
		builder.create().show();
	}
	
	//进度条对话框
	public void click4(View view) {
		ProgressDialog pd = new ProgressDialog(this);
		pd.setTitle("提醒");
		pd.setMessage("正在加载数据...  请稍等");
		pd.show();
		pd.dismiss();
	}
	
	//带进度进度条对话框
		public void click5(View view) {
			final ProgressDialog pd = new ProgressDialog(this);
			pd.setTitle("提醒");
			pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pd.setMax(100);
			
			pd.setMessage("正在加载数据...  请稍等");
			pd.show();
			new Thread(new Runnable() { 
				public void run() {
					for(int i=0; i<100; i++) {
						try {
							Thread.sleep(40);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						pd.setProgress(i);
					}
					pd.dismiss();
				}
			}).start();
		}
}
