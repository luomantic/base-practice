package com.example.xmpull.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

import com.example.xmpull.Person;

import android.os.Environment;
import android.test.AndroidTestCase;
import android.util.Log;
import android.util.Xml;

public class TestCase extends AndroidTestCase {

	public void Test(){
//		writeXmlToLocal();
		
		List<Person> personList = parserXmlFromLocal();
		
		// foreach快捷键	--->		fore
		for (Person person : personList) {
			Log.i("TestCase", person.toString());
		}
	}

	/**
	 * 写xml文件到本地
	 */
	private void writeXmlToLocal() {
		List<Person> personList = getPersonList();
		
		//	获得序列化对象
		XmlSerializer xmlSerializer = Xml.newSerializer();
	
		try {
			File path = new File(Environment.getExternalStorageDirectory(), "persons.xml");
			FileOutputStream fileOutputStream = new FileOutputStream(path);
			//	指定序列化对象输出的位置和编码
			xmlSerializer.setOutput(fileOutputStream, "utf-8");
			
			xmlSerializer.startDocument("utf-8", true);//	开始<?xml version='1.0' encoding='utf-8' standalone='yes' ?>
			
			xmlSerializer.startTag(null, "persons");// <persons>
			
			for(Person person : personList) {
				 //开始写人	##  加空双引号不如 1.String.valueof(int)  2.Integer.valueof(String) 3.new StringBuild/new StringBuffer然后append
				xmlSerializer.startTag(null, "person");//	<person>
				xmlSerializer.attribute(null, "id", String.valueOf(person.getId())); // attribute(属性)
				
				// 	写名字
				xmlSerializer.startTag(null, "name");
				xmlSerializer.text(person.getName());
				xmlSerializer.endTag(null, "name");
				
				//	写年龄
				xmlSerializer.startTag(null, "age");
				xmlSerializer.text(String.valueOf(person.getAge()));
				xmlSerializer.endTag(null, "age");
				
				xmlSerializer.endTag(null, "person");//    </person>
			}
			
			xmlSerializer.startTag(null, "persons");// </persons>
			
			xmlSerializer.endDocument();//	结束
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private List<Person> getPersonList() {
		List<Person> personList = new ArrayList<Person>();
		
		for (int i = 0; i < 30; i++) {
			personList.add(new Person(i, "wang" + i, 18 + i));
		}
		
		return personList;
	}
	
	private List<Person> parserXmlFromLocal()	{
		try {
			File path = new File(Environment.getExternalStorageDirectory(), "persons.xml");
			FileInputStream fis = new FileInputStream(path);
			
			//	获得pull解析器对象
			XmlPullParser parser = Xml.newPullParser();
			//	指定解析的文件和编码格式
			parser.setInput(fis, "utf-8");
			
			int eventType = parser.getEventType();	//	获得事件类型
			
			List<Person> personList = null;
			Person person = null;
			String id;
			while (eventType != XmlPullParser.END_DOCUMENT) {
				String tagName = parser.getName();  //	  获得当前节点的名称
				
				switch (eventType) {
				case XmlPullParser.START_TAG:	//	当前等于开始节点<person>
					if ("persons".equals(tagName)) {	//	<persons>
						personList = new ArrayList<Person>();
					}	else if ("person".equals(tagName)) {
						person = new Person();
						id = parser.getAttributeValue(null, "id");
						person.setId(Integer.valueOf(id));
					}	else if ("name".equals(tagName)) {	 //	<name>	
						person.setName(parser.nextText());
					} else if ("age".equals(tagName)) {	//	<age>
						person.setAge(Integer.parseInt(parser.nextText()));
					}
					break;
				case XmlPullParser.END_TAG:
					if ("person".equals(tagName)) {
						//	需要把设置好值得person对象添加到集合中
						personList.add(person);
					}
					break;
				default:
					break;
				}
				
				eventType = parser.next();	//	获得下一个事件类型
			}
			return personList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
