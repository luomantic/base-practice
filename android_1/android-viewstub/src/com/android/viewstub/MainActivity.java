package com.android.viewstub;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;

public class MainActivity extends Activity {

	private Button button1,button2;
	private ViewStub viewstub;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		button1 = (Button) findViewById(R.id.button1);
		button2 = (Button) findViewById(R.id.button2);
		viewstub = (ViewStub) findViewById(R.id.stub);
		button1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//表示填充动态布局
				View view = viewstub.inflate();
				LinearLayout layout = (LinearLayout) view;
				RatingBar bar = (RatingBar) layout.findViewById(R.id.ratingBar);
				bar.setNumStars(3);
			}
		});
		
		button2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//隐藏动态加载的布局
				viewstub.setVisibility(View.GONE);
			}
		});
	}
}
