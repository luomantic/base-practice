package com.android.qqlogin;

import java.util.Map;

import com.android.qqlogin.utils.UtilsOfSharedPreferences;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{

	private static final String TAG = "MainActivity";
	private EditText etNumber;
	private EditText etPassword;
	private CheckBox cbRememberPWD;
	private Button button;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// /data/data/包名/files
//		this.getFilesDir();
		
		etNumber = (EditText) findViewById(R.id.et_number);
		etPassword = (EditText) findViewById(R.id.et_password);
		cbRememberPWD = (CheckBox) findViewById(R.id.cb_remember_pwd);
		button = (Button) findViewById(R.id.login);
		
		button.setOnClickListener(this);
		
		// 回显数据
		Map<String, String> userInfoMap = UtilsOfSharedPreferences.getUserInfo(this);
		if (userInfoMap != null) {
			etNumber.setText(userInfoMap.get("number"));
			etPassword.setText(userInfoMap.get("password"));
		}
	}

	@Override
	public void onClick(View v) {
		//执行登录的操作
		
		//1.取出号码和密码
		String number = etNumber.getText().toString();
		String password = etPassword.getText().toString();
		
		if (TextUtils.isEmpty(number) || TextUtils.isEmpty(password)) {
			Toast.makeText(MainActivity.this, "请正确输入", Toast.LENGTH_SHORT).show();
			return;
		}
		
		//2.判断记住密码是否被选中，如果被选中，存起来
		if (cbRememberPWD.isChecked()) {
			//当前需要记住密码
			Log.i(TAG, "记住密码："+ number +  ", " + password);
			
			boolean isSuccess = UtilsOfSharedPreferences.saveUserInfo(this, number, password);
			if (isSuccess) {
				Toast.makeText(this, "保存成功", Toast.LENGTH_SHORT).show();
			}else {
				Toast.makeText(this, "保存失败", Toast.LENGTH_SHORT).show();
			}
		}
		
		//3.登录成功
		Toast.makeText(this, "登录成功", Toast.LENGTH_SHORT).show();
	}
}
