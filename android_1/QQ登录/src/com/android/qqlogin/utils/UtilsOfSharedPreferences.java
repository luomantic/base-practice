package com.android.qqlogin.utils;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

public class UtilsOfSharedPreferences {

	public UtilsOfSharedPreferences() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 保存用户信息
	 * @param number
	 * @param password
	 * @return true 成功
	 */
	public static boolean saveUserInfo(Context context,String number, String password){
		
		try {
			//	文件存储路径：/data/data/包名/shared_prefs/文件名
			SharedPreferences sp = context.getSharedPreferences("lchxxx", Context.MODE_PRIVATE);
			
			//	获得一个编辑对象
			Editor edit = sp.edit();
			
			//	存数据
			edit.putString("number", number);
			edit.putString("password", password);
			
			//	提交，数据真正存储起来了
			edit.commit();
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	/**
	 * 获得用户信息
	 * @param context
	 * @return
	 */
	public static Map<String, String> getUserInfo(Context context){
		
		SharedPreferences sp = context.getSharedPreferences("lchxxx", Context.MODE_PRIVATE);
		
		String number = sp.getString("number", null);
		String password = sp.getString("passowrd", null);
		
		if (!TextUtils.isEmpty(number) && !TextUtils.isEmpty(password)) {
			Map<String, String> userinfoMap = new HashMap<String, String>();
			userinfoMap.put("number", number);
			userinfoMap.put("password", password);
			return userinfoMap;
		}
		
		return null;
	}
}
