package com.android.qqlogin.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.text.TextUtils;

public class Utils {

	public Utils() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 保存用户信息
	 * @param number
	 * @param password
	 * @return true 成功
	 */
	public static boolean saveUserInfo(Context context,String number, String password){
		try {
//			String path = "/data/data/com.android.qqlogin/userinfo.txt";
//			File filesDir = context.getFilesDir();
			File filesDir = context.getCacheDir();
			
			File f = new File(filesDir, "userinfo.txt");
			
			FileOutputStream fos = new FileOutputStream(f);
			
			// 2754757520##123123
			String data = number + "##" +password;
			
			fos.write(data.getBytes());
			
			fos.flush();
			
			fos.close();
			
			return true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static Map<String, String> getUserInfo(Context context){
		try {
//			String path = "/data/data/com.android.qqlogin/userinfo.txt";
			
//			File filesDir = context.getFilesDir();
			File filesDir = context.getCacheDir();
			
			File f = new File(filesDir, "userinfo.txt");
			
			FileInputStream fis = new FileInputStream(f);//字节流
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(fis));//字符流
			
			// 2754757520##123123
			String text = reader.readLine();
			
			if (!TextUtils.isEmpty(text)) {
				String[] split = text.split("##");
				
				Map<String, String> userinfoMap = new HashMap<String, String>();
				userinfoMap.put("number", split[0]);
				userinfoMap.put("password", split[1]);
				
				reader.close();
			
				return userinfoMap;
			}
			
			fis.close();		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
