package com.android.qqlogin.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

public class UtilsOfSDcard {

	public UtilsOfSDcard() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 保存用户信息到sd卡
	 * @param number
	 * @param password
	 * @return true 成功
	 */
	public static boolean saveUserInfo(Context context,String number, String password){
		File sdCardFile = Environment.getExternalStorageDirectory();
		
		try {
			//	判断当前的手机是否有sd卡
			String state = Environment.getExternalStorageState();
			
			if (!Environment.MEDIA_MOUNTED.equals(state)) {
				//已经挂载了sd卡
				return false;
			}
			
			File file = new File(sdCardFile+"/userinfo.txt");
			
			FileOutputStream fos = new FileOutputStream(file);
			
			String data = number + "##" + password;
			
			fos.write(data.getBytes());
			
			fos.flush();
			
			fos.close();
			
			return true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * 获取sd卡中的用户信息
	 * @param context
	 * @return
	 */
	public static Map<String, String> getUserInfo(Context context){
		File sdCardFile = Environment.getExternalStorageDirectory();
		
		try {
			//	判断当前的手机是否有sd卡
			String state = Environment.getExternalStorageState();
			
			if (!Environment.MEDIA_MOUNTED.equals(state)) {
				//已经挂载了sd卡
				return null;
			}
			
			File file = new File(sdCardFile+"/userinfo.txt");
			
			BufferedReader bReader = new BufferedReader(new InputStreamReader(
					new FileInputStream(file)));
			
			String text = bReader.readLine();
			
			bReader.close();
			
			if (!TextUtils.isEmpty(text)) {
				Map<String, String> userinfoMap = new HashMap<String, String>();
				String[] split = text.split("##");
				userinfoMap.put("number", split[0]);
				userinfoMap.put("password", split[1]);
				return userinfoMap;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
