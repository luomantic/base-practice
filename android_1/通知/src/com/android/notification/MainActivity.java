package com.android.notification;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	/**
	 * 新版本的notification
	 * @param view
	 */
	public void click(View view) {
		Notification notification = new Notification.Builder(this)
				.setContentTitle("我是标题")
				.setContentText("我是内容")
				.setSmallIcon(R.drawable.notification)
				.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
				.build();
		NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		notification.defaults = Notification.DEFAULT_SOUND;
		notification.defaults |= Notification.DEFAULT_VIBRATE; //振动器需要权限
		notification.tickerText = "来消息啦...";
		nm.notify(0, notification);
	}
	
}
 