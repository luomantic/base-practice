package com.android.htmldemo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class buttonClickListener implements OnClickListener {

	private static final String TAG = "buttonClickListener";
	protected static final int SUCCESS = 0;
	protected static final int ERROR = 1;
	private EditText etUrl;
	private Handler handler;
	
	public buttonClickListener(EditText etUrl, Handler handler) {
		super();
		this.etUrl = etUrl;
		this.handler = handler;
	}

	@Override
	public void onClick(View v) {
		final String url = etUrl.getText().toString();
		
		new Thread(new Runnable() {
			public void run() {
				// 请求网络
				String html = getHtmlFromInternet(url);
				
				if (!TextUtils.isEmpty(html)) {
					// 跟新textview的显示
					Message msg = new Message();
					msg.what = SUCCESS;
					msg.obj = html;
					handler.sendMessage(msg);
				} else {
					Message msg = new Message();
					msg.what = ERROR;
					handler.sendMessage(msg);
				}
			}
		}).start();
		
	}

	/**
	 * 根据给定的url访问网络，抓取html代码
	 * @param url
	 * @return
	 */
	protected String getHtmlFromInternet(String url) {
		
		try {
			URL mURL = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) mURL.openConnection();
			
			connection.setRequestMethod("GET");
			connection.setConnectTimeout(8*1000);
			connection.setReadTimeout(5*1000);
			
//			connection.connect();
			
			int responseCode = connection.getResponseCode();
			
			if (responseCode == 200) {
				
				InputStream inputStream = connection.getInputStream();
				String html = getStringFromInputStream(inputStream);
				
				return html;
			}else {
				Log.i(TAG, "访问失败: " + responseCode);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * 根据流返回一个字符串信息
	 * @param inputStream
	 * @return
	 * @throws IOException 
	 */
	private String getStringFromInputStream(InputStream inputStream) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = -1;
		
		while ((len = inputStream.read(buffer)) != -1) {
			baos.write(buffer, 0, len);
		}
		inputStream.close();
		
		//	解决乱码问题
		String html = baos.toString();	// 把流中的数据转换成字符串，采用的是utf-8
		
		String charset = "utf-8";
		if (html.contains("gbk") || html.contains("gb2312") || 
				html.contains("GBK") || html.contains("GB2312")) {
			charset = "gbk";
		}		
		
		html = new String(baos.toByteArray(), charset);	// 对原有的字节数组进行使用处理后的编码名称进行编码
		
		baos.close();
		return html;
	}
}
