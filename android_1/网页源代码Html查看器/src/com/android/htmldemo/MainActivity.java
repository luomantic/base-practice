package com.android.htmldemo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	protected static final int SUCCESS = 0;
	protected static final int ERROR = 1;
	private EditText etUrl;
	private Button button;
	private TextView tvHtml;
	
	private Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			
			switch (msg.what) {
			case SUCCESS:
				tvHtml.setText(String.valueOf(msg.obj));
				break;
			case ERROR:
				Toast.makeText(MainActivity.this, "����ʧ��", Toast.LENGTH_LONG).show();
				break;
			default:
				break;
			}
		}
		
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		etUrl = (EditText) findViewById(R.id.et_url);
		button = (Button) findViewById(R.id.btn_getHtml);
		tvHtml = (TextView) findViewById(R.id.tv_html);
		
		button.setOnClickListener(new buttonClickListener(etUrl, handler));
	}
}
