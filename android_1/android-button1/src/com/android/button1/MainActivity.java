package com.android.button1;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener, OnTouchListener,
					OnFocusChangeListener{
	private int value = 1;//用于改变按钮的大小
	private Button commonbutton;
	private Button imagebutton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		commonbutton = (Button) findViewById(R.id.commonbutton);
		imagebutton = (Button) findViewById(R.id.imagebutton);
		commonbutton.setOnClickListener(this);
		imagebutton.setOnClickListener(this);
		imagebutton.setOnTouchListener(this);
		imagebutton.setOnFocusChangeListener(this);
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		if(hasFocus) {
			imagebutton.setBackgroundResource(R.drawable.button2);
		}else {
			imagebutton.setBackgroundResource(R.drawable.button1);
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(event.getAction()==MotionEvent.ACTION_UP){
			v.setBackgroundResource(R.drawable.button1);
		}else if(event.getAction()==MotionEvent.ACTION_DOWN){
			v.setBackgroundResource(R.drawable.button3);
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Button button = (Button) v;
		if(value==1&&button.getWidth()==getWindowManager().getDefaultDisplay()
				.getWidth()){
			value = -1;
		}else if(value==-1 && button.getWidth() < 100){
			button.setWidth(button.getWidth()+(int)(button.getWidth()*0.1*value));
			button.setHeight(button.getHeight()+(int)(button.getHeight()*0.1*value));
		}
	}
	
	
}
