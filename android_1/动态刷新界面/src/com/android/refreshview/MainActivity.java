package com.android.refreshview;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

	private LinearLayout llGroup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		llGroup = (LinearLayout) findViewById(R.id.ll);
	}
	
	public void	onClick(View view) {
		
		//	添加一个TextView向llGroup
		
		//	定义一个TextView
		TextView tv = new TextView(MainActivity.this);
		tv.setText("张三　　　				女　　　				24");
//		tv.setTextSize();
		
		llGroup.addView(tv);
		
	}
}
