package com.java.multidownloaddemo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;

public class MultiThreadDownload {
	// 线程的数量
	private static int threadCount = 3;

	// 每个线程下载的大小
	private static int blockSize;
	
	/*
	 * 正在运行的线程数量
	 */
	private static int runningThraedCount;

	/*
	 * 思路
	 */
	public static void main(String[] args) throws Exception {
		// 服务器文件的路径
		String path = "http://192.168.56.1:8080/ff.exe";
		URL url = new URL(path);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setConnectTimeout(5 * 1000);

		int code = connection.getResponseCode();
		if (code == 200) { // 一般市面上框架里面都是 code/100 == 2
			long size = connection.getContentLength(); // 得到服务器返回的文件大小
			System.out.println("服务器文件的大小：" + size);
			blockSize = (int) (size / threadCount);

			// 1.在本地创建一个跟服务器一模一样大小的空白文件 RandomAccessFile
			File file = new File("temp.exe");
			RandomAccessFile raf = new RandomAccessFile(file, "rw");
			raf.setLength(size);

			// 2.开启若干个子线程分别去下载对应的资源。
			runningThraedCount = threadCount;
			for (int i = 1; i <= threadCount; i++) {
				System.out.println("开启线程：" + i);
				long startIndex = (i - 1) * blockSize + 0;
				long endIndex = i * blockSize - 1;
				if (i == threadCount) {
					// 最后一个线程
					endIndex = size - 1;
				}
				System.out.println("开启线程：" + i + "下载的位置：" + startIndex + "~" + endIndex);
				new DownloadThread(path, i, startIndex, endIndex).start();
			}
			raf.close();
		}
		connection.disconnect();
	}

	/*
	 * 实现
	 */
	private static class DownloadThread extends Thread {
		private int threadId;
		private long startIndex;
		private long endIndex;
		private String path;

		public DownloadThread(String path, int threadId, long startIndex, long endIndex) {
			this.path = path;
			this.threadId = threadId;
			this.startIndex = startIndex;
			this.endIndex = endIndex;
		}

		@Override
		public void run() {
			try {
				// 当前线程下载的总大小
				int total = 0;
				File positionFile = new File(threadId + ".txt");
				URL url = new URL(path);
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				connection.setRequestMethod("GET");
				// 接着从上一次的位置继续下载数据
				if (positionFile.exists() && positionFile.length() > 0) {
					FileInputStream fis = new FileInputStream(positionFile);
					BufferedReader br = new BufferedReader(new InputStreamReader(fis));
					// 获取当前线程上次下载的总大小是多少
					String lasttotalstr = br.readLine();
					int lastTotal = Integer.valueOf(lasttotalstr);
					System.out.println("上次线程" + threadId + "下载的总大小");
					startIndex += lastTotal;
					total+=lastTotal;//	加上上次下载的总大小
					fis.close();
				}
				connection.setRequestProperty("Range", "bytes=" + startIndex + "-" + endIndex);
				connection.setConnectTimeout(5 * 1000);

				int code = connection.getResponseCode();
				System.out.println("code = " + code); // code == 206
				InputStream inputStream = connection.getInputStream();
				File file = new File("temp.exe");
				RandomAccessFile raf = new RandomAccessFile(file, "rw");
				// 指定文件开始写的位置
				raf.seek(startIndex);
				System.out.println("第" + threadId + "个线程：写文件的开始位置: " + String.valueOf(startIndex));
				int len = 0;
				byte[] buffer = new byte[1024*1024];		// bt毁硬盘，缓冲区设大一点  =。=
				
				while ((len = inputStream.read(buffer)) != -1) {
					// FileOutputStream fos = new
					// FileOutputStream(positionFile);
					RandomAccessFile rf = new RandomAccessFile(positionFile, "rwd");
					raf.write(buffer, 0, len);
					total += len;
					rf.write(String.valueOf(total).getBytes());
					rf.close();
				}
				// while ((len = inputStream.read(buffer)) != -1) {
				// raf.write(buffer, 0, len);
				// }
				raf.close();
				inputStream.close();
				System.out.println("线程" + threadId + "下载完毕了");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				// 只有所有线程都下载完成之后，才可以删除记录文件...  （避免文件较大的时候，线程完成的时间差之内出异常，记录没了，需要重新下载）
				synchronized (MultiThreadDownload.class) {
					System.out.println("线程" + threadId + "'下载完毕了,删除记录文件");
					runningThraedCount--;
					if (runningThraedCount<1) {
						System.out.println("所有的线程都工作完毕了...");
						for(int i = 1; i <= threadCount; i++) {
							File file = new File (i +".txt");
							System.out.println(file.delete());
						}
					}
				}
			}
		}
	}

}
