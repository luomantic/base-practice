package com.android.netphoto;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class buttonClickListener implements OnClickListener {
	private static final String TAG = "buttonClickListener";
	private EditText etUrl;
	private Handler handler;
	private static final int SUCCESS = 0;
	private static final int ERROR = 1;
	
	public buttonClickListener(EditText etUrl, Handler handler) {
		this.etUrl = etUrl;
		this.handler = handler;
	}
	
	@Override
	public void onClick(View v) {
		final String url = etUrl.getText().toString();
		
		new Thread(new Runnable() {
			public void run() {
				Bitmap bitmap = getImageFromNet(url);
//				ivIcon.setImageBitmap(bitmap);
				if (bitmap != null) {
					Message msg = new Message();
					msg.what = SUCCESS;
					msg.obj = bitmap;
					handler.sendMessage(msg);
				}else {
					Message msg = new Message();
					msg.what = ERROR;
					handler.sendMessage(msg);
				}
			}
		}).start();
				
	};

	/**
	 * 根据url连接，去网络抓取图片返回
	 * @param url	
	 * @return url对应的图片
	 */
	public Bitmap getImageFromNet(String url) {
		HttpURLConnection connection = null;
		
		try {
			URL mUrl = new URL(url);	// 创建一个url对象
			
			//	得到http的连接对象
			connection = (HttpURLConnection) mUrl.openConnection();
			
			connection.setRequestMethod("GET"); // 设置请求方法为Get
			connection.setConnectTimeout(8*1000);  // 设置连接服务器的超时时间，如果超过8秒钟没有连接，抛异常
			connection.setReadTimeout(5*1000); // 设置读取数据时的超时时间，如果读取超过5秒，抛出异常
		
			connection.connect(); // 开始连接
			
			int responseCode = connection.getResponseCode();
			if (responseCode == 200) {
				//	访问成功
				InputStream inputStream = connection.getInputStream(); // 获得服务器返回流
				Bitmap bitmap = BitmapFactory.decodeStream(inputStream); // 根据流数据，创建一个bitmap位图对象
				
				return bitmap;
			}else {
				Log.i(TAG, "访问失败：respondCode = " + responseCode);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		
		return null;
	}
	
}
