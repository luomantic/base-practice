package com.android.netphoto;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private ImageView ivIcon;
	private EditText etUrl;
	private Button button;
	private static final int SUCCESS = 0;
	private static final int ERROR = 1;

	private Handler hanlder = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (msg.what == SUCCESS) {
				ivIcon.setImageBitmap((Bitmap)msg.obj);
			}else if (msg.what == ERROR) {
				Toast.makeText(MainActivity.this, "��ȡ����ͼƬʧ��", Toast.LENGTH_LONG).show();
			}
		}
		
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ivIcon = (ImageView) findViewById(R.id.iv_icon);
		etUrl = (EditText) findViewById(R.id.et_url);
		
		button = (Button) findViewById(R.id.btn_submit);
		button.setOnClickListener(new buttonClickListener(etUrl, hanlder));
		
	}
	
}


