package com.android.fish;

import com.android.alipay.ISafePay;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	private ISafePay iSafePay;
	private Myconn conn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Intent intent = new Intent();
		intent.setAction("com.android.alipay");
		startService(intent);
		//保证服务长期后台运行
	}
	
	public void click(View view) {
		Intent intent = new Intent();
		intent.setAction("com.android.alipay");
		conn = new Myconn();
		bindService(intent, conn, BIND_AUTO_CREATE); //	 异步的操作
		//绑定服务，调用服务的方法
	}
	
	private class Myconn implements ServiceConnection {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// TODO Auto-generated method stub
			iSafePay = ISafePay.Stub.asInterface(service);
			try {
				boolean result = iSafePay.callPay(System.currentTimeMillis(), "123", 3.52f);
				if (result) {
					Toast.makeText(getApplicationContext(), "支付成功，获得炸弹一枚", Toast.LENGTH_LONG).show();
				}else {
					Toast.makeText(getApplicationContext(), "支付失败，请重新支付", Toast.LENGTH_LONG).show();
				}
				unbindService(conn);
				conn = null;
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
			
		}
		
	}
}
