package com.example.android_alertdialog_progress;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Custom	{
	private Context context;
	private Dialog dialog;
	
	public Custom(Context context) {
		this.context = context;
		dialog = new Dialog(context);
	}
	
	public void show(){
		View view = LayoutInflater.from(context).inflate(R.layout.custom_dialog, null);
//		setContentView(R.layout.custom_dialog); //加载自定义对话框
		dialog.setContentView(view);
		dialog.setTitle("自定义对话框");
		TextView textView = (TextView) view.findViewById(R.id.text);
		textView.setText("你好，自定义对话框");
		textView.setTextColor(Color.BLACK);
		ImageView imageView = (ImageView) view.findViewById(R.id.image);
		imageView.setImageResource(R.drawable.ic_launcher);		
		dialog.show();
	}
}
