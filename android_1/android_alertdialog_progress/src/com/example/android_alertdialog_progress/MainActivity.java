package com.example.android_alertdialog_progress;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	private Button button;
	private Button button2;
	private Button button3;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		button = (Button) findViewById(R.id.button1);
		button2 = (Button) findViewById(R.id.button2);
		button3 = (Button) findViewById(R.id.button3);
		
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				ProgressDialog.show(MainActivity.this, "提示", "正在加载请稍候");
				ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
				progressDialog.setTitle("提示");
				progressDialog.setMessage("正在加载，请稍候......");
				progressDialog.show();//显示对话框
//				progressDialog.dismiss();//隐藏对话框，结合线程或者消息使用...
			}
		});
		
		button2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				new ProgressDialog(MainActivity.this, ProgressDialog.STYLE_HORIZONTAL);
				ProgressDialog dialog = new ProgressDialog(MainActivity.this);
				dialog.setTitle("下载提示");
				dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				dialog.setProgress(50);//如果加上线程操作
				dialog.setCancelable(false);
				dialog.show();
			}
		});
		
		button3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Custom dialog = new Custom(MainActivity.this);
				dialog.show();
			}
		});
	}
}
