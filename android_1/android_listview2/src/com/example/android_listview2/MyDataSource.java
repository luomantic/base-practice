package com.example.android_listview2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyDataSource {

	public MyDataSource() {
		// TODO Auto-generated constructor stub
	}

	public static List<Map<String, String>> getMaps(){
		List<Map<String, String>>  listmaps = new ArrayList<Map<String,String>>();
		Map<String, String> map1 = new HashMap<String, String>();
		map1.put("pname", "西瓜");
		map1.put("price", "$2.30");
		map1.put("address", "海南");
		
		Map<String, String> map2 = new HashMap<String, String>();
		map2.put("pname", "香蕉");
		map2.put("price", "$4.30");
		map2.put("address", "广西");
		
		Map<String, String> map3 = new HashMap<String, String>();
		map3.put("pname", "南瓜");
		map3.put("price", "$1.30");
		map3.put("address", "河北");
		
		Map<String, String> map4 = new HashMap<String, String>();
		map4.put("pname", "橘子");
		map4.put("price", "$3.50");
		map4.put("address", "湖南");
		
		Map<String, String> map5 = new HashMap<String, String>();
		map5.put("pname", "西瓜");
		map5.put("price", "$2.30");
		map5.put("address", "海南");
		
		Map<String, String> map6 = new HashMap<String, String>();
		map6.put("pname", "西瓜");
		map6.put("price", "$2.30");
		map6.put("address", "海南");
		
		Map<String, String> map7 = new HashMap<String, String>();
		map7.put("pname", "西瓜");
		map7.put("price", "$2.30");
		map7.put("address", "海南");
		
		Map<String, String> map8 = new HashMap<String, String>();
		map8.put("pname", "西瓜");
		map8.put("price", "$2.30");
		map8.put("address", "海南");
		
		Map<String, String> map9 = new HashMap<String, String>();
		map9.put("pname", "西瓜");
		map9.put("price", "$2.30");
		map9.put("address", "海南");
		
		Map<String, String> map10 = new HashMap<String, String>();
		map10.put("pname", "西瓜");
		map10.put("price", "$2.30");
		map10.put("address", "海南");
		
		Map<String, String> map21 = new HashMap<String, String>();
		map21.put("pname", "香蕉");
		map21.put("price", "$4.30");
		map21.put("address", "广西");
		
		Map<String, String> map22 = new HashMap<String, String>();
		map22.put("pname", "香蕉");
		map22.put("price", "$4.30");
		map22.put("address", "广西");
		
		Map<String, String> map23 = new HashMap<String, String>();
		map23.put("pname", "香蕉");
		map23.put("price", "$4.30");
		map23.put("address", "广西");
		
		Map<String, String> map24 = new HashMap<String, String>();
		map24.put("pname", "香蕉");
		map24.put("price", "$4.30");
		map24.put("address", "广西");
		
		Map<String, String> map25 = new HashMap<String, String>();
		map25.put("pname", "香蕉");
		map25.put("price", "$4.30");
		map25.put("address", "广西");
		
		Map<String, String> map26 = new HashMap<String, String>();
		map26.put("pname", "香蕉");
		map26.put("price", "$4.30");
		map26.put("address", "广西");
		
		Map<String, String> map27 = new HashMap<String, String>();
		map27.put("pname", "香蕉");
		map27.put("price", "$4.30");
		map27.put("address", "广西");
		
		Map<String, String> map28 = new HashMap<String, String>();
		map28.put("pname", "香蕉");
		map28.put("price", "$4.30");
		map28.put("address", "广西");
		
		Map<String, String> map29 = new HashMap<String, String>();
		map29.put("pname", "香蕉");
		map29.put("price", "$4.30");
		map29.put("address", "广西");
		
		Map<String, String> map30 = new HashMap<String, String>();
		map30.put("pname", "香蕉");
		map30.put("price", "$4.30");
		map30.put("address", "广西");
		
		listmaps.add(map1);
		listmaps.add(map2);
		listmaps.add(map3);
		listmaps.add(map4);
		listmaps.add(map5);
		listmaps.add(map6);
		listmaps.add(map7);
		listmaps.add(map8);
		listmaps.add(map9);
		listmaps.add(map10);
		listmaps.add(map21);
		listmaps.add(map22);
		listmaps.add(map23);
		listmaps.add(map24);
		listmaps.add(map25);
		listmaps.add(map26);
		listmaps.add(map27);
		listmaps.add(map28);
		listmaps.add(map29);
		listmaps.add(map30);
		
		return listmaps;
	}
}
