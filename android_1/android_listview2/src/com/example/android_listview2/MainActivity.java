package com.example.android_listview2;

import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class MainActivity extends Activity {
	private ListView listview;
	private SimpleAdapter adapter;
	private List<Map<String, String>> data=null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		listview = (ListView) findViewById(R.id.listview);
		data = MyDataSource.getMaps();
		adapter = new SimpleAdapter(MainActivity.this, 
				data, 
				R.layout.activity_main,
				new String[]{"pname","price","address"},
				new int[]{R.id.pname,R.id.price,R.id.address});
		listview.setAdapter(adapter);
	}
}
