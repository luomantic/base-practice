package com.example.android_alertdialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	private Button button;
	private Button button2;
	private Button button3;
	private Button button4;
	private StringBuffer sBuffer;

	// 定义3个item选项
	private final CharSequence[] items = { "北京", "上海", "广州" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		button = (Button) findViewById(R.id.button);
		button2 = (Button) findViewById(R.id.button2);
		button3 = (Button) findViewById(R.id.button3);
		button4 = (Button) findViewById(R.id.button4);
		sBuffer = new StringBuffer("您选择了: ");

		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// 先要创建一个Builder对象,Builder完成对dialog基本信息的构建
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setTitle("提示");
				builder.setMessage("你确定要删除吗？");
				builder.setIcon(R.drawable.ic_launcher);
				builder.setPositiveButton("确定", new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 完成用户的操作，比如删除数据，提交请求
						// dialog.dismiss();//让对话框消失，有木有都无所谓
					}
				});
				builder.setNegativeButton("取消", new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						// 取消用户的操作，比如停止下载
					}
				});
				builder.setNeutralButton("忽略", new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				});
				AlertDialog alertDialog = builder.create();// 创建一个dialog
				alertDialog.show();// 让对话框显示
			}
		});

		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setTitle("请选择以下城市");
				// builder.setMessage("请选择以下城市");//不能设置Message的内容
				builder.setItems(items, new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						CharSequence select_item = items[which];
						Toast.makeText(MainActivity.this, "---->" + select_item, Toast.LENGTH_SHORT).show();
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		});

		button3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setTitle("请选择以下城市");
				builder.setSingleChoiceItems(items, -1, new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						CharSequence select_item = items[which];
						Toast.makeText(MainActivity.this, select_item, Toast.LENGTH_SHORT).show();
						dialog.dismiss();
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		});

		button4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setTitle("请选择以下城市");
				// 第二个参数设置为null，表示没有选项默认被选中
				// 使用StringBuffer追加，中间用逗号分隔开
				builder.setMultiChoiceItems(R.array.city, new boolean[] { false, false, false, false},
						new android.content.DialogInterface.OnMultiChoiceClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked == true) {
							sBuffer.append(items[which] + ",");
						}
					}
				});
				builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Toast.makeText(MainActivity.this,sBuffer.subSequence(0, sBuffer.length() - 1),
								Toast.LENGTH_LONG).show();
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		});
	}

}
