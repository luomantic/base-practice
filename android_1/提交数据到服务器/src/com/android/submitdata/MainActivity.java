package com.android.submitdata;

import com.android.submitdata.utils.NetUtils;
import com.android.submitdata.utils.NetUtils2;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

	private EditText etUsername;
	private EditText etPassword;
	private Button doGetButton;
	private Button doPostButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		etUsername = (EditText) findViewById(R.id.et_username);
		etPassword = (EditText) findViewById(R.id.et_password);
		
		doGetButton = (Button) findViewById(R.id.btn_get);
		doPostButton = (Button) findViewById(R.id.btn_post);
		
		//	使用get方式向服务器提交数据
		doGetButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final String username = etUsername.getText().toString();
				final String password = etPassword.getText().toString();
				
				new Thread(new Runnable() {
					public void run() {
						final String state = NetUtils.loginOfGet(username, password);
						
						//	执行任务在主线程中
						runOnUiThread(new Runnable() {
							public void run() {
								// 就是在主线程中操作
								Toast.makeText(MainActivity.this, state, Toast.LENGTH_LONG).show();
							}
						});
					}
				}).start();
			}
		});
		
		doPostButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final String username = etUsername.getText().toString();
				final String password = etPassword.getText().toString();
				
				new Thread(new Runnable() {
					public void run() {
						//	子线程中访问网络
						final String state = NetUtils.loginOfPost(username, password);
						
						//	主线程中根据返回的数据更新ui
						runOnUiThread(new Runnable() {
							public void run() {
								Toast.makeText(MainActivity.this, state, Toast.LENGTH_LONG).show();
							}
						});
					}
				}).start();
			}
		});
	}
	
	/**
	 * 使用httpClient方式提交get请求
	 * @param view
	 */
	public void doHttpClientOfGet(View view) {
		
		new Thread(new Runnable() {
			public void run() {
				final String username = etUsername.getText().toString();
				final String password = etPassword.getText().toString();
				
				new Thread(new Runnable() {
					public void run() {
						final String state = NetUtils2.loginOfGet(username, password);
						
						//	执行任务在主线程中
						runOnUiThread(new Runnable() {
							public void run() {
								// 就是在主线程中操作
								Toast.makeText(MainActivity.this, state, Toast.LENGTH_LONG).show();
							}
						});
					}
				}).start();
			}
		}).start();
		
	}
	
	/**
	 * 使用httpClient方式提交post请求
	 * @param view
	 */
	public void doHttpClientOfPost(View view) {
		final String username = etUsername.getText().toString();
		final String password = etPassword.getText().toString();
		
		new Thread(new Runnable() {
			public void run() {
				//	子线程中访问网络
				final String state = NetUtils2.loginOfPost(username, password);
				
				//	主线程中根据返回的数据更新ui
				runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(MainActivity.this, state, Toast.LENGTH_LONG).show();
					}
				});
			}
		}).start();
	}
}
