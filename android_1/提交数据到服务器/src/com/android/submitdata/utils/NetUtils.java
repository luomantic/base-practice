package com.android.submitdata.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import android.util.Log;

public class NetUtils {
	
	private static final String TAG = "NetUtils";

	/**
	 * 使用get的方式登录
	 * @param username
	 * @param password
	 * @return	状态
	 */
	public static String loginOfGet(String username, String password) {
		HttpsURLConnection connection = null;
		try {
			//	对url进行编码，浏览器提交的时候，也是默认对汉字进行url编码
			String data = "username=" + URLEncoder.encode(username) +"&password="+ URLEncoder.encode(password);
			
			URL url = new URL("http://localhost:8080+xxxxx" + data);
			 connection = (HttpsURLConnection) url.openConnection();
			
			 connection.setRequestMethod("GET");
			 connection.setConnectTimeout(8*1000);
			 connection.setReadTimeout(5*1000);
			 
			 int responseCode = connection.getResponseCode();
			 if (responseCode == 200) {
				InputStream inputStream = connection.getInputStream();
				String state = getStringFromInputStream(inputStream);
				return state;
			}else	{
				Log.i(TAG, "访问失败： " + responseCode  );
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		return null;
	}
	
	/**
	 * 使用post的方式登录
	 * @param name
	 * @param password
	 * @return	登录的状态
	 */
	public static String loginOfPost(String username, String password) {
		HttpsURLConnection connection = null;
		try {
			URL url = new URL("http://+++++++++");
			
			connection = (HttpsURLConnection) url.openConnection();
			
			connection.setRequestMethod("POST");
			connection.setConnectTimeout(8*1000);
			connection.setReadTimeout(5*1000);
			connection.setDoOutput(true);  //  必须设置此方法，允许输出
//			connection.setRequestProperty("Content-Length", 234);	//	设置请求头消息，可以有多个
			
			//	post请求的参数
			String data = "username=" + username +"&password="+ password;
			
		//  获得一个输出流，用于向服务器写数据，默认情况下，系统不允许向服务器输出内容
			OutputStream outputStream = connection.getOutputStream();  
			outputStream.write(data.getBytes());
			outputStream.flush();
			outputStream.close();
			
			 int responseCode = connection.getResponseCode();
			 if (responseCode == 200) {
				InputStream inputStream = connection.getInputStream();
				String state = getStringFromInputStream(inputStream);
				return state;
			}else	{
				Log.i(TAG, "访问失败： " + responseCode  );
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		
		return null;
	}
	
	/**
	 * 根据流返回一个字符串信息
	 * @param inputStream
	 * @return
	 * @throws IOException 
	 */
	private static String getStringFromInputStream(InputStream inputStream) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = -1;
		
		while ((len = inputStream.read(buffer)) != -1) {
			baos.write(buffer, 0, len);
		}
		inputStream.close();
		
//		String html = baos.toString();	//	把流中的数据转换成字符串，采用的编码是：utf-8
		String html = new String(baos.toByteArray(), "GBK");
		
		baos.close();
		return html;
	}
}
