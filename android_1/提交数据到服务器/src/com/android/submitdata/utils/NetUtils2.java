package com.android.submitdata.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

public class NetUtils2 {

	private static final String TAG = "NetUtils";

	/**
	 * 使用get的方式登录
	 * 
	 * @param username
	 * @param password
	 * @return 状态
	 */
	public static String loginOfGet(String username, String password) {
		CloseableHttpClient httpclient = null;
		try {
			//	定义一个客户端
			httpclient = HttpClients.createDefault();
			
			//	定义一个get请求方法
			HttpGet httpget = new HttpGet("http://xxx");
			
			//	response 服务器响应对象，其中包含了状态信息和服务器返回的数据
			CloseableHttpResponse response = httpclient.execute(httpget);	 
			
			//	获得响应码
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				 InputStream inputStream = response.getEntity().getContent();
				 String text = getStringFromInputStream(inputStream);
				 return text;
			}else {
				Log.i(TAG, "请求失败：" + statusCode);
			}
			response.close();   // 这个是不是爱写不写  =。=
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (httpclient != null) {
				try {
					httpclient.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

	/**
	 * 使用post的方式登录
	 * 
	 * @param name
	 * @param password
	 * @return 登录的状态
	 */
	public static String loginOfPost(String username, String password) {
		CloseableHttpClient httpclient= null;
		try {
			//	定义一个客户端
			httpclient= HttpClients.createDefault();
			
			//	定义post方法
			HttpPost post = new HttpPost("http://xxxx");
			
			List<NameValuePair> parameters = new ArrayList<NameValuePair>();
			parameters.add(new BasicNameValuePair("username", username));
			parameters.add(new BasicNameValuePair("password", password));
			
			//  设置请求头消息
//			post.addHeader("Content-Length", "xxx"); 	请求头消息，爱写不写  = = 
			
			//  把post请求的参数包装了一层.
			
			//	不写编码名称时服务器数据乱码.	需要指定字符集为utf-8
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters, "utf-8");	
		
			//  设置参数、
			post.setEntity(entity);
			
			//	使用客户端执行post方法
			CloseableHttpResponse response = httpclient.execute(post);	//  开始执行post请求，会返回给我们一个closeableHttpResponse对象
			
			//	使用响应对象，获得状态码，处理内容
			int statusCode = response.getStatusLine().getStatusCode();	//  获得状态码
			if (statusCode == 200) {
				//	使用响应对象获得实体，获得输入流
				 InputStream inputStream = response.getEntity().getContent();
				 String text = getStringFromInputStream(inputStream);
				 return text;
			}else {
				Log.i(TAG, "请求失败：" + statusCode);
			}
			response.close();   // 这个是不是爱写不写  =。=
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if (httpclient != null) {
				try {
					httpclient.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

	/**
	 * 根据流返回一个字符串信息
	 * 
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	private static String getStringFromInputStream(InputStream inputStream) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = -1;

		while ((len = inputStream.read(buffer)) != -1) {
			baos.write(buffer, 0, len);
		}
		inputStream.close();

		// String html = baos.toString(); // 把流中的数据转换成字符串，采用的编码是：utf-8
		String html = new String(baos.toByteArray(), "GBK");

		baos.close();
		return html;
	}
}
