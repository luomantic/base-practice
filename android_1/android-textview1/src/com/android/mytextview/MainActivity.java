package com.android.mytextview;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class MainActivity extends Activity {

	private TextView textview1,textview2;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		textview1 = (TextView) findViewById(R.id.textview1);
		textview2 = (TextView) findViewById(R.id.textview2);
		
		// 添加一段html的标志
		String html = "<font color='red'>xxx</font><br>";
		html+="<font color='#0000ff'><big><i>xxx</i></big></font><p>";
		html+="<big><a href='http://www.baidu.com'>百度</big>";
		CharSequence charSequence = Html.fromHtml(html);
		textview1.setText(charSequence);
		// 点击的时候产生超链接
		textview1.setMovementMethod(LinkMovementMethod.getInstance());
		
		String text = "我的URL:http://www.sina.com\n";
		text+="我的email:sljsl@qq.com\n";
		text+="我的电话号码:2342325245";
		textview2.setText(text);
		textview2.setMovementMethod(LinkMovementMethod.getInstance());
		
	}
}
