package com.android.intent2;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;

import android.app.Activity;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.util.Base64;
import android.widget.TextView;

public class OtherActivity extends Activity {
	private TextView textView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.other);
		textView = (TextView) findViewById(R.id.msg);
	// ==============   获取文本	=============
	/*	 
		ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
		
		//  获取label
		String message = clipboardManager.getPrimaryClip().getDescription().getLabel().toString();
		//	获取text
		String msg = clipboardManager.getPrimaryClip().getItemAt(0).getText().toString();
		//	版本11以上不推荐使用的方法
		String message1 = clipboardManager.getText().toString();
		textView.setText(msg);	
	*/
		
	//	=============   获取对象	=============
		ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
		
	/*
	   	//  获取label
		String message = clipboardManager.getPrimaryClip().getDescription().getLabel().toString();
		//	版本11以上不推荐使用的方法
		String message1 = clipboardManager.getText().toString();	
	 */
		//	获取text
		String message = clipboardManager.getPrimaryClip().getItemAt(0).getText().toString();
		byte[] byteT = Base64.decode(message, Base64.DEFAULT);
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteT);
		try {
			ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
			MyData myData = (MyData) objectInputStream.readObject();
			textView.setText(myData.toString());
		} catch (StreamCorruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
