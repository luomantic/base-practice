package com.android.intent2;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	private Button button;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		button = (Button) findViewById(R.id.button);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			//	==============	写入文本	===============
			/*
				ClipboardManager clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				ClipData clipData = ClipData.newPlainText("newPlainTextLabel", "jack"); 
				clipboardManager.setPrimaryClip(clipData);
				//	版本11以后不推荐使用的方法
				String message = "jack";
				clipboardManager.setText(message);
				
				Intent intent = new Intent(MainActivity.this,OtherActivity.class);
				startActivity(intent);
			*/
				
			//	===============	  写入对象	=================	
				MyData myData = new MyData("jack", 21);
				//	将对象转换成字符串
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				String base64String = "";
				try {
					ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
					objectOutputStream.writeObject(myData);
					base64String = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
					objectOutputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ClipboardManager clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				
				ClipData clipData = ClipData.newPlainText("newPlainTextLabel", base64String);
				Item item = new Item(base64String);
				clipData.addItem(item);
				clipboardManager.setPrimaryClip(clipData);
				// 不推荐的方法
//				clipboardManager.setText(base64String);
				
				Intent intent = new Intent(MainActivity.this,OtherActivity.class);
				startActivity(intent);	
			}
		});
	}
}
