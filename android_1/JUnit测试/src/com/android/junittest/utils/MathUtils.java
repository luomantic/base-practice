package com.android.junittest.utils;

import android.util.Log;

public class MathUtils {

	public MathUtils() {
		// TODO Auto-generated constructor stub
	}
	
	public static int increment(int x, int y) {
		System.out.println(x + " + " + y + " = " + (x+y));
		System.err.println(x + " + " + y + " = " + (x+y));
		
		Log.v("MathUtils","黑色"+  x + " + " + y + " = " + (x+y));
		Log.d("MathUtils","蓝色"+ x + " + " + y + " = " + (x+y));
		Log.i("MathUtils", "绿色"+ x + " + " + y + " = " + (x+y));
		Log.w("MathUtils","黄色"+ x + " + " + y + " = " + (x+y));
		Log.e("MathUtils", "红色"+ x + " + " + y + " = " + (x+y));
		
		return x+y;
	}

}
