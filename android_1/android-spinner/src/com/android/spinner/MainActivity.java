package com.android.spinner;

import java.util.List;
import java.util.Map;

import com.android.adapter.MyAdapter;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

public class MainActivity extends Activity {
	private Spinner spinner;
	private Spinner spinner2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		spinner = (Spinner) findViewById(R.id.spinner);
		List<String> list = MyAdapter.getData();
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this, 
				android.R.layout.simple_spinner_item, list);
		spinner.setAdapter(adapter);
		
		spinner2 = (Spinner) findViewById(R.id.spinner2);
		//List<Map<String, Object>>
		final List<Map<String, Object>> listmaps = MyAdapter.getListMaps();
		SimpleAdapter simpleAdapter = new SimpleAdapter(MainActivity.this,
				listmaps, R.layout.item, new String[] {"ivLogo","applicationName"}, 
				new int[] {R.id.imageview,R.id.textview});
		spinner2.setAdapter(simpleAdapter);
		spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
				String appname = (String) listmaps.get(position).get("applicationName");
				setTitle(appname);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
}
