package com.android.mediaplayforlocal;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;

public class MusicService extends Service {

	private MediaPlayer mediaPlayer;
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	//	在服务创建的时候，开始播放音乐
	@Override
	public void onCreate() {
		super.onCreate();
		
		//	先搞个application里面的音乐测试一下
//		Here is an example of how to play audio that's available as a local raw resource (saved in your application's res/raw/ directory):

			mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.ayay);
			mediaPlayer.start(); // no need to call prepare(); create() does that for you
	}
	
	//	这个方法先无视掉，反正有它没它都能测试...
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return super.onStartCommand(intent, flags, startId);
	}
	
	//	在服务停止的时候，停止播放音乐
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
		}
		mediaPlayer.release();
		mediaPlayer = null;
	}
	
	//	服务中的方法
	public void pause(){
		//  一会再补上... 先测试...
	}
	
	public class MusicServiceBinder extends Binder implements IMiddlePerson{

		@Override
		public void callMethodInService() {
			pause();
		}
		
	}
}
