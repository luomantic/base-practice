package com.android.mediaplayforlocal;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener{

	 private Button startServiceButton, stopServiceButton, 
	 			fastBackward,  fastForward;  
	 private Intent intent;
	 private MyConn conn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Init();
	}
	
	/*
	 * 实例化组件
	 */
	public void Init() {
		startServiceButton = (Button) findViewById(R.id.startServiceButton);  
        stopServiceButton = (Button) findViewById(R.id.stopServiceButton);  
        fastBackward = (Button) findViewById(R.id.fastBackward);  
        fastForward = (Button) findViewById(R.id.fastForward);  
  
        startServiceButton.setOnClickListener(this);  
        stopServiceButton.setOnClickListener(this);  
        fastBackward.setOnClickListener(this);  
        fastForward.setOnClickListener(this);  
        intent = new Intent(this, MusicService.class); 
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.startServiceButton:
			bindService(intent, conn, BIND_AUTO_CREATE);
			break;

		default:
			break;
		}
	}
	
	//	点击开始按钮，获得服务器返回的IBinder对象
	private class MyConn implements ServiceConnection{

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			conn = (MyConn) service;
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
}
