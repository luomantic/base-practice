package com.android.autotextview;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.MultiAutoCompleteTextView;

public class MainActivity extends Activity {

	private AutoCompleteTextView auto;
	private MultiAutoCompleteTextView multiauto;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		auto = (AutoCompleteTextView) findViewById(R.id.autotext);
		
		//手机没有中文输入法没法检测
		String[] autoString = new String[]{"联合国","联合国安理会","联合国常任委员会","google",
				"google map","Belgium", "France", "Italy", "Germany", "Spain",	
				"Adapter"};
		//第二个参数表示适配器的下拉风格
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				MainActivity.this, android.R.layout.simple_dropdown_item_1line, autoString);
		auto.setAdapter(adapter);
		
		multiauto = (MultiAutoCompleteTextView) findViewById(R.id.MultiAuto);
		multiauto.setAdapter(adapter);
		//完成对选项的拆分功能，以逗号分开
		multiauto.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
	}
	
}
