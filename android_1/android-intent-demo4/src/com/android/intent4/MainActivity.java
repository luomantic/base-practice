package com.android.intent4;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	private Button button;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		button = (Button)findViewById(R.id.button);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//	����һ����ͼ
				Intent intent = new Intent();
				intent.setClass(MainActivity.this, OtherActivity.class);
				OtherActivity.age = 21;
				OtherActivity.name = "jack";
				startActivity(intent);
			}
		});
	}
}
