package com.android.intent4;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class OtherActivity extends Activity {
	private TextView textView;
	public static String name;
	public static int age;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.other);
		textView = (TextView) findViewById(R.id.msg);
		textView.setText("name--->"+name+"\n"+"age--->"+age);
	}
}
