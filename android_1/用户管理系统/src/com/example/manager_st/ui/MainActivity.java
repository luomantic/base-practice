package com.example.manager_st.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

import com.example.manager_st.R;
import com.example.manager_st.entity.Person;
import com.example.manager_st.util.UtilsOfSharedPreferences;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Xml;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	public static List<Person> personList = new ArrayList<Person>();

	private EditText et_userName;
	private EditText et_userSex;
	private EditText et_userAge;
	private LinearLayout llGroup;
	private Button saveData;
	private Button recoveryData;

	private AlertDialog logregDialog;
	
	private EditText et_dialog_username;
	private EditText et_dialog_password;
	
	private CheckBox cbRememberPWD;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		
		Toast.makeText(MainActivity.this, "☆吃饭( ｰ̀дｰ́ ) 2015年10月1日 19:46:34☆ 找错误找的蛋疼，， 这他喵大半天就这点东西 - - 握日☆",
				Toast.LENGTH_LONG).show();
		logregDialog = new AlertDialog.Builder(MainActivity.this).create();
		logregDialog.show();
		logregDialog.getWindow().setContentView(R.layout.log_reg);
		
		logregDialog.getWindow()
					.findViewById(R.id.bt_log).setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {					
							et_dialog_username = (EditText) logregDialog.getWindow().findViewById(R.id.et_username);
							et_dialog_password = (EditText) logregDialog.getWindow().findViewById(R.id.et_password);
							
							String username = et_dialog_username.toString();
							String password = et_dialog_password.toString();
							
							cbRememberPWD = (CheckBox) logregDialog.getWindow().findViewById(R.id.cb_remember_pwd);

							if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
								Toast.makeText(MainActivity.this, "请正确输入", Toast.LENGTH_SHORT).show();
								return;
							}
							
							if (cbRememberPWD.isChecked()) {
								boolean isSuccess = UtilsOfSharedPreferences.saveUserInfo(MainActivity.this, username, password);
								if (isSuccess) {
									Toast.makeText(MainActivity.this, "记住密码", Toast.LENGTH_SHORT).show();
								}else {
									Toast.makeText(MainActivity.this, "取消记住密码", Toast.LENGTH_SHORT).show();
								}
							}
							
							Toast.makeText(MainActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
							logregDialog.dismiss();
						}
					});
		
		//	回显不成功，得把自定义dialog封装到一个类中
		Map<String, String> userInfoMap = UtilsOfSharedPreferences.getUserInfo(this);
		if (userInfoMap != null) {
			et_dialog_username.setText(userInfoMap.get("number"));
			et_dialog_password.setText(userInfoMap.get("password"));
		}
		
		et_userName = (EditText) findViewById(R.id.et_name);
		et_userSex = (EditText) findViewById(R.id.et_sex);
		et_userAge = (EditText) findViewById(R.id.et_age);

		llGroup = (LinearLayout) findViewById(R.id.ll);

		saveData = (Button) findViewById(R.id.bt_savedata);
		recoveryData = (Button) findViewById(R.id.bt_recoverydata);

		saveData.setOnClickListener(new SaveDataToLocal());
		recoveryData.setOnClickListener(new RecoveryData());
	}

	/**
	 * 获取EditView里面的数据，并显示到scrollview
	 * 
	 * @param view
	 */
	public void onClickToAddUser(View view) {

		String userName = et_userName.getText().toString();
		int userID = Integer.valueOf(et_userSex.getText().toString());
		int userAge = Integer.parseInt(et_userAge.getText().toString());

		if (userName.isEmpty()) {
			createAlertDialog();
			return;
		}

		personList.add(new Person(userID, userName, userAge));

		// 获取edittext，保存到textview并显示
		TextView tv = new TextView(MainActivity.this);
		CharSequence text = "姓名：" + userName + "			" + "ID号：" + userID + "			" + "年龄：" + userAge;
		tv.setText(text);
		llGroup.addView(tv);

	}

	/**
	 * 创建AlertDialog
	 */
	public void createAlertDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

		builder.setTitle("操作失败")
					.setMessage("( ｰ̀дｰ́ ) 用户名不能为空哦~")
					.setNeutralButton("返回",
							new android.content.DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {

							}
					});

		AlertDialog alertDialog = builder.create();// 创建一个dialog

		alertDialog.show();// 让对话框显示

	}

	/**
	 * @author Administrator 保存用户数据为本地XML文件
	 */
	public class SaveDataToLocal implements OnClickListener {

		@Override
		public void onClick(View v) {

			XmlSerializer xmlSerializer = Xml.newSerializer();

			try {
				File path = new File(Environment.getExternalStorageDirectory(), "persons.xml");
				FileOutputStream fileOutputStream = new FileOutputStream(path);
				// 指定序列化对象输出的位置和编码
				xmlSerializer.setOutput(fileOutputStream, "utf-8");

				// 开始<?xml// version='1.0' encoding='utf-8' standalone='yes'?>
				xmlSerializer.startDocument("utf-8", true);

				xmlSerializer.startTag(null, "persons");// <persons>

				for (Person person : personList) {
					// 开始写人 ## 加空双引号不如 1.String.valueof(int)
					// 2.Integer.valueof(String) 3.new StringBuild/new
					// StringBuffer然后append
					xmlSerializer.startTag(null, "person");// <person>
					xmlSerializer.attribute(null, "id", String.valueOf(person.getId())); // attribute(属性)

					// 写名字
					xmlSerializer.startTag(null, "name");
					xmlSerializer.text(person.getName());
					xmlSerializer.endTag(null, "name");

					// 写年龄
					xmlSerializer.startTag(null, "age");
					xmlSerializer.text(String.valueOf(person.getAge()));
					xmlSerializer.endTag(null, "age");

					xmlSerializer.endTag(null, "person");// </person>
				}

				xmlSerializer.startTag(null, "persons");// </persons>

				xmlSerializer.endDocument();// 结束

				Toast.makeText(MainActivity.this, "保存成功", Toast.LENGTH_SHORT).show();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	/**
	 * @author Administrator 用pull解析XML文件，回显到scrollview
	 */
	public class RecoveryData implements OnClickListener {

		@Override
		public void onClick(View v) {
			try {
				File path = new File(Environment.getExternalStorageDirectory(), "persons.xml");
				InputStream instream = new FileInputStream(path);

				// 获得pull解析器对象
				XmlPullParser parser = Xml.newPullParser();
				// 指定解析的文件和编码格式
				parser.setInput(instream, "utf-8");

				int eventType = parser.getEventType(); // 获得事件类型

				List<Person> new_personlist = null;
				Person person = null;
				String id;
				while (eventType != XmlPullParser.END_DOCUMENT) {
					String tagName = parser.getName(); // 获得当前节点的名称

					switch (eventType) {
					case XmlPullParser.START_TAG: // 当前等于开始节点<person>
						if ("persons".equals(tagName)) { // <persons>
							new_personlist = new ArrayList<Person>();
						} else if ("person".equals(tagName)) {
							person = new Person();
							id = parser.getAttributeValue(null, "id");
							person.setId(Integer.valueOf(id));
						} else if ("name".equals(tagName)) { // <name>
							person.setName(parser.nextText());
						} else if ("age".equals(tagName)) { // <age>
							person.setAge(Integer.parseInt(parser.nextText()));
						}
						break;
					case XmlPullParser.END_TAG:
						if ("person".equals(tagName)) {
							 String userName = person.getName();
							 int userID = person.getId();
							 int userAge = person.getAge();
							
							 TextView tv = new TextView(MainActivity.this);
							 CharSequence text = "姓名：" + userName + " " +
							 "ID号：" + userID + " " + "年龄："
							 + userAge;
							 tv.setText(text);
							 llGroup.addView(tv);
						// 需要把设置好值的person对象添加到集合中
							new_personlist.add(person);
						}

						break;
					default:
						break;
					}

					eventType = parser.next(); // 获得下一个事件类型
				}

//				for (Person person1 : new_personlist) {
//					String userName = person1.getName();
//					int userID = person1.getId();
//					int userAge = person1.getAge();
//
//					TextView tv = new TextView(MainActivity.this);
//					CharSequence text = "姓名：" + userName + "			" + "ID号：" + userID + "			" + "年龄："
//							+ userAge;
//					tv.setText(text);
//					llGroup.addView(tv);
//				}

				Toast.makeText(MainActivity.this, "恢复成功", Toast.LENGTH_SHORT).show();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
