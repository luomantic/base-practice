package com.android.textview3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		TextView textView = (TextView) findViewById(R.id.textview1);
		TextView textView2 = (TextView) findViewById(R.id.textview2);
		String text1 = "显示Activity1";
		String text2 = "显示activity2";
		// 主要是用来拆分字符串
		SpannableString spannableString = new SpannableString(text1);
		SpannableString spannableString2 = new SpannableString(text2);
		spannableString.setSpan(new ClickableSpan() {

			@Override
			public void onClick(View widget) {
				Intent intent = new Intent(MainActivity.this, Main1Activity.class);
				MainActivity.this.startActivity(intent);
			}
		}, 0, text1.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		spannableString2.setSpan(new ClickableSpan() {

			@Override
			public void onClick(View widget) {
				Intent intent = new Intent(MainActivity.this, Main2Activity.class);
				MainActivity.this.startActivity(intent);
			}
		}, 0, text2.length()-1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

		textView.setText(spannableString);
		textView2.setText(spannableString2);
		textView.setMovementMethod(LinkMovementMethod.getInstance());
		textView2.setMovementMethod(LinkMovementMethod.getInstance());

		// TextView textView2 = (TextView) findViewById(R.id.textview2);
		// textView2.setText("显示activity2");
		// textView2.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// Intent intent = new Intent(MainActivity.this, Main2Activity.class);
		// startActivity(intent);
		// }
		// });
	}
}
