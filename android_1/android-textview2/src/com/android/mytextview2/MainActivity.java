package com.android.mytextview2;

import java.lang.reflect.Field;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	public int getResourcesId(String name) {
		try {
			//	根据资源的ID的变量名获得Field的对象，使用反射机制实现
			Field field = R.drawable.class.getField(name);
			//	取得并返回资源的id的字段(静态变量)的值，使用反射机制
			return Integer.parseInt(field.get(null).toString());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return 0;
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		TextView textView = (TextView) findViewById(R.id.textview);
		textView.setTextColor(Color.BLACK);
		textView.setBackgroundColor(Color.WHITE);
		textView.setTextSize(20);//设置字体的大小
		String html = "图像1<img src='image1'/>图像2<img src='image2'/>图像3<img src='image3'/><p>";
		html+="图像4<a href='http://www.baidu.com'><img src='image4'/></a>图像5<img src='image5'/>";
	
		CharSequence charSequence = Html.fromHtml(html, new ImageGetter() {			
			@Override
			public Drawable getDrawable(String source) {
				//	获得系统资源的信息，比如图片信息
				Drawable drawable = getResources().getDrawable(getResourcesId(source));
				//	第三个图片文件按照50%的比例进行压缩
				if (source.equals("image3")) {
					drawable.setBounds(0, 0, drawable.getIntrinsicWidth()/2,drawable.getIntrinsicHeight()/2);
				}else {
					drawable.setBounds(0,0,drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
				}
				return drawable;
			}
		}, null);
		textView.setText(charSequence);
		textView.setMovementMethod(LinkMovementMethod.getInstance());
	}
}
