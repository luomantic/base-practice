package com.android.junittest.test;

import com.android.junittest.utils.MathUtils;

import android.test.AndroidTestCase;

public class TestCase extends AndroidTestCase {

	public TestCase() {
		
	}

	public void test(){
		int result = MathUtils.increment(9, 9);
		assertEquals(18, result);
	}
}
