package com.example.android_listview;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private ListView listview;
	private ArrayAdapter<String> adapter;
	//表示数据源
	private List<String> data = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		data = MyDataSource.getDataSource();
		adapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_list_item_multiple_choice,data);
		listview = (ListView) findViewById(R.id.listview);
		//给每一个选项添加单选操作
//		listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		listview.setAdapter(adapter);
		listview.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, 
					int arg2, long arg3) {
				Toast.makeText(MainActivity.this, "click me ", 
						Toast.LENGTH_SHORT).show();
				return false;
			}
		});
	}
}
