package com.android.sqlitedemo;

import java.util.List;

import com.android.sqlitedemo.dao.PersonDao;
import com.android.sqlitedemo.entities.Person;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity2 extends Activity {

	private ListView mListView;
	private List<Person> personList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mListView = (ListView) findViewById(R.id.listview);
		
		PersonDao dao = new PersonDao(this);
		personList = dao.queryAll();
		
		//	把view层对象ListView和控制器BaseAdapter关联起来
		mListView.setAdapter( new MyAdapter());
	}

	/**
	 * @author Administrator
	 *数据适配器
	 */
	class MyAdapter extends BaseAdapter{

		private static final String TAG = "MyAdapter";

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getCount()
		 * 定义ListView的数据的长度
		 */
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return personList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 * 此方法返回的是ListView的列表中某一行的View对象
		 * positon 当前返回的View的索引位置
		 * convertView	缓存对象
		 * parent 就是ListView对象
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = null;

			if (convertView == null) {
				//布局填充器对象，用于把XML布局转换成View对象
				LayoutInflater layoutInflater = MainActivity2.this.getLayoutInflater();
				view = layoutInflater.inflate(R.layout.listview_item, null);
			}else {
				view = convertView;
			}
			
			//给view中的姓名和年龄赋值
			TextView tvName = (TextView) view.findViewById(R.id.tv_listview_item_name);
			TextView tvAge = (TextView) view.findViewById(R.id.tv_listview_item_age);
			
			Person person = personList.get(position);
			
			tvName.setText("姓名：" + person.getName());
			tvAge.setText("年龄：" + person.getAge());
			
			return view;
		}
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
