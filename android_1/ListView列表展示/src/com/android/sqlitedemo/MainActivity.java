package com.android.sqlitedemo;

import java.util.List;

import com.android.sqlitedemo.dao.PersonDao;
import com.android.sqlitedemo.entities.Person;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {

	private ListView mListView;
	private List<Person> personList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mListView = (ListView) findViewById(R.id.listview);
		
		PersonDao dao = new PersonDao(this);
		personList = dao.queryAll();
		
		//	把view层对象ListView和控制器BaseAdapter关联起来
		mListView.setAdapter( new MyAdapter());
	}

	/**
	 * @author Administrator
	 *数据适配器
	 */
	class MyAdapter extends BaseAdapter{

		private static final String TAG = "MyAdapter";

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getCount()
		 * 定义ListView的数据的长度
		 */
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return personList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 * 此方法返回的是ListView的列表中某一行的View对象
		 * positon 当前返回的View的索引位置
		 * convertView	缓存对象
		 * parent 就是ListView对象
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			TextView tv = null;
			if (convertView != null) {	//判断缓存对象是否为null，不为null时已经缓存了对象
				Log.i(TAG, "getView:复用" + position);
				tv  = (TextView) convertView;
			}	else {	// 等于null，说明第一次显示，新创建
				Log.i(TAG, "getView:新建" + position);
				tv = new TextView(MainActivity.this);
			}
			
			Person person = personList.get(position);	//	获得指定位置的数据，进行TextView的绑定
			tv.setTextSize(25);
			tv.setText(person.toString());
			
			return tv;
		}
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
