package com.pull.test;

import java.io.InputStream;
import java.util.List;

import com.pull.domain.Person;
import com.pull.http.HttpUtils;
import com.pull.parser.PullXmlTools;

public class Test {

	public Test() {
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) {
		String path = "http://127.0.0.1:8080/myhttp/person.xml";
		InputStream inputStream = HttpUtils.getXML(path);
		List<Person> list = PullXmlTools.parseXML(inputStream, "utf-8");
		for (Person person:list){
			System.out.println(person.toString());
		}
	}
	
}
