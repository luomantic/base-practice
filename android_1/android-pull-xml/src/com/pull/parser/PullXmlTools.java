package com.pull.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.pull.domain.Person;

/*
 * 主要是使用pull解析xml
 */
public class PullXmlTools {

	public PullXmlTools() {
		// TODO Auto-generated constructor stub
	}
	
	/*
	 * 从服务器获得xml文件，以流的形式放回
	 * encode必须与xml文件中的编码格式是一致的
	 */
	public static List<Person> parseXML(InputStream inputStream,String encode){
		List<Person> list = null;
		Person person = null; // 装载解析每一个person节点的内容
		
		try {
			//	创建一个xml文件解析的工厂
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			//	获得xml解析类的引用
			XmlPullParser parser = factory.newPullParser();
			parser.setInput(inputStream, encode);
			//	获得事件的类型
			int eventType = parser.getEventType();
			while (eventType!=XmlPullParser.END_DOCUMENT){
				switch (eventType) {
				case XmlPullParser.START_DOCUMENT:
					list = new ArrayList<Person>();
					break;
				case XmlPullParser.START_TAG:
					if ("person".equals(parser.getName())){
						person = new Person();
						// 取出属性值
						int id = Integer.parseInt(parser.getAttributeValue(0));
						person.setId(id);
					}else if ("name".equals(parser.getName())){
						String name = parser.nextText(); // 获取该节点的内容
						person.setName(name);
					}else if ("age".equals(parser.getName())){
						int age = Integer.parseInt(parser.nextText());
						person.setAge(age);
					}
					break;
				case XmlPullParser.END_TAG:
					if ("pserson".equals(parser.getName())) {
						list.add(person);
						person = null;
					}
					break;
				default:
					break;
				}
				eventType = parser.next();
			}
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
}
