package com.android.radiobutton1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends Activity {

	private RadioGroup group;
	private Button button;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		group = (RadioGroup) findViewById(R.id.sex);
		button = (Button) findViewById(R.id.button);
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int len = group.getChildCount();//获得单选按钮的选项个数
				String msg = "";
				for (int i=0; i<len; i++) {
					RadioButton radioButton = (RadioButton) group.getChildAt(i);
					if (radioButton.isChecked()) {
						msg = radioButton.getText().toString();
						break;
					}
				}
				Toast.makeText(MainActivity.this, msg, 1).show();
			}
		});
	}
}
