package com.android.smshelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListSmsActivity extends Activity {

	private ListView lv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_sms);
		lv = (ListView) findViewById(R.id.lv);
		lv.setAdapter(new ArrayAdapter<String>(this, R.layout.sms_item, R.id.tv_info, SmsContent.sms_contents));
		
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String smsinfo = SmsContent.sms_contents[position];
				Intent data = new Intent();
				data.putExtra("smsinfo", smsinfo);
				//	设置数据
				setResult(0, data);
				//	关闭掉当前的activity，并回传数据给 onActivityResult
				finish();
			}
		});
	}
	
}
