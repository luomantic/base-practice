package com.android.sqlitedemo;

import java.util.List;

import com.android.sqlitedemo.dao.PersonDao;
import com.android.sqlitedemo.entities.Person;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		LinearLayout llList = (LinearLayout) findViewById(R.id.ll_List);
		
		PersonDao dao = new PersonDao(this);
		List<Person> personList = dao.queryAll();
		
		if (personList!=null) {
			TextView tv;
			for (Person person : personList) {
				//	向线性布局中添加一个textview
				tv = new TextView(this);
				tv.setText(person.toString());
				tv.setTextSize(20);
				
				llList.addView(tv);
			}
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
