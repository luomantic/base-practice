package com.android.playmeimei;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

public class MainActivity extends Activity {

	private ImageView iv;
	//	可以修改的位图
	private Bitmap alertBitmap;
	private Canvas canvas;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		iv = (ImageView) findViewById(R.id.iv);
		
		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.pre21);
		//创建一个空白原图拷贝
		alertBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
		canvas = new Canvas(alertBitmap);
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		canvas.drawBitmap(bitmap, new Matrix(), paint);
		
		iv.setImageBitmap(alertBitmap);
		
		iv.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: 
					System.out.println("action down");
					break;
				case MotionEvent.ACTION_MOVE: 
					int x = (int) event.getX();	// 相对于图片的坐标
//					event.getRawX();//	相对于屏幕的坐标
					int y = (int) event.getY();
					System.out.println("设置" + x + ","+y + "为透明颜色");
					for(int i=-20; i<21; i++) {
						for (int j = -20; j<21; j++) {
							try {
								alertBitmap.setPixel(x+i, y+j, Color.TRANSPARENT);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					
					iv.setImageBitmap(alertBitmap);
					break;
				case MotionEvent.ACTION_UP: 
					MediaPlayer.create(getApplicationContext(), R.raw.higirl).start();
					break;
				default:
					break;
				}
				
				return true;	//可以重复循环的处理事件
			}
		});
	}
}
