package com.android.edittext;

import java.lang.reflect.Field;
import java.util.Random;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	private EditText edittext;
	private Button button;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		edittext = (EditText) findViewById(R.id.edittext);
		button = (Button) findViewById(R.id.button);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int randomId = 1 + new Random().nextInt(9);
				try {
					Field field = R.drawable.class.getDeclaredField("face"+randomId);
					int resourceId = Integer.parseInt(field.get(null).toString());
					//在android中要显示图片信息，必须要使用Bitmap位图的对象来装载
					Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
							resourceId);
					ImageSpan imageSpan = new ImageSpan(MainActivity.this, bitmap);
					SpannableString spannableString = new SpannableString("face");
					spannableString.setSpan(imageSpan, 0, 4, 
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					edittext.append(spannableString);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
	}
	
}
