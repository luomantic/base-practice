package com.android.bindremote;

import com.android.remoteservice.IMiddlePerson;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;

public class MainActivity extends Activity {

	private Myconn conn;
	private IMiddlePerson imp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void bind(View view) {
		Intent intent = new Intent();
		intent.setAction("com.android.remoteservice");
		conn = new Myconn();
		bindService(intent, conn, BIND_AUTO_CREATE);
	}
	
	private class Myconn implements ServiceConnection {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			imp = IMiddlePerson.Stub.asInterface(service);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	@Override
	protected void onDestroy() {
		unbindService(conn);
		super.onDestroy();
	}
	
	public void unbind(View view) {
		
	}
	
	public void call (View view) {
		try {
			imp.callMethodInService();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
