package com.http.post;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class HttpUtils {
	//	请求服务器端的url
	private static String PATH = "http://localhost:8080/myhttp/xx";
	private static URL url;
	
	public HttpUtils() {
		// TODO Auto-generated constructor stub
	}
	
	static {
		try {
			url = new URL(PATH);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * params 填写的url的参数
	 * encode 字节编码
	 */
	public static String sendPostMessage(Map<String, String> params, String encode) {
		// 作为StringBuffer初始化的字符串
		StringBuffer buffer = new StringBuffer();
//		buffer.append("?");
		try {
			if (params != null && !params.isEmpty()) {
				for (Map.Entry<String, String> entry : params.entrySet()) {
					buffer.append(entry.getKey())
						  .append("=")
						  .append(URLEncoder.encode(entry.getValue(), encode))
						  .append("&");
				}
				/*
				 * 另一种方式
				 * 	 Iterator iterator = params.entrySet().iterator();
				 * 	 while (iterator.hasNext()){
				 * 	 	Map.Entry entry = iterator.next();
				 *      buffer.append(entry.getkey())
				 *            .append("=")
				 *            .append(URLEncoder.encode(entry.getvalue(), encode))
				 *            .append("&");
				 *   }
				 */
			}
			//	删除掉最后一个&
			buffer.deleteCharAt(buffer.length()-1);
			System.out.println(buffer.toString());
			
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setConnectTimeout(3000);
			urlConnection.setRequestMethod("POST");
			urlConnection.setDoInput(true); // 从服务器获取数据
			urlConnection.setDoOutput(true); // 向服务器写数据
			//	获得上传信息的字节大小以及长度
			byte[] mydata = buffer.toString().getBytes();
			//	表示设置请求体的类型是文本类型
			urlConnection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded"); 
			urlConnection.setRequestProperty("Content-Length", 
					String.valueOf(mydata.length));
			//	获得输出流，向服务器输出数据
			OutputStream outputStream = urlConnection.getOutputStream();
			outputStream.write(mydata);
			//	获得服务器响应的结果和状态码
			int responseCode = urlConnection.getResponseCode();
			if (responseCode==200) {
				return changeInputStream(urlConnection.getInputStream(), encode);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	// 将一个输入流转换成指定编码的字符串
	private static String changeInputStream(InputStream inputStream, String encode) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(); 
		byte[] data = new byte[1024];
		int len = 0;
		String result = "";
		if (inputStream!=null) {
			try {
				while ((len=inputStream.read(data)) != -1) {
					outputStream.write(data, 0, len);
				}
				result = new String(outputStream.toByteArray(), encode);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
		
		return result;
	}

	public static void main(String[] args) {
		Map<String,String> params = new HashMap<String,String>();
		params.put("username", "admin");
		params.put("password", "123");
		String result = sendPostMessage(params,"utf-8");
		System.out.println("--result-->"+result);
	}
}
