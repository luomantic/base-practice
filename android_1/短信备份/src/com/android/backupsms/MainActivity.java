package com.android.backupsms;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlSerializer;

import com.android.backupsms.entities.SmsInfo;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Xml;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	/**
	 * 备份短信
	 * @param view
	 */
	public void backupSMS(View view) {
		//1.查出所有的短信
		Uri uri = Uri.parse("content://sms/");
		
		ContentResolver resolver = getContentResolver();
		Cursor cursor = resolver.query(uri, new String[]{"_id", "address", "date", "type","body"},
				null, null, null);
		if (cursor!=null && cursor.getCount()>0) {
			List<SmsInfo> smslist = new ArrayList<SmsInfo>();
			SmsInfo sms;
			while (cursor.moveToNext()) {	//	控制游标结果集的指针向下移一位，当到最后一位停止，返回false
				sms = new SmsInfo();
				sms.setId(cursor.getInt(0)); // id
				sms.setAddress(cursor.getString(1));	// address 短信号码
				sms.setDate(cursor.getLong(2));	//	短信的日期
				sms.setType(cursor.getInt(3));	// 短信的类型，接收1/发送2
				sms.setBody(cursor.getString(4));	// 设置短信的内容
				smslist.add(sms);
			}
			cursor.close();
			
			//2.序列化到本地
			writeToLocal(smslist);
		}
	}
	
	/**
	 * 序列化到本地
	 * @param smslist
	 */
	private void writeToLocal(List<SmsInfo> smslist) {
		
		XmlSerializer serializer = Xml.newSerializer();	// 得到序列化对象
		//	指定输出位置
		try {
			File path = new File(Environment.getExternalStorageDirectory(), "smsbackup.xml");
			FileOutputStream fos = new FileOutputStream(path);
			serializer.setOutput(fos, "utf-8");
			
			serializer.startDocument("utf-8", false);

			serializer.startTag(null, "smss");
			
			for (SmsInfo smsInfo : smslist) {
				serializer.startTag(null, "sms");
				serializer.attribute(null, "_id", String.valueOf(smsInfo.getId()));
				
				//	写号码
				serializer.startTag(null, "address");
				serializer.text(smsInfo.getAddress());
				serializer.endTag(null, "address");
				
				//	写时间
				serializer.startTag(null, "date");
				serializer.text(String.valueOf(smsInfo.getDate()));
				serializer.endTag(null, "date");
				
				//    写类型
				serializer.startTag(null, "type");
				serializer.text(String.valueOf(smsInfo.getType()));
				serializer.endTag(null, "type");
				
				//	写内容
				serializer.startTag(null, "body");
				serializer.text(smsInfo.getBody());
				serializer.endTag(null, "body");
				
				serializer.endTag(null, "sms");
			}
			
			serializer.endTag(null, "smss");
			
			serializer.endDocument();
			
			Toast.makeText(this, "备份成功", Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			Toast.makeText(this, "备份失败", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		} 
	}
}
