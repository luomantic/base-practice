package com.dom.xml.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.Node;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.dom.xml.domain.Book;
import com.dom.xml.http.HttpUtils;

public class DomService {

	public DomService() {
		// TODO Auto-generated constructor stub
	}

	public List<Book> getBooks(InputStream inputStream) throws ParserConfigurationException, SAXException, IOException{
		List<Book> list = new ArrayList<Book>();
		// 	创建一个document解析的工厂
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder bulider = factory.newDocumentBuilder();
		Document document = (Document) bulider.parse(inputStream);
		//	获得稳定的元素节点
		Element element = (Element) document.getDocumentElement();
		NodeList bookNodes = element.getElementsByTagName("book");
		for (int i=0; i<bookNodes.getLength(); i++){
			Element bookeElement = (Element) bookNodes.item(i);
			Book book = new Book();
			book.setId(Integer.parseInt(bookeElement.getAttribute("id")));
			NodeList childNodes = bookeElement.getChildNodes();
			for(int j=0; j<childNodes.getLength(); j++) {
				if(childNodes.item(j).getNodeType() == Node.ELEMENT_NODE){
					if ("name".equals(childNodes.item(j).getNodeName())){
						book.setName(childNodes.item(j).getFirstChild().getNodeValue());
					}else if("price".equals(childNodes.item(j).getNodeName())){
						book.setPrice(Float.parseFloat(childNodes.item(j)
								.getFirstChild().getNodeValue()));
					}
				}
			}
			list.add(book);
		}
		return list;
	}
	public static void main(String[] args) {
		String path = "http://127.0.0.1:8080/myhttp/book.xml";
		InputStream inputStream = HttpUtils.getXML(path);
		DomService service = new DomService();
		try {
			List<Book> list = service.getBooks(inputStream);
			for (Book book:list) {
				System.out.println(book.toString());
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
