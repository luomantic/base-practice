package com.android.gridview;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

public class MainActivity extends Activity implements OnItemSelectedListener, 
OnItemClickListener{
	private ImageView imageview;
	private int[] resIds = new int[]{
			R.drawable.item1,R.drawable.item2,R.drawable.item3,
			R.drawable.item4,R.drawable.item5,R.drawable.item6,
			R.drawable.item7,R.drawable.item8,R.drawable.item9,
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, 
			long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub
		
	}
}
