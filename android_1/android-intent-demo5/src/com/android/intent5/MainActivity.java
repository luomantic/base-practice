package com.android.intent5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {
	private Button button;
	private final static int REQUESTCODE = 1;	//	请求码
	private EditText one, two, result;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		button = (Button) findViewById(R.id.button);
		one = (EditText) findViewById(R.id.one);
		two = (EditText) findViewById(R.id.two);
		result = (EditText) findViewById(R.id.result);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int a = Integer.parseInt(one.getText().toString());
				int b = Integer.parseInt(two.getText().toString());
				Intent intent = new Intent(MainActivity.this, OtherActivity.class);
				intent.putExtra("a", a);
				intent.putExtra("b", b);
				//	采用startActivityForResult跳到另一个activity传递intent跟请求码
				startActivityForResult(intent, REQUESTCODE);
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode==2) {
			if (requestCode==REQUESTCODE) {
				int three = data.getIntExtra("three", 0);
				result.setText(String.valueOf(three));
			}
		}
	}
}
