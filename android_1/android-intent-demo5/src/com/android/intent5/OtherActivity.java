package com.android.intent5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class OtherActivity extends Activity {
	private Button button;
	private TextView textView;
	private EditText editText;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.other);
		button = (Button) findViewById(R.id.button2);
		textView = (TextView) findViewById(R.id.msg);
		editText = (EditText) findViewById(R.id.three);
		Intent intent = getIntent();
		int a = intent.getIntExtra("a", 0);
		int b = intent.getIntExtra("b", 0);
		textView.setText(a+" + "+b+" = "+" ? ");
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				int three = Integer.parseInt(editText.getText().toString());
				intent.putExtra("three", three);
				//	通过Intent对象返回结果,setResualt方法
				setResult(2, intent);
				finish();	//	结束当前的activity的生命周期
			}
		});
	}
}
