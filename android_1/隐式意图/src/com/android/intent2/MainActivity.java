package com.android.intent2;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void click(View view) {
		Intent intent = new Intent();
		intent.setAction("com.android.intent2.open2");
		intent.addCategory(intent.CATEGORY_DEFAULT);
//		intent.setType("application/gaga");
//		intent.setData(Uri.parse("jianren:����"));
		intent.setDataAndType(Uri.parse("jianren:����"), "application/gaga");
		startActivity(intent);
	}
}
