package com.android.seekbar;

import android.app.Activity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class MainActivity extends Activity implements OnSeekBarChangeListener{
	private TextView textview1,textview2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		textview1 = (TextView) findViewById(R.id.textview1);
		textview2 = (TextView) findViewById(R.id.textview2);
		SeekBar seekBar = (SeekBar) findViewById(R.id.seekbar1);
		SeekBar seekBar2 = (SeekBar) findViewById(R.id.seekbar2);
		seekBar.setOnSeekBarChangeListener(this);
		seekBar2.setOnSeekBarChangeListener(this);
	}

	// 当滑动滑杆的时候会触发的事件
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		// TODO Auto-generated method stub
		if (seekBar.getId()==R.id.seekbar1) {
			textview1.setText("seekbar1的当前位置是:"+progress);
		}else {
			textview2.setText("seekbar2的当前位置是:"+progress);
		}
	}

	//从哪里开始拖动
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		if (seekBar.getId()==R.id.seekbar1) {
			textview1.setText("seekbar1开始拖动");
		}else {
			textview2.setText("seekbar2开始拖动");
		}
	}

	//从哪里结束拖动
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		if (seekBar.getId()==R.id.seekbar1) {
			textview1.setText("seekbar1停止拖动");
		}else {
			textview2.setText("seekbar2停止拖动");
		}
	}
}
