package com.android.intent0;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class OtherActivity extends Activity {
	private TextView textView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.other);
		textView = (TextView) findViewById(R.id.msg);
		
		Intent intent = getIntent();
		int age = intent.getIntExtra("age", 0);
		String name = intent.getStringExtra("name");
		String address = intent.getStringExtra("address");
		
		textView.setText("age--->"+age+"\n"+"name--->"+name+
				"\n"+"address--->"+address);
	}
}
