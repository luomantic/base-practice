package com.android.sqlite.test;

import java.util.List;

import com.android.sqlitedemo.dao.PersonDao;
import com.android.sqlitedemo.db.PersonSQLiteOpenHelper;
import com.android.sqlitedemo.entities.Person;

import android.test.AndroidTestCase;
import android.util.Log;

public class TestCase extends AndroidTestCase {
	
	private static final String TAG = "TestCase";

	public void test() {
		//	数据库什么时候创建
		PersonSQLiteOpenHelper openHelper = new PersonSQLiteOpenHelper(getContext());
		
		//	第一次链接数据库时创建数据库文件，oncreate方法会被调用
		openHelper.getReadableDatabase();
	}
	
	public void testInsert() {
		PersonDao dao = new PersonDao(getContext());
		
		dao.insert(new Person(0, "冠希", 26));
	}
	
	/**
	 * 根据id删除
	 */
	public void testDelete()	{
		PersonDao dao = new PersonDao(getContext());
		dao.delete(1);
	}
	
	public void testUpdate()	{
		PersonDao dao = new PersonDao(getContext());
		dao.update(3, "jiefeng");
	}
	
	public void testQueryAll()	{
		PersonDao dao = new PersonDao(getContext());
		List<Person> personList = dao.queryAll();
		
		for (Person person : personList) {
			Log.i(TAG, person.toString());
		}
	}
	
	public void testQueryItem() {
		PersonDao dao = new PersonDao(getContext());
		Person person = dao.queryItem(3);
		Log.i(TAG, person.toString());
	}
}
