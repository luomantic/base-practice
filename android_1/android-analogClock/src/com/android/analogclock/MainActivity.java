package com.android.analogclock;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{
	private Button button1,button2;
	private int hourOfDay,minute;
	private int monthOfYear,dayOfMonth;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		button1 = (Button) findViewById(R.id.button);
		button2 = (Button) findViewById(R.id.button2);
		button1.setOnClickListener(this);
		button2.setOnClickListener(this);
		//获得当前的时间，获得小时和分钟
		Calendar calendar = Calendar.getInstance();
		hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
		minute = calendar.get(Calendar.MINUTE);
		
		monthOfYear = calendar.get(Calendar.MONTH);
		dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.button:
			//
			TimePickerDialog timePickerDialog = new TimePickerDialog(MainActivity.this, 
					new MyTimePickerDialog(), hourOfDay, minute, true);
			timePickerDialog.show();//显示对话框
			break;
		case R.id.button2:
			DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, 
					new MyDatePickerDialog(), 2015, monthOfYear, dayOfMonth);
			datePickerDialog.show();
			break;
		default:
			break;
		}
	}
	
	public class MyTimePickerDialog implements OnTimeSetListener{

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// TODO Auto-generated method stub
			Toast.makeText(MainActivity.this, "hourOfDay:"+hourOfDay+"	minute:"+minute,
					Toast.LENGTH_SHORT).show();;
		}
		
	}
	
	public class MyDatePickerDialog implements OnDateSetListener{

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			// TODO Auto-generated method stub
			Toast.makeText(MainActivity.this, "monthOfYear:"+monthOfYear+"	dayOfMonth:"+
			dayOfMonth, Toast.LENGTH_SHORT).show();;
		}
		
	}
}
