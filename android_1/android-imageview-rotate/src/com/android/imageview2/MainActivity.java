package com.android.imageview2;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class MainActivity extends Activity implements OnSeekBarChangeListener{
	private int minWidth = 80;
	private ImageView imageView;
	private TextView textview, textview2;
	private Matrix matrix = new Matrix();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		imageView = (ImageView) findViewById(R.id.imageview);
		SeekBar seekBar = (SeekBar) findViewById(R.id.seekbar);
		SeekBar seekBar2 = (SeekBar) findViewById(R.id.seekbar2);
		textview = (TextView) findViewById(R.id.textview);
		textview2 = (TextView) findViewById(R.id.textview2);
		seekBar.setOnSeekBarChangeListener(this);
		seekBar2.setOnSeekBarChangeListener(this);
		//
		DisplayMetrics dMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		seekBar.setMax(dMetrics.widthPixels-minWidth);
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		// TODO Auto-generated method stub
		if (seekBar.getId()==R.id.seekbar) {
			int newWidth = progress+minWidth;
			int newHeight = (int)newWidth*3/4;//按照原图片进行缩放
			imageView.setLayoutParams(new LinearLayout.LayoutParams(newWidth,newHeight));
			textview.setText("图像宽度："+newWidth+"		图像高度："+newHeight);
		}else if (seekBar.getId()==R.id.seekbar2) {
			Bitmap bitmap = ((BitmapDrawable)(getResources().getDrawable(R.drawable.dog))).getBitmap();
			matrix.setRotate(progress);//设置翻转的角度
			bitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),
					matrix,true);
			imageView.setImageBitmap(bitmap);
			textview2.setText(progress+"度");
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}
}
