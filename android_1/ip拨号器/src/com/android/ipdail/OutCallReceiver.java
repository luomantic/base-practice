package com.android.ipdail;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class OutCallReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		String number = getResultData();
		System.out.println("哈哈，有外拨电话打出去了");
		
		SharedPreferences sp = context.getSharedPreferences("config", Context.MODE_PRIVATE);
		String ipnumber = sp.getString("ipnumber", "");
		
		//	判断是否是长度，是否有前缀
		setResultData(ipnumber + number);
	}

}
