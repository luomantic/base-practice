package com.android.ipdail;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

	private EditText et_number;
	private Button button;
	private SharedPreferences sp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		et_number = (EditText) findViewById(R.id.et_number);
		button  = (Button) findViewById(R.id.save);
		sp = getSharedPreferences("config", MODE_PRIVATE);
		
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String ipnumber = et_number.getText().toString().trim();
				if (TextUtils.isEmpty(ipnumber)) {
					Toast.makeText(MainActivity.this, "清楚ip号码成功", Toast.LENGTH_SHORT).show();
				}else {
					Toast.makeText(MainActivity.this, "保存ip号码成功", Toast.LENGTH_SHORT).show();
				}
				Editor editor = sp.edit();
				editor.putString("ipnumber", ipnumber);
				editor.commit();
			}
		});
	}
}
