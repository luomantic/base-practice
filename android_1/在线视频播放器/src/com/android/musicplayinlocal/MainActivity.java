package com.android.musicplayinlocal;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

/**
 * @author Administrator
 *	没用到服务，没必要看...  看服务请移步本地音乐播放器
 */
public class MainActivity extends Activity implements OnClickListener{

	private EditText et_path;
	private Button bt_play, bt_pause, bt_stop, bt_replay;
	private MediaPlayer mediaPlayer;
	private SurfaceView sv;
	private SurfaceHolder holder;
	private int positon;
	private String filepath;
	private SeekBar seekbar1;
	private Timer timer;
	private TimerTask task;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		et_path = (EditText) findViewById(R.id.et_path);
		bt_play = (Button) findViewById(R.id.bt_play);
		bt_pause = (Button) findViewById(R.id.bt_pause);
		bt_stop = (Button) findViewById(R.id.bt_stop);
		bt_replay = (Button) findViewById(R.id.bt_replay);
		
		mediaPlayer = new MediaPlayer();
		
		bt_play.setOnClickListener(this);
		bt_pause.setOnClickListener(this);
		bt_stop.setOnClickListener(this);
		bt_replay.setOnClickListener(this);
		
		seekbar1 = (SeekBar) findViewById(R.id.seekBar1);
		seekbar1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				int position = seekBar.getProgress();
				mediaPlayer.seekTo(position);
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				// TODO Auto-generated method stub
				
			}
		});
		
//	得到surfaceview
		sv = (SurfaceView) findViewById(R.id.sv);
		//	得到显示界面内容的容器
		holder = sv.getHolder();
		holder.addCallback(new Callback() {
			
			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				System.out.println("destoryed");
				if (mediaPlayer!=null && mediaPlayer.isPlaying()) {
					positon = mediaPlayer.getCurrentPosition();
					mediaPlayer.stop();
					mediaPlayer.release();
					mediaPlayer = null;
					
					timer.cancel();
					task.cancel();
					timer = null;
					task = null;
				}
			}
			
			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				System.out.println("created");
				if (positon>0) { //记录有播放记录
					try {
						mediaPlayer = new MediaPlayer();
						mediaPlayer.setDataSource(filepath);  // 设置播放的数据源
						mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
						mediaPlayer.setDisplay(holder);
						mediaPlayer.prepare();//准备开始播放 播放的逻辑是c代码在新的线程里面执行的
						mediaPlayer.start();
						mediaPlayer.seekTo(positon);
						bt_play.setEnabled(false);
						mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
							
							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub
								bt_play.setEnabled(true);
							}
						});
						
						int max = mediaPlayer.getDuration();
						seekbar1.setMax(max);
						timer = new Timer();
						task = new TimerTask() {
							@Override
							public void run() {
								seekbar1.setProgress(mediaPlayer.getCurrentPosition());
							}
						};
						timer.schedule(task, 0, 300);
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
				// TODO Auto-generated method stub
				System.out.println("changed");
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_play:
			play();
			break;
		case R.id.bt_pause:
			pause();
			break;
		case R.id.bt_stop:
			stop();
			break;
		case R.id.bt_replay:
			replay();
			break;
		default:
			break;
		}
	}
	
	public void play() {
		filepath = et_path.getText().toString().trim();
//		File file = new File(filepath);
		if (filepath.startsWith("http://")) {
			try {
				mediaPlayer = new MediaPlayer();
				mediaPlayer.setDataSource(filepath);  // 设置播放的数据源
				mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
				mediaPlayer.setDisplay(holder);
//				mediaPlayer.setVideoScalingMode();自定义宽高比例，默认是适应屏幕
				mediaPlayer.prepare();//准备开始播放 播放的逻辑是c代码在新的线程里面执行的
				mediaPlayer.start();

				//得到文件的总时长度...单位ms 设置拖动进度条的最大值
				int max = mediaPlayer.getDuration();
				seekbar1.setMax(max);
				timer = new Timer();
				task = new TimerTask() {
					@Override
					public void run() {
						seekbar1.setProgress(mediaPlayer.getCurrentPosition());
					}
				};
				timer.schedule(task, 0, 300);
				
				bt_play.setEnabled(false);
				mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						bt_play.setEnabled(true);
					}
				});
			} catch ( Exception e) {
				e.printStackTrace();
				Toast.makeText(this, "播放失败", Toast.LENGTH_SHORT).show();
			}
		}else {
			Toast.makeText(this, "文件不存在，请检查文件路径", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void pause() {
		if ("继续".equals(bt_pause.getText().toString())) {
			mediaPlayer.start();
			bt_pause.setText("暂停");
			return;
		}
		if (mediaPlayer!=null && mediaPlayer.isPlaying()) {
			mediaPlayer.pause();
			bt_pause.setText("继续");
		}
	}
	
	public void stop() {
		if (mediaPlayer!=null && mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
		}
		bt_pause.setText("暂停");
		bt_play.setEnabled(true);
	}
	
	public void replay() {
		if (mediaPlayer!=null && mediaPlayer.isPlaying()) {
			mediaPlayer.seekTo(0);
		}else {
			play();
		}
		bt_pause.setText("暂停");
	}
	
}
