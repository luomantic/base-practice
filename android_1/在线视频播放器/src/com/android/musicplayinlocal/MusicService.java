package com.android.musicplayinlocal;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class MusicService extends Service {

	//2.实现服务成功绑定的代码，返回一个中间人对象
	@Override
	public IBinder onBind(Intent intent) {	//  解除绑定什么的先不写了、、反正现在已经能够通过测试，一会再补上..
		System.out.println("绑定服务成功..");	
		return new MyBinder();
	}

	// 这是服务里的一个方法     其他几个方法一会再加、、
	public void play(){
		System.out.println("我是服务里的play方法... 我开始播放音乐");
	}
	
	//	1.通过一个中间人接口，暴露服务里的方法...
	class MyBinder extends Binder implements IMiddlePerson{

		@Override
		public void callMethodInService() {
			// TODO Auto-generated method stub
			
		}
		
		//	此方法不在接口定义之内...  随便你爱干啥干啥，反正activity里面也调用不到...
		public void Test(){
			System.out.println("你大爷的.... 啦啦啦啦啦");
		}
		
	}
	
}
