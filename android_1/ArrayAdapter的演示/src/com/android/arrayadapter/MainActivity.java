package com.android.arrayadapter;

import android.R.id;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ListView mListView = (ListView) findViewById(R.id.listview);
		
		String[] textArray = {"功能1","功能2","功能3","功能4","功能5","功能6","功能7","功能8"};
		
		/*
		 * 定义数据适配器
		 * android.R.layout.simple_list_item_1    ListView的子条目现实的布局的id
		 * textArray 显示在ListVIew列表中的数据
		 */
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
				textArray);

		mListView.setAdapter(adapter);
		
	}
}
