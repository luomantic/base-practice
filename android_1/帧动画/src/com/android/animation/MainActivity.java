package com.android.animation;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends Activity {

	private ImageView iv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ImageView iv = (ImageView) findViewById(R.id.iv);
		//	把帧动画的资源文件制定为iv的背景
		iv.setBackgroundResource(R.drawable.frameanimation);
		//	获取iv的背景
		AnimationDrawable ad = (AnimationDrawable) iv.getBackground();
		ad.start();
	}
	
}
