package com.android.copyimage;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity {

	private ImageView iv1,iv2;
	private Bitmap alterBitmap;
	private Bitmap  srcBmp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		iv1 = (ImageView) findViewById(R.id.iv1);
		iv2 = (ImageView) findViewById(R.id.iv2);
		
		//给第一个imageview默认设置一个位图
		srcBmp = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
		iv1.setImageBitmap(srcBmp);
		
		//创建原图的一个副本。可修改  创建的是一个空白的图形
		alterBitmap = Bitmap.createBitmap(srcBmp.getWidth(), srcBmp.getHeight(), srcBmp.getConfig());
	}
	
	/**
	 * 创建原图bitmap的一个副本
	 * @param view
	 */
	public void click(View view) {
		//1 准备一个面板 在上面放上准备好的空白的位图
		Canvas canvas = new Canvas(alterBitmap);
		//2 准备一个画笔
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		//3 画画
		Matrix m = new Matrix();
//		m.setValues(new float[]{
//				0.5f, 0, 0,
//				0, 0.5f, 0,
//				0, 0, 1
//		});
		m.setScale(1.0f, 0.5f);
		canvas.drawBitmap(srcBmp, m, paint);
		
		iv2.setImageBitmap(alterBitmap);
	}
}
