package com.android.contactobserverdemo;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//	监听系统短信
		ContentResolver resolver = getContentResolver();
		
		//	注册一个内容观察者短信数据库
		resolver.registerContentObserver(Uri.parse("content://sms/"), true, 
				new MyContentObserver(new Handler()));
	}
	
	/**
	 * @author Administrator
	 *内容观察者
	 */
	class MyContentObserver extends ContentObserver {

		public MyContentObserver(Handler handler) {
			super(handler); //	handler消息处理器
			// TODO Auto-generated constructor stub
		}

		/*
		 * 当被监听的内容发送改变时回调
		 */
		@Override
		public void onChange(boolean selfChange) {
			Uri uri = Uri.parse("content://sms/outbox");	// 发件箱的uri
			
			//	查询发件箱的内容
			Cursor cursor = getContentResolver().query(uri, new String[]{"address", "date", "body"}, 
					null, null, null);
			if (cursor != null && cursor.getCount() > 0) {
				
				while (cursor.moveToNext()) {
					String address = cursor.getString(0);
					long date = cursor.getLong(1);
					String body = cursor.getString(2);
				}
			}
		}
		
	}
}
