package com.luocanhui.shiguangzhou;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class MyAdapter extends BaseExpandableListAdapter {

	private Context context;
	private List<TimeLineBean> list;

	public MyAdapter(Context context, List<TimeLineBean> list) {
		super();
		this.context = context;
		this.list = list;
	}

	class GroupHolder {
		TextView tv;
	}

	class ChildHolder {
		TextView tv;
	}

	// ----------------------- child ---------------------------------
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {
		ChildHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.child_item, null);
			holder = new ChildHolder();
			holder.tv = (TextView) convertView.findViewById(R.id.ci_thing);
			convertView.setTag(holder);
		} else {
			holder = (ChildHolder) convertView.getTag();
		}
		holder.tv.setText(list.get(groupPosition).getThings().get(childPosition));
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return list.get(groupPosition).getThings().size();
	}

	// --------------------------- group ----------------------
	@Override
	public Object getGroup(int groupPosition) {
		return list.get(groupPosition).getDate();
	}

	@Override
	public int getGroupCount() {
		return list.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		GroupHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.group_item, null);
			holder = new GroupHolder();
			holder.tv = (TextView) convertView.findViewById(R.id.gi_date);
			convertView.setTag(holder);
		}else {
			holder = (GroupHolder) convertView.getTag();
		}
		holder.tv.setText(list.get(groupPosition).getDate());
		return convertView;
	}

	// ---------------------------------
	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}

}
