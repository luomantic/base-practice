package com.luocanhui.shiguangzhou;

import java.util.List;

public class TimeLineBean {
	private String date;
	private List<String> things;

	public TimeLineBean(String date, List<String> things) {
		super();
		this.date = date;
		this.things = things;
	}

	public TimeLineBean() {
		super();
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<String> getThings() {
		return things;
	}

	public void setThings(List<String> things) {
		this.things = things;
	}

}
