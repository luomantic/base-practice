package com.luocanhui.shiguangzhou;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;

public class MainActivity extends Activity {

	private List<TimeLineBean> list;
	private ExpandableListView lv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		initData();
		initView();
	}

	private void initView() {
		lv = (ExpandableListView) this.findViewById(R.id.lv);
		lv.setAdapter(new MyAdapter(MainActivity.this, list));
		for (int i = 0; i<list.size(); i++) {
			lv.expandGroup(i);
		}
		lv.setGroupIndicator(null);
		lv.setOnGroupClickListener(new OnGroupClickListener() {
			
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				return true;
			}
		});
	}

	private void initData() {
		list = new ArrayList<TimeLineBean>();
		List<String> things = new ArrayList<String>();
		things.add("六王毕，四海一；");
		things.add("蜀山兀，阿房出。");
		things.add("覆压三百余里，隔离天日。");
		list.add(new TimeLineBean("11月24日", things));
		things = new ArrayList<String>();
		things.add("骊山北构而西折，");
		things.add("直走咸阳。");
		things.add("二川溶溶，流入宫墙。");
		list.add(new TimeLineBean("11月23日", things));
		things = new ArrayList<String>();
		things.add("五步一楼，十步一阁；");
		things.add("廊腰缦回，");
		list.add(new TimeLineBean("11月22日", things));
		things = new ArrayList<String>();
		things.add("檐牙高啄；");
		things.add("各抱地势，钩心斗角。");
		things.add("盘盘焉，囷囷焉，蜂房水涡，");
		things.add("矗不知乎几千万落！");
		list.add(new TimeLineBean("11月21日", things));
		things = new ArrayList<String>();
		things.add("长桥卧波，未云何龙？");
		things.add("複道行空，不霁何虹？");
		things.add("高低冥迷，不知西东。");
		list.add(new TimeLineBean("11月20日", things));
		things = new ArrayList<String>();
		things.add("歌台暖响，");
		things.add("春光融融；");
		list.add(new TimeLineBean("11月19日", things));
		things = new ArrayList<String>();
		things.add("舞殿冷袖，");
		things.add("风雨凄凄。");
		things.add("一日之内，一宫之间，");
		things.add("而气候不齐。");
		list.add(new TimeLineBean("11月18日", things));
	}
}
