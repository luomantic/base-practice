package com.luocanhui.imgdemo;

import java.io.ByteArrayOutputStream;
import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity {

	public static final int NONE = 0;
	public static final int PHOTOGRAPH = 1; // 拍照
	public static final int PHOTOZOOM = 2; // 缩放
	public static final int PHOTORESOULT = 3; // 结果
	
	public static final String IMAGE_UNSPECIFIELD = "image/*"; 
	private ImageView mImageView;
	private Button button_paizhao;
	private Button button_xiangce;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mImageView = (ImageView) this.findViewById(R.id.imageview_show);
		button_paizhao = (Button) this.findViewById(R.id.button_paizhao);
		button_xiangce = (Button) this.findViewById(R.id.button_xiangce);
		
		button_xiangce.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 打开系统相册
				Intent intent = new Intent(Intent.ACTION_PICK, null); 
				intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIELD);
				startActivityForResult(intent, PHOTOZOOM);
			}
		});
		
		button_paizhao.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(
						MediaStore.EXTRA_OUTPUT, Uri.fromFile(
								new File(Environment.getExternalStorageDirectory()+"/"+getPackageName(), "temp.jpg")));
				startActivityForResult(intent, PHOTOGRAPH);
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == NONE) {
			return;
		}
		
		// 拍照
		if (requestCode == PHOTOGRAPH) {
			// 设置文件保存路径，这里放跟目录下
			File picture = new File(Environment.getExternalStorageDirectory()+"/"+getPackageName(), "/temp.jpg'");
			startPhotoZoom(Uri.fromFile(picture));
		}
		
		if (data == null) {
			return;
		}

		// 读取相册缩放图片
		if (requestCode == PHOTOZOOM) {
			startPhotoZoom(data.getData());
		}
		
		//处理结果
		if (requestCode == PHOTORESOULT) {
			Bundle extras = data.getExtras();
			if (extras != null) {
				Bitmap photo = extras.getParcelable("data");
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				photo.compress(Bitmap.CompressFormat.JPEG, 75, stream);
				mImageView.setImageBitmap(photo);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void startPhotoZoom(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, IMAGE_UNSPECIFIELD);
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 150);
		intent.putExtra("outputY", 150);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, PHOTORESOULT);
	}
	
}
