package com.luocanhui.ycposition.fragment;

import com.luocanhui.ycposition.R;

import android.view.View;
import android.widget.TextView;

public class ContentFragment extends BaseFragment {

	TextView tv;
	
	@Override
	public View initViews() {
		View view = View.inflate(mActivity, R.layout.fragment_content, null);
		tv = (TextView) view.findViewById(R.id.content_fragment);
		return view;
	}

}
