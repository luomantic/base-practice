package com.luocanhui.ycposition.fragment;

import com.luocanhui.ycposition.R;

import android.view.View;
import android.widget.TextView;

public class LeftMenuFragment extends BaseFragment {

	TextView tv;
	
	@Override
	public View initViews() {
		View view = View.inflate(mActivity, R.layout.fragment_left_menu, null);
		tv = (TextView) view.findViewById(R.id.city_name);
		return view;
	}

}
