package com.luocanhui.imageloader01.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.widget.ImageView;

/**
 * @author Administrator 图片加载类
 */
public class ImageLoader {
	// 图片缓存
	LruCache<String, Bitmap> mImageCache;
	// 线程池，线程数量为cpu的数量
	ExecutorService mExecutorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	public ImageLoader() {
		initImageCache();
	}

	private void initImageCache() {
		// 计算最大可用内存
		int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		// 取四分之一的可用内存作为缓存
		int cacheSize = maxMemory / 4;

		mImageCache = new LruCache<String, Bitmap>(cacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				return bitmap.getRowBytes() * bitmap.getHeight() / 1024;
			}
		};
	}

	public void displayImage(final String url, ImageView imageView) {
		imageView.setTag(url);
		mExecutorService.submit(new Runnable() {
			public void run() {
				Bitmap bitmap = downloadImage(url);
				if (bitmap == null) {
					return;
				}
			}
		});
	}

	private Bitmap downloadImage(String url) {
		// TODO Auto-generated method stub
		return null;
	}
}
