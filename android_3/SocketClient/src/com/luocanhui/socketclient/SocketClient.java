package com.luocanhui.socketclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class SocketClient {

	public static void main(String[] args) {
		SocketClient client = new SocketClient();
		client.start();
	}
	
	public void start() {
		BufferedReader inputReader = null; // 用来读取本地输入
		BufferedReader reader = null; // 用来读取服务器端传过来的数据
		BufferedWriter writer = null; 
		Socket socket = null;
		try {
			socket = new Socket("127.0.0.1", 6666);
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			inputReader = new BufferedReader(new InputStreamReader(System.in));
			startServerReplyListener(reader);
			String inputContent;
			while (!(inputContent = inputReader.readLine()).equals("bye")) {
				writer.write(inputContent + "\n");
				writer.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				reader.close();
				writer.close();
				inputReader.close();
				socket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 开启子线程，监听服务器发送过来的数据
	 * @param reader
	 */
	public void startServerReplyListener(final BufferedReader reader) {
		new Thread(new Runnable() {
			public void run() {
				try {
					String response;
					while ((response=reader.readLine())!=null) {
						System.out.println(response);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
}
