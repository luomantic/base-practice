package com.luocanhui.kaiguananniu;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;

@SuppressLint("ClickableViewAccessibility")
public class MyToggleButton extends View implements OnClickListener {

	// 作为背景的图片
	private Bitmap backgroundBitmap;

	// 作为滑动的图片
	private Bitmap slideButton;

	private Paint paint; // 画笔

	private float slideButton_left; // 滑动按钮的左边界

	/*
	 * 背景图的资源id
	 */
	private int backgroundId;

	/*
	 * 滑动图片的资源id
	 */
	private int slideButtonId;

	/**
	 * 在代码里面创建对象的时候，使用此构造方法
	 * 
	 * @param context
	 */
	public MyToggleButton(Context context) {
		super(context);
	}

	/**
	 * 在布局文件中声明的view，创建时，由系统自动调用。
	 * 
	 * @param context
	 * @param attrs
	 */
	public MyToggleButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		// 无命名空间测试
		String testAttrs = attrs.getAttributeValue(null, "testAttrs");
		System.out.println("testAtrrs ---------- " + testAttrs);
		
		// 获得自定义的属性
		TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.MyToggleButton);
		
		int N = ta.getIndexCount();
		for (int i=0; i<N; i++) {
			int itemId = ta.getIndex(i); // 获得当前属性的id值
			switch (itemId) {
			case R.styleable.MyToggleButton_curr_state:
				currState = ta.getBoolean(itemId, false);
				break;
			case R.styleable.MyToggleButton_my_background:
				backgroundId  = ta.getResourceId(itemId, -1);
				if (backgroundId == -1) {
					throw new RuntimeException("请设置背景图片");
				}
				backgroundBitmap = BitmapFactory.decodeResource(getResources(), backgroundId);
				break;
			case R.styleable.MyToggleButton_my_slide_button:
				slideButtonId = ta.getResourceId(itemId, -1);
				if (slideButtonId == -1) {
					throw new RuntimeException("请设置滑动按钮的背景图片");
				}
				slideButton = BitmapFactory.decodeResource(getResources(), slideButtonId);
				break;
			}
		}
		
		ta.recycle();
		initView();
	}

	// /**
	// * @param context
	// * @param attrs
	// * @param defStyleAttr 空间长什么样子，控制空间的样式、
	// */
	// public MyToggleButton(Context context, AttributeSet attrs, int
	// defStyleAttr) {
	// super(context, attrs, defStyleAttr);
	// }

	/*
	 * view 对象显示在屏幕上的几个重要步骤： 1、构造方法创建对象。 2、测量view的大小。 onMeasure(int,int);
	 * 3、确定view的位置，view自身有一些建议权，决定权在父view手中。onLayout();(一般由viewgroup执行，
	 * 自定义view中不常用) 4、绘制view的内容。onDraw(Canvas)
	 */

	/**
	 * 初始化
	 */
	private void initView() {
		// 初始化背景图片
//		backgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.switch_background);
//		slideButton = BitmapFactory.decodeResource(getResources(), R.drawable.slide_button);

		// 初始化画笔
		paint = new Paint();
		paint.setAntiAlias(true); // 打开抗锯齿

		// 添加onclick事件监听
		this.setOnClickListener(this);
		
		flushState();
	}

	/*
	 * 测量尺寸时的回调方法
	 */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		/*
		 * 设置当前view的大小 width：view的宽度 height：view的高度 （单位：像素）
		 */
		setMeasuredDimension(backgroundBitmap.getWidth(), backgroundBitmap.getHeight());
	}

	// /*
	// * 确定位置的时候调用此方法。
	// * 自定义view的时候作用不大。
	// */
	// @Override
	// protected void onLayout(boolean changed, int left, int top, int right,
	// int bottom) {
	// super.onLayout(changed, left, top, right, bottom);
	// }

	private boolean currState = false; // 当前开关的状态，true为开

	/*
	 * 绘制当前view的内容
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		// super.onDraw(canvas);

		// 绘制背景
		/*
		 * backgroundBitmap 要绘制的图片，
		 *  left 图片的左边距 
		 *  top 图片的上边距 
		 *  paint 绘制图片要使用的画笔
		 */
		canvas.drawBitmap(backgroundBitmap, 0, 0, paint);

		// 绘制 可滑动按钮
		canvas.drawBitmap(slideButton, slideButton_left, 0, paint);
	}

	/*
	 * 判断是否发生拖动， 如果拖动了，就不再响应onclick事件
	 */
	private boolean isDrag = false;

	/*
	 * onclick 事件在View.onTouchEvent 中被解析。 系统对onclick事件的解析，过于简陋，只要有down事件和up事件，
	 * 系统就认为发生了click事件
	 */
	@Override
	public void onClick(View v) {
		/*
		 * 如果没有拖动，才刷新状态
		 */
		if (!isDrag) {
			currState = !currState;
			flushState();
		}
	}

	/*
	 * down 事件时的x值
	 */
	private int firstX;
	/*
	 * touch 事件的上一个x值
	 */
	private int lastX;

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			firstX = lastX = (int) event.getX();
			isDrag = false;
			break;
		case MotionEvent.ACTION_MOVE:
			// 判断是否发生拖动
			if (Math.abs(event.getX() - firstX) > 5) {
				isDrag = true;
			}
			int dis = (int) (event.getX() - lastX); // 计算手指在屏幕移动的距离
			lastX = (int) event.getX(); // 将本次的位置设置给lastX
			slideButton_left = slideButton_left + dis; // 根据手指移动的距离，改变slideButton_left的值
			break;
		case MotionEvent.ACTION_UP:
			// 在发生拖动的情况下，根据最后的位置，判断当前开关的状态
			if (isDrag) {
				int maxLeft = backgroundBitmap.getWidth() - slideButton.getWidth();// slideButton左边界最大值
				/*
				 * 根据slideButton_left，判断当前是什么状态
				 */
				if (slideButton_left > maxLeft / 2) { // 此时应当为打开的状态
					currState = true;
				} else {
					currState = false;
				}

				flushState();
			}
			break;
		}

		flushView();

		return true;
	}

	/**
	 * 刷新当前状态
	 */
	private void flushState() {
		if (currState) {
			slideButton_left = backgroundBitmap.getWidth() - slideButton.getWidth();
		} else {
			slideButton_left = 0;
		}

		flushView();
	}

	/**
	 * 刷新当前视图
	 */
	private void flushView() {
		/*
		 * 对 slideButton_left 的值进行判断，确定其在合理的位置。 0<= slideButton_left <= maxLeft
		 */
		int maxLeft = backgroundBitmap.getWidth() - slideButton.getWidth();// slideButton左边界最大值

		// 确保slideButton_left >= 0;
		slideButton_left = slideButton_left > 0 ? slideButton_left : 0;
		// 确保slideButton_left <= maxLeft;
		slideButton_left = slideButton_left < maxLeft ? slideButton_left : maxLeft;

		/*
		 * 刷新当前视图，导致ondraw方法执行
		 */
		invalidate();
	}

}
