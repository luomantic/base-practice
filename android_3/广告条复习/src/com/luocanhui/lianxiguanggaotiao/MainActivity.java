package com.luocanhui.lianxiguanggaotiao;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("HandlerLeak")
public class MainActivity extends Activity {

	private ViewPager mViewPager;
	private LinearLayout point_group;
	private TextView imageDesc;
	
	// 引入图片资源id
	private final int[] imageIds = {R.drawable.a, R.drawable.b, R.drawable.c,
			R.drawable.d, R.drawable.e};  
	private ArrayList<ImageView> imageList; // 保存图片的集合
	
	// 引入图片的描述信息
	private final String[] imageDescriptions = {
			"巩俐不低俗，我就不能低俗",
			"扑树又回来啦！再唱经典老歌引万人大合唱",
			"揭秘北京电影如何升级",
			"乐视网TV版大派送",
			"热血屌丝的反杀"
	};
	
	protected int lastPosition;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mViewPager = (ViewPager) findViewById(R.id.myviewpager);
		point_group = (LinearLayout) findViewById(R.id.linearlayout_point_group);
		imageDesc = (TextView) findViewById(R.id.textview_message);
		imageDesc.setText(imageDescriptions[0]);
		
		imageList = new ArrayList<ImageView>();
		for(int i=0; i<imageIds.length; i++) {
			// 初始化图片资源
			ImageView imageView = new ImageView(this);
			imageView.setBackgroundResource(imageIds[i]);
			imageList.add(imageView);
			
			// 添加指示点
			ImageView point = new ImageView(this);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(10, 10);
			params.rightMargin = 20;
			point.setLayoutParams(params);
			point.setBackgroundResource(R.drawable.point_bg);
			if (i==0) {
				point.setEnabled(true);
			}else {
				point.setEnabled(false);
			}
			point_group.addView(point);
		}
		
		mViewPager.setAdapter(new MyViewPagerAdapter());
		mViewPager.setCurrentItem(Integer.MAX_VALUE/2 - (Integer.MAX_VALUE/2%imageList.size())); // 进去的时候总是第一个页面
		mViewPager.addOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			// 页面切换后
			public void onPageSelected(int position) {
				position = position%imageList.size();
				imageDesc.setText(imageDescriptions[position]); // 设置图片的文字描述信息
				//改变指示点的状态
				point_group.getChildAt(position).setEnabled(true);
				point_group.getChildAt(lastPosition).setEnabled(false);
				lastPosition = position;
			}
			
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				
			}
			
			@Override
			public void onPageScrollStateChanged(int state) {
				
			}
		});
		
		isRunning = true; // 默认开启自动播放
		handler.sendEmptyMessageDelayed(0, 3000);
	}
	
	private boolean isRunning = false; // 初始化默认播放为false
	
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			// 让viewpager 滑动到下一页
			mViewPager.setCurrentItem(mViewPager.getCurrentItem()+1);
			if (isRunning) {
				handler.sendEmptyMessageDelayed(0, 3000);
			}
		};
	};
	
	@Override
	protected void onDestroy() {
		isRunning = false;
		super.onDestroy();
	}

	class MyViewPagerAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return Integer.MAX_VALUE; // 无限多个页面
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			container.addView(imageList.get(position%imageList.size()));
			return imageList.get(position%imageList.size());
		}
		
		@Override
		public boolean isViewFromObject(View view, Object object) {
			if (view == object) {
				return true;
			}else {
				return false;
			}
		}
		
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
			object = null;
		}
	}
	
}
