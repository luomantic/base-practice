package com.luocanhui.youkucaidan;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class MainActivity extends Activity implements OnClickListener{

	private ImageView icon_menu;
	private ImageView icon_home;
	
	private RelativeLayout level1;
	private RelativeLayout level2;
	private RelativeLayout level3;
	
	/*
	 * 判断第三级菜单是否显示
	 */
	private boolean isLevel3Show = true; // 默认显示
	private boolean isLevel2Show = true;
	private boolean isLevel1Show = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		icon_menu = (ImageView) findViewById(R.id.icon_menu);
		icon_home = (ImageView) findViewById(R.id.icon_home);
		
		level1 = (RelativeLayout) findViewById(R.id.level1);
		level2 = (RelativeLayout) findViewById(R.id.level2);
		level3 = (RelativeLayout) findViewById(R.id.level3);
		
		icon_home.setOnClickListener(this);
		icon_menu.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.icon_menu: // 处理menu图标的点击事件
			// 如果第3级菜单是显示状态，那么将其隐藏
			// 如果第3级菜单是隐藏状态，那么将其显示
			if (isLevel3Show) {
				MyUtils.startAnimationOut(level3);
			}else {
				MyUtils.startAnimationIn(level3);
			}
			isLevel3Show = !isLevel3Show;
			break;
		case R.id.icon_home: // 处理home图标的点击事件
			// 如果第2级菜单是显示状态，那么就隐藏第2,3级菜单
			// 如果第2级菜单是隐藏状态，那么就显示第2级菜单
			if (isLevel2Show) {
				MyUtils.startAnimationOut(level2);
				isLevel2Show = false;
				if (isLevel3Show) {
					MyUtils.startAnimationOut(level3, 200);
					isLevel3Show = false;
				}
			}else {
				MyUtils.startAnimationIn(level2);
				isLevel2Show = true;
			}
			break;
		}
	}
	
	/* 
	 * 监听按键的动作
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) { // 监听menu按键
			changeLevel1State();
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * 改变第1级菜单的状态
	 */
	private void changeLevel1State() {
		// 如果第1级菜单是显示状态，那么就隐藏第1,2,3级菜单
		// 如果第1级菜单是隐藏状态，那么就显示1,2级菜单
		if (isLevel1Show ) {
			MyUtils.startAnimationOut(level1);
			isLevel1Show = false;
			if (isLevel2Show) {
				MyUtils.startAnimationOut(level2, 100);
				isLevel2Show = false;
				
				if (isLevel3Show) {
					MyUtils.startAnimationOut(level3, 200);
					isLevel3Show = false;
				}
			}
		}else {
			MyUtils.startAnimationIn(level1);
			isLevel1Show = true;
			
			MyUtils.startAnimationIn(level2, 200);
			isLevel2Show = true;
		}
	}
}
