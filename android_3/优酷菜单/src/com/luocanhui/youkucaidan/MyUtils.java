package com.luocanhui.youkucaidan;

import android.view.animation.RotateAnimation;
import android.widget.RelativeLayout;

public class MyUtils {

	/**
	 * 让指定view执行旋转离开的动画
	 * @param view
	 */
	public static void startAnimationOut(RelativeLayout view) {
		// 旋转动画。圆心：左上角；旋转方向：顺时钟方向0-180°
		RotateAnimation animation = new RotateAnimation(
				0, 180,view.getWidth()/2, view.getHeight());
		animation.setDuration(500); // 设置运行的时间
		animation.setFillAfter(true); // 动画执行完成以后，保存在最后的状态
		view.startAnimation(animation);
	}

	/**
	 * 让指定view执行旋转进入的动画
	 * @param view
	 */
	public static void startAnimationIn(RelativeLayout view) {
		RotateAnimation animation = new RotateAnimation(
				180, 360, view.getWidth()/2, view.getHeight());
		animation.setDuration(500);
		animation.setFillAfter(true);
		view.startAnimation(animation);
	}

	/**
	 * 让指定view 延时执行旋转离开的动画
	 * @param view 指定的view
	 * @param time 延时的时间
	 */
	public static void startAnimationOut(RelativeLayout view, int time) {
		RotateAnimation animation = new RotateAnimation(
				0, 180, view.getWidth()/2, view.getHeight());
		animation.setDuration(500);
		animation.setFillAfter(true);
		animation.setStartOffset(time); // 设置延时执行的时间
		view.startAnimation(animation);
	}

	/**
	 * 让指定view 延时执行旋转进入的动画
	 * @param view 指定的view
	 * @param time 延时的时间
	 */
	public static void startAnimationIn(RelativeLayout view, int time) {
		RotateAnimation animation = new RotateAnimation(
				180, 360, view.getWidth()/2, view.getHeight());
		animation.setDuration(500);
		animation.setFillAfter(true);
		animation.setStartOffset(time); // 设置延时执行的时间
		view.startAnimation(animation);
	}

}
