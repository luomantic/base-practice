package com.luocanhui.server;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

public class MyTextLineCumulativeDecoder extends CumulativeProtocolDecoder{

	@Override
	protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput output) 
			throws Exception {
		int startPosition = in.position();
		while (in.hasRemaining()) {
			byte b = in.get();
			if (b == '\n') {
				int currentPosition = in.position();
				int limit = in.limit();
				in.position(startPosition); // λ���ض���
				in.limit(currentPosition);
				IoBuffer buffer = in.slice(); // �ַ�����ȡ
				byte[] dest = new byte[buffer.limit()];
				buffer.get(dest);
				String str = new String(dest);
				output.write(str);
				in.position(currentPosition); // λ�û�ԭ
				in.limit(limit);
				return true;
			}
		}
		in.position(startPosition);
		return false;
	}

}
