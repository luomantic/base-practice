package com.luocanhui.server;

import java.net.InetSocketAddress;

import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

public class MinaServer {
	
	public static void main(String[] args) {
		try {
			NioSocketAcceptor acceptor = new NioSocketAcceptor();
			acceptor.setHandler(new MyServerHandler());  // 消息处理跟网络处理分离
			
			// 系统自带的解码器
//			acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(new TextLineCodecFactory()));
			// 自定义的解码器
			acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(new MyTextLineFactory())); 
			
			acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 5); // 设置超时发送心跳包的时间
			
			acceptor.bind(new InetSocketAddress(6666));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
