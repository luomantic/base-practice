package com.luocanhui.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServer {

	BufferedReader reader = null;
	BufferedWriter writer = null;

	public static void main(String[] args) {
		SocketServer socketServer = new SocketServer();
		socketServer.startServer();
	}

	/**
	 * 开启服务器
	 */
	public void startServer() {
		ServerSocket serverSocket = null;
		Socket socket = null;
		try {
			serverSocket = new ServerSocket(6666);
			System.out.println("server started ...");
			while (true) {
				socket = serverSocket.accept(); // 获得用来通信的socket
				manageConnection(socket);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
				serverSocket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 管理服务器端跟客户端的连接
	 */
	public void manageConnection(final Socket socket) {
		new Thread(new Runnable() {
			public void run() {
				try {
					System.out.println("client " + socket.hashCode() + " is connected");
					reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
					String receivedMsg;
					while ((receivedMsg = reader.readLine()) != null) {
						System.out.println("client" + socket.hashCode() + ":" + receivedMsg);
						writer.write("server reply:" + receivedMsg + "\n");
						writer.flush();
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						reader.close();
						writer.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

}
