package com.luocanhui.server;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

public class MyTextLineDecoder implements ProtocolDecoder {

	@Override
	public void decode(IoSession session, IoBuffer in, ProtocolDecoderOutput output) 
			throws Exception {
		int startPosition = in.position();
		while (in.hasRemaining()) {
			byte b = in.get();
			if (b == '\n') {
				int currentPosition = in.position();
				int limit = in.limit();
				in.position(startPosition); // 位置重定向
				in.limit(currentPosition);
				IoBuffer buffer = in.slice(); // 字符串截取
				byte[] dest = new byte[buffer.limit()];
				buffer.get(dest);
				String str = new String(dest);
				output.write(str);
				in.position(currentPosition); // 位置还原
				in.limit(limit);
			}
		}
	}

	@Override
	public void dispose(IoSession arg0) throws Exception {

	}

	@Override
	public void finishDecode(IoSession arg0, ProtocolDecoderOutput arg1) 
			throws Exception {
		
	}

}
