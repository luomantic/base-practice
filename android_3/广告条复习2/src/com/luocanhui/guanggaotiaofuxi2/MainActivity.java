package com.luocanhui.guanggaotiaofuxi2;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("HandlerLeak")
public class MainActivity extends Activity {

	private ViewPager viewPager;
	private TextView imageDesc;
	private LinearLayout pointGroup;

	private final int[] imageIds = { 
				R.drawable.a, 
				R.drawable.b, 
				R.drawable.c, 
				R.drawable.e, 
				R.drawable.d 
			};
	private ArrayList<ImageView> imageList;

	// 引入图片的描述信息
	private final String[] imageDescriptions = { "巩俐不低俗，我就不能低俗", "扑树又回来啦！再唱经典老歌引万人大合唱", "揭秘北京电影如何升级", "乐视网TV版大派送",
			"热血屌丝的反杀" };
	protected int lastPosition;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		viewPager = (ViewPager) findViewById(R.id.myviewpager);
		imageDesc = (TextView) findViewById(R.id.imageDesc);
		pointGroup = (LinearLayout) findViewById(R.id.point_group);

		imageList = new ArrayList<ImageView>();
		imageDesc.setText(imageDescriptions[0]);
		for (int i = 0; i < imageIds.length; i++) {
			ImageView imageView = new ImageView(this);
			imageView.setBackgroundResource(imageIds[i]);
			imageList.add(imageView);

			ImageView point = new ImageView(this);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(10, 10);
			params.rightMargin = 20;
			point.setLayoutParams(params);
			point.setBackgroundResource(R.drawable.point_bg);
			if (i == 0) {
				point.setEnabled(true);
			} else {
				point.setEnabled(false);
			}
			pointGroup.addView(point);
		}

		viewPager.setAdapter(new MyViewPagerAdapter());
		// 解决刚开始只能向右滑，不能向左滑
		viewPager.setCurrentItem(Integer.MAX_VALUE / 2 - (Integer.MAX_VALUE / 2 % imageList.size()));
		viewPager.addOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				position = position % imageList.size();
				imageDesc.setText(imageDescriptions[position]);

				pointGroup.getChildAt(position).setEnabled(true);
				pointGroup.getChildAt(lastPosition).setEnabled(false);
				lastPosition = position;
			}

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			}

			@Override
			public void onPageScrollStateChanged(int state) {
			}
		});
		isRunning = true;
		handler.sendEmptyMessageDelayed(0, 2000);
	}

	private boolean isRunning = false;

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
			if (isRunning) {
				handler.sendEmptyMessageDelayed(0, 2000);
			}
		};
	};

	protected void onDestroy() {
		isRunning = false;
		super.onDestroy();
	};

	class MyViewPagerAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return Integer.MAX_VALUE;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			container.addView(imageList.get(position % imageList.size()));
			return imageList.get(position % imageList.size());
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			if (view == object) {
				return true;
			} else {
				return false;
			}
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}
	}
}
