package com.luocanhui.myattrs;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

public class MyView extends View {

	public MyView(Context context) {
		super(context);
	}

	public MyView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		/*
		 * AttributeSet 对xml布局文件，解析后的结果，封装为 AttributeSet 对象。
		 * 存储的都是原始数据。仅对数据进行简单加工。
		 */
		
		int attributeCount = attrs.getAttributeCount();
		
		for (int i=0; i<attributeCount; i++) {
			String attributeName = attrs.getAttributeName(i);
			String attributeValue = attrs.getAttributeValue(i);
			System.out.println("attributeName:" + attributeName + 
					"     attributeValue" + attributeValue);
		}
		
		/*
		 * 对AttributeSet 中的原始数据 按照 图纸中的说明（R.styleable.MyView中的类型说明）
		 * 创建出具体的对象。
		 */
		
		TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.MyView);
		
		/*
		 * 获得内容的个数
		 */
		int taCount = ta.getIndexCount();
		System.out.println("-----------------------------------------------------");
		
		for (int i=0; i<taCount; i++) {
			int itemId = ta.getIndex(i);
			System.out.println("itemId:" + itemId);
			
			switch (itemId) {
			case R.styleable.MyView_test_id:
				ta.getInt(itemId, 10);
				break;
			case R.styleable.MyView_test_msg:
				String msg = ta.getString(itemId);
				System.out.println("test_msg: " + msg);
				break;
			case R.styleable.MyView_test_bitmap:
				ta.getDrawable(itemId);
				int bitmapId = ta.getResourceId(itemId, 100);
				System.out.println("test_bitmap_bitmapId: " + bitmapId);
				break;
			}
			
		}
		
		ta.recycle();
	}

//	public MyView(Context context, AttributeSet attrs, int defStyleAttr) {
//		super(context, attrs, defStyleAttr);
//	}

}
