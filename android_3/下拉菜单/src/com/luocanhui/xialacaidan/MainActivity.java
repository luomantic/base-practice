package com.luocanhui.xialacaidan;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

public class MainActivity extends Activity {

	private EditText edittext;
	private ImageView downArrow;

	private List<String> msgList;

	private PopupWindow popWin;
	private ListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		edittext = (EditText) findViewById(R.id.edittext);
		downArrow = (ImageView) findViewById(R.id.down_arrow);

		msgList = new ArrayList<String>();

		for (int i = 0; i < 20; i++) {
			msgList.add("100000" + i);
		}

		initListView();

		downArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// 定义popupwindow
				popWin = new PopupWindow(MainActivity.this);
				popWin.setWidth(edittext.getWidth());
				popWin.setHeight(300);

				popWin.setContentView(listView);// popwindow填充内容

				// 点击popupwindow以外的区域，自动关闭当前popupwindow
				popWin.setOutsideTouchable(true);

				popWin.showAsDropDown(edittext, 0, 0); // 设置popupwindow显示的位置
			}
		});

	}

	/**
	 * 弄一个ListView
	 */
	private void initListView() {
		listView = new ListView(this);
		listView.setDivider(null); // 设置条目之间的分隔线为空
		listView.setVerticalScrollBarEnabled(false); // 关闭竖直滑动按钮
		listView.setBackgroundResource(R.drawable.listview_background); // 设置listview背景
		listView.setAdapter(new MyListViewAdapter());
	}

	class MyListViewAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return msgList.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = View.inflate(getApplicationContext(), R.layout.list_item, null);

				holder = new ViewHolder();
				holder.delete = (ImageView) convertView.findViewById(R.id.delete);
				holder.tv_msg = (TextView) convertView.findViewById(R.id.tv_list_item);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.tv_msg.setText(msgList.get(position));
			holder.delete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// 删除对应的条目
					msgList.remove(position);

					// 刷新listview
					MyListViewAdapter.this.notifyDataSetChanged();
				}
			});

			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// 设置输入框
					edittext.setText(msgList.get(position));

					// 隐藏popwind
					if (popWin.isShowing()) {
						popWin.dismiss();
					}

				}
			});

			return convertView;
		}

		private class ViewHolder {
			TextView tv_msg;
			ImageView delete;
		}
	}

}
