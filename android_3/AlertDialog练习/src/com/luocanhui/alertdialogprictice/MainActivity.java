package com.luocanhui.alertdialogprictice;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class MainActivity extends Activity {

	private String[] items = null;
	private ArrayAdapter<String> adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		items = new String[] { "魏", "蜀", "吴" };
	}

	public void btnClick(View view) {
		switch (view.getId()) {
		case R.id.btn1:
			btn1();
			break;
		case R.id.btn2:
			btn2();
			break;
		case R.id.btn3:
			btn3();
			break;
		case R.id.btn4:
			btn4();
			break;
		case R.id.btn5:
			btn5();
			break;
		case R.id.btn6:
			btn6();
			break;
		}
	}

	private void btn6() {
		View view = LayoutInflater.from(this).inflate(R.layout.activity_main, null);
		AlertDialog dialog = new AlertDialog.Builder(this).setTitle("自定义View").setIcon(R.drawable.a4s).setView(view)
				.create();
		dialog.show();
	}

	private void btn5() {
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, new String[] { "张三", "李四" });
		AlertDialog dialog = new AlertDialog.Builder(this).setTitle("自定义Adapter").setIcon(R.drawable.a4s)
				.setAdapter(adapter, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				}).create();
		dialog.show();
	}

	private void btn4() {
		AlertDialog dialog = new AlertDialog.Builder(this).setTitle("多选对话框").setIcon(R.drawable.a4s)
				.setNegativeButton("取消", null).setPositiveButton("确定", null)
				.setMultiChoiceItems(items, null, new OnMultiChoiceClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked) {
						Log.d("qianfeng", "isChecked:" + isChecked);
					}
				}).create();
		dialog.show();
	}

	private void btn3() {
		AlertDialog dialog = new AlertDialog.Builder(this).setTitle("单选对话框").setIcon(R.drawable.a4s)
				.setSingleChoiceItems(items, -1, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Toast.makeText(MainActivity.this, items[which], Toast.LENGTH_SHORT).show();
						dialog.dismiss();
					}
				}).create();
		dialog.show();
	}

	private void btn2() {
		AlertDialog dialog = new AlertDialog.Builder(this).setTitle("显示Item对话框").setIcon(R.drawable.a4s)
				.setItems(items, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Toast.makeText(MainActivity.this, items[which], Toast.LENGTH_SHORT).show();
					}
				}).create();
		dialog.show();
	}

	private void btn1() {
		AlertDialog dialog = new AlertDialog.Builder(this).setTitle("普通对话框").setIcon(R.drawable.a4s)
				.setNegativeButton("取消", null).setPositiveButton("确定", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 处理确认按钮的点击事件
					}
				}).setNeutralButton("中立", null).setMessage("确认删除？").create();
		dialog.show();
	}
}
