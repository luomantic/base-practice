package com.luocanhui.guanggaotiao;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("HandlerLeak")
public class MainActivity extends Activity {

	private ViewPager viewPager;
	private LinearLayout pointGroup;
	private TextView textView;

	// 图片资源id
	private final int[] imageIds = { R.drawable.a, R.drawable.b, R.drawable.c, 
			R.drawable.d, R.drawable.e };

	// 图片标题集合
	private final String[] imageDescriptions = {
			"巩俐不低俗，我就不能低俗",
			"朴树又回来啦！再唱经典老歌引万人大合唱",
			"揭秘北京电影如何升级",
			"乐视网TV版大派送",
			"热血屌丝的反杀"
	};
	
	private ArrayList<ImageView> imageList;
	
	// 上一个页面的位置
	protected int lastPosition;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		viewPager = (ViewPager) findViewById(R.id.viewpager);
		pointGroup = (LinearLayout) findViewById(R.id.point_group);
		textView = (TextView) findViewById(R.id.textview);
		
		imageList = new ArrayList<ImageView>();
		for(int i=0; i<imageIds.length; i++) {
			// 初始化图片资源
			ImageView image = new ImageView(this);
			image.setBackgroundResource(imageIds[i]);
			imageList.add(image);
			
			// 添加指示点
			ImageView point = new ImageView(this);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(10, 10);
			params.rightMargin = 20;
			point.setLayoutParams(params);
			
			point.setBackgroundResource(R.drawable.point_bg);
			if (i==0) {
				point.setEnabled(true);
			}else {
				point.setEnabled(false);
			}
			
			pointGroup.addView(point); // 把指示点添加到之前写的，空的linearlayout
		}
		
		textView.setText(imageDescriptions[0]); // 默认显示第一个标题
		
		viewPager.setAdapter(new MyPagerAdapter());
		viewPager.setCurrentItem(Integer.MAX_VALUE/2 - (Integer.MAX_VALUE/2%imageList.size())); // 设置初始页面的位置
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			/*
			 * 页面切换后调用
			 * position 新的页面位置
			 */
			@Override
			public void onPageSelected(int position) {
				
				position = position%imageList.size(); // 修改position，让viewpager的image循环
				
				// 设置标题内容
				textView.setText(imageDescriptions[position]);
				// 把当前point enable设置true，并且把上一个点设置为flase
				pointGroup.getChildAt(position).setEnabled(true);
				pointGroup.getChildAt(lastPosition).setEnabled(false);
				lastPosition = position;
			}
			
			/*
			 * 页面正在滑动的时候，不断的回调
			 */
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				// 当前位置的偏移量，便宜的像素等
			}
			
			/*
			 * 当页面滑动状态发生改变的时候，回调
			 */
			@Override
			public void onPageScrollStateChanged(int state) {
				
			}
		});
		
		/*
		 * 自动循环：
		 * 1.定时器：Timer
		 * 2.开子线程while true循环
		 * 3.ColckManager
		 * 4.用handler发送延时信息，实现循环
		 */
		isRunning = true;
		handler.sendEmptyMessageDelayed(0, 2000);
	}

	// 判断是否自动滚动
	private boolean isRunning = false;
	
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			
			// 让viewPager滑动到下一页
			viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
			if (isRunning) {
				handler.sendEmptyMessageDelayed(0, 2000);
			}
			
		};
	};
	
	@Override
	protected void onDestroy() {
		isRunning = false;
		super.onDestroy();
	};
	
	private class MyPagerAdapter extends PagerAdapter {

		@Override
		public int getCount() { // 获得页面总数
			return Integer.MAX_VALUE;
		}

		/*
		 * 获得相应位置上的view 
		 * container view的容器，其实就是viewpager自身 
		 * position 相应的位置
		 */
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			// 给container 添加一个view
			container.addView(imageList.get(position%imageList.size())); // 修改position，让viewpager的image循环
			// 返回一个和该view相对应的object对象
			return imageList.get(position%imageList.size());
		}
		
		/*
		 * 判断view和object的对应关系
		 */
		@Override
		public boolean isViewFromObject(View view, Object object) {
			if (view == object) {
				return true;
			}else {
				return false;
			}
		}
		
		/*
		 * 销毁对应位置上的object
		 */
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
//			super.destroyItem(container, position, object);
			container.removeView((View) object);
			object = null;
		}

	}

}
